package name.remal.gradle_plugins.plugins.code_quality.detekt

import io.gitlab.arturbosch.detekt.api.Detektion
import io.gitlab.arturbosch.detekt.api.Location
import io.gitlab.arturbosch.detekt.api.OutputReport
import io.gitlab.arturbosch.detekt.api.Severity.Defect
import io.gitlab.arturbosch.detekt.api.Severity.Minor
import io.gitlab.arturbosch.detekt.api.Severity.Security
import io.gitlab.arturbosch.detekt.api.SourceLocation
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.utils.code_quality.FindBugsReport
import name.remal.gradle_plugins.dsl.utils.code_quality.asXmlString
import name.remal.nullIf
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8

@AutoService(OutputReport::class)
class FindBugsXmlCustomReporter : OutputReport() {

    override val ending = "xml"

    override fun render(detektion: Detektion) = FindBugsReport().apply {
        detektion.findings.asSequence()
            .flatMap { entry -> entry.value.asSequence().map { entry.key to it } }
            .forEach { (category, finding) ->

                bug {
                    category(category)
                    type(finding.issue.id)
                    when (finding.issue.severity) {
                        Security -> rankBlocker()
                        Defect -> rankCritical()
                        Minor -> rankMajor()
                        else -> rankMinor()
                    }
                    confidence(1)
                    message(finding.message)
                    location {
                        with(finding.location) {
                            className(run {
                                val entity = finding.invokeMethod("getEntity") ?: return@run null
                                val ktElement = entity.invokeMethod("getKtElement") ?: return@run null
                                val fqName = ktElement.invokeMethod("getFqName") ?: return@run null
                                return@run fqName.toString()
                            })

                            sourceFile(file)
                            startLine(source.line)
                            startLineOffset(source.column)

                            sourceEnd?.let {
                                endLine(it.line)
                                endLineOffset(it.column)
                            }
                        }
                    }
                }

                type(finding.issue.id) {
                    textDescription(finding.issue.description)
                }

            }
    }.asXmlString()


    private fun Any?.invokeMethod(name: String): Any? {
        if (this == null) return null
        try {
            val method = javaClass.getMethod(name)
            return method.invoke(this)

        } catch (ignored: Throwable) {
            return null
        }
    }

}

private val Location.sourceEnd: SourceLocation?
    get() {
        val startIndex = text.start
        val endIndex = text.end
        if (endIndex < startIndex) return null

        val content = File(file).nullIf { !isAbsolute }
            ?.readText(UTF_8)
            ?.replace("\r\n", "\n")
            ?.replace("\n\r", "\n")
            ?.replace("\r", "\n")
            ?: return null

        val startLine = 1 + content.substring(0, startIndex).count { it == '\n' }
        if (startLine != source.line) return null
        val startLineOffset = run {
            var offset = 1
            var i = startIndex - 1
            while (i >= 0) {
                if (content[i] == '\n') {
                    break
                } else {
                    offset++
                }
                i--
            }
            return@run offset
        }
        if (startLineOffset != source.column) return null

        var endLine = startLine
        var endLineOffset = startLineOffset
        var i = startIndex + 1
        while (i < endIndex) {
            if (i >= content.length) {
                return null
            } else if (content[i] == '\n') {
                endLine++
                endLineOffset = 1
            } else {
                endLineOffset++
            }
            i++
        }

        return SourceLocation(endLine, endLineOffset)
    }
