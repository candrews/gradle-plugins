The plugin applies these plugins:

* [`name.remal.common-ci`](name.remal.common-ci.md)
* [`name.remal.github-actions-ci`](name.remal.github-actions-ci.md)
* [`name.remal.gitlab-ci`](name.remal.gitlab-ci.md)
* [`name.remal.travis-ci`](name.remal.travis-ci.md)
