The plugin applies plugin [`name.remal.dependency-versions`](name.remal.dependency-versions.md).

&nbsp;

The plugin creates `checkDependencyUpdates` task of type [`CheckDependencyUpdates`](#nameremalgradle_pluginspluginscheck_updatescheckdependencyupdates) that displays dependency updates for the project.

Also the plugin create `checkDependencyUpdates` configuration to specify dependencies that have to be checked for available updates.

&nbsp;

### `name.remal.gradle_plugins.plugins.check_updates.CheckDependencyUpdates`
| Property | Type | Description
| --- | :---: | --- |
| `notCheckedDependencies` | <nobr>`MutableSet<String>`</nobr> | Dependency notations to exclude from checking for available updates, for example: `com.google.guava:guava`. You can use `*` character to specify a notation pattern, for example: `com.*.guava:*`. |
| `checkAllVersionsDependencies` | <nobr>`MutableSet<String>`</nobr> | Dependency notations to check for all available version, even excluded with [`name.remal.dependency-versions`](name.remal.dependency-versions.md) plugin. You can use `*` character as for `notCheckedDependencies` property. |
| `failIfUpdatesFound` | `boolean` | Fail the build if new versions have been found. Default value: `false`. |
