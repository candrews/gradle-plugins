The plugin applies these plugins:

* [`java`](https://docs.gradle.org/current/userguide/java_plugin.html)
* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.classes-processing`](name.remal.classes-processing.md)
* [`name.remal.jacoco-settings`](name.remal.jacoco-settings.md)

&nbsp;

This plugin processes compilation result of all tasks of type [`AbstractCompile`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/compile/AbstractCompile.html) for `main` source-set. It relocates dependency classes by copying them to the compiled classes directory adding prefix (from [`ClassesRelocationExtension.relocatedClassesPackageName`](#nameremalgradle_pluginspluginsclasses_relocationclassesrelocationextension)) to relocated class names.

The functionality can be configured using created `classesRelocation` extension of type [`ClassesRelocationExtension`](#nameremalgradle_pluginspluginsclasses_relocationclassesrelocationextension).

### `relocateClasses` configuration

Classes from dependencies defined in `relocateClasses` configuration will be relocated. All other dependencies of `compileClasspath` and `runtimeClasspath` will be excluded from relocation process.

If you want to exclude dependencies from relocation, add them to `excludeFromClassesRelocation` configuration.

`compileOnly` configuration extends `relocateClasses`.

### `RelocateClasses` and `RelocatePackages` annotations

Classes set via `name.remal.gradle_plugins.api.RelocateClasses` and classes inside packages set via `name.remal.gradle_plugins.api.RelocatePackages` will be relocated for the class annotated with these annotations. Only dependencies from `excludeFromForcedClassesRelocation` configuration will be excluded.

These annotations are accessible using `name.remal:gradle-plugins-api:@gradle-plugins-api.version@` Maven compile-only dependency.

&nbsp;

### `name.remal.gradle_plugins.plugins.classes_relocation.ClassesRelocationExtension`
| Property | Type | Description
| --- | :---: | --- |
| `relocatedClassesPackageName` | <nobr>`String`</nobr> | Base package name for relocated classes. Default value: `<project group>.<project name>.internal._relocated`. |
