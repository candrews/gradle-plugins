Plugin that configures some default [component capabilities](https://docs.gradle.org/current/userguide/managing_transitive_dependencies.html#sec:capabilities).

For example:

* `ch.qos.logback:logback-classic:*`
    * `logging:slf4j-impl:0`
* `commons-logging:commons-logging:*`
    * `logging:jcl-api:0`
* `commons-logging:commons-logging-api:*`
    * `logging:jcl-api:0`
* `log4j:log4j:*`
    * `logging:log4j12-api:0`
* `org.apache.logging.log4j:log4j-1.2-api:*`
    * `logging:log4j12-api:0`
* `org.apache.logging.log4j:log4j-api:*`
    * `logging:log4j-api:0`
* `org.apache.logging.log4j:log4j-core:*`
    * `logging:log4j-impl:0`
* `org.apache.logging.log4j:log4j-jcl:*`
    * `logging:jcl-impl:0`
* `org.apache.logging.log4j:log4j-slf4j-impl:*`
    * `logging:slf4j-impl:0`
* `org.apache.logging.log4j:log4j-to-slf4j:*`
    * `logging:log4j-impl:0`
* `org.slf4j:jcl-over-slf4j:*`
    * `logging:jcl-api:*`
    * `logging:jcl-impl:0`
* `org.slf4j:log4j-over-slf4j:*`
    * `logging:log4j12-api:0`
* `org.slf4j:nlog4j:*`
    * `logging:log4j12-api:*`
    * `logging:slf4j-api:*`
    * `logging:slf4j-impl:0`
* `org.slf4j:slf4j-android:*`
    * `logging:slf4j-impl:0`
* `org.slf4j:slf4j-api:*`
    * `logging:slf4j-api:0`
* `org.slf4j:slf4j-jcl:*`
    * `logging:slf4j-impl:0`
* `org.slf4j:slf4j-jdk14:*`
    * `logging:slf4j-impl:0`
* `org.slf4j:slf4j-log4j12:*`
    * `logging:slf4j-impl:0`
* `org.slf4j:slf4j-nop:*`
    * `logging:slf4j-impl:0`
* `org.slf4j:slf4j-simple:*`
    * `logging:slf4j-impl:0`
* `org.springframework:spring-jcl:*`
    * `logging:jcl-api:*`
    * `logging:jcl-impl:0`


Also the plugin applies component capabilities from [Nebula Gradle Resolution Rules](https://github.com/nebula-plugins/gradle-resolution-rules).
