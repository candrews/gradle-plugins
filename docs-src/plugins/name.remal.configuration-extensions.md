This plugin adds some useful extension methods to all configurations.

&nbsp;

#### `excludeLogging()` - excludes logging dependencies:

* org.slf4j:*
* ch.qos.logback:*
* org.apache.logging.log4j:*
* log4j:*
* commons-logging:*
* org.springframework:spring-jcl

Usage:

```gradle
configurations.api.excludeLogging()
```

#### `create*ClassLoader()` - creates `URLClassLoader` for the configuration's files

There are 3 methods:

* `createClassLoader()` - creates usual class loader that loads classes from the parent first and only then from itself
* `createThisOnlyClassLoader()` - creates a class loader that loads classes only from itself and not from the parent
* `createThisFirstClassLoader()` - creates a class loader that loads classes from itself first and only then from the parent

"The parent class loader" is a class loader that Gradle uses for the current build.

**Logging classes are always loaded from the current Gradle build (parent) class loader.**

**These methods return `URLClassLoader`, so don't forget to close it.**
