This plugin adds <code>dependencies.filter(Object dependency, Action&lt;<a href="#nameremalgradle_pluginspluginsdependenciesfiltered_dependenciesdependencyfilter">DependencyFilter</a>&gt; configurer)<code> extension method.

`dependency` can be an instance of:
* [`Dependency`](https://docs.gradle.org/current/javadoc/org/gradle/api/artifacts/Dependency.html)
* Dependency notation

The extension method works this way:
1. If `dependency` is a dependency notation, then a dependency with this notation is created.
1. An instance of [`DependencyFilter`](#nameremalgradle_pluginspluginsdependenciesfiltered_dependenciesdependencyfilter) is configured.
1. A cacheable self-resolving dependency is returned where filters are applied on original dependency.

&nbsp;

### `name.remal.gradle_plugins.plugins.dependencies.filtered_dependencies.DependencyFilter`
| Property | Type | Description
| --- | :---: | --- |
| `includes` | <nobr>`MutableSet<String>`</nobr> | ANT like include patterns of resources from original dependency. |
| `excludes` | <nobr>`MutableSet<String>`</nobr> | ANT like exclude patterns of resources from original dependency. |

| Method | Description
| --- | --- |
| `void include(String... includes)` | Add ANT like include patterns of resources from original dependency. |
| `void include(Iterable<String> includes)` | Add ANT like include patterns of resources from original dependency. |
| `void exclude(String... excludes)` | Add ANT like exclude patterns of resources from original dependency. |
| `void exclude(Iterable<String> excludes)` | ANT like exclude patterns of resources from original dependency. |
