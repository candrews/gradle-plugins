This plugin modifies all dependencies of all configurations. For that purpose services of type [`DependencyModifier`](#nameremalgradle_pluginspluginsdependenciesdependencymodifier) are used.

Be default these modifications are implemented:
* `com.google.guava:guava`: exclude static analysis transitive dependencies

&nbsp;

The services and factories are loaded using [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html).

&nbsp;

### `name.remal.gradle_plugins.plugins.dependencies.DependencyModifier`
| Method | Description
| --- | --- |
| <code>void modify(<a href="https://docs.gradle.org/current/javadoc/org/gradle/api/artifacts/Dependency.html">Dependency</a> dependency)</code> | Modifies the dependency. |
