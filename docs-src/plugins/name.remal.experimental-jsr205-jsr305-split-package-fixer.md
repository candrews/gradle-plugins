**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied and [`sourceCompatibility`](https://docs.gradle.org/current/javadoc/org/gradle/api/plugins/JavaPluginConvention.html#getSourceCompatibility--) is Java 9 compatible.**

&nbsp;

JSR 205 and JSR 305 define annotations in `javax.annotation` package. For Java version 9 and greater it causes split package error. Take a look at [this article](https://blog.codefx.org/java/jsr-305-java-9/). This plugin was made to solve this problem.

The plugin processes all configurations and sets up [dependency substitution](https://docs.gradle.org/current/userguide/customizing_dependency_resolution_behavior.html#sec:dependency_substitution_rules) to substitute `javax.annotation:javax.annotation-api` and `com.google.code.findbugs:jsr305` with `name.remal.merged:jsr205-jsr305:javax_<annotation-api version>-findbugs_<jsr305 version>`.

The plugin correctly handles a situation when one of the JSRs is defined in one configuration and the other in another configuration that extends the first one.
Let's consider that we have `compile` and `compileOnly` configurations. `compileOnly` extends `compile`. `compile` configuration has `javax.annotation:javax.annotation-api:<annotation-api version>` dependency and `compileOnly` configuration has `com.google.code.findbugs:jsr305:<jsr305 version>` dependency. In this case **both** configurations will have corresponding `name.remal.merged:jsr205-jsr305:javax_<annotation-api version>-findbugs_<jsr305 version>` dependency.

If the project has only one of the dependencies, a merged dependency will be added anyway. The latest version of missed dependency will be used. For example if we have `javax.annotation:javax.annotation-api:<annotation-api version>` dependency without jsr305, this dependency will be used: `name.remal.merged:jsr205-jsr305:javax_<annotation-api version>-+`.

Transitive dependencies are handled correctly.
