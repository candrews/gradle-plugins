**This plugin works only if [`idea`](https://docs.gradle.org/current/userguide/idea_plugin.html) plugin is applied.**

The plugin applies [`name.remal.idea-settings`](name.remal.idea-settings.md) plugin.

&nbsp;

This plugin:

* Registers all classes from `META-INF/services/name.remal.gradle_plugins.plugins.ide.idea.EntryPointAnnotation` classpath resources as IDEA entry-point annotations.
* Registers all classes from `META-INF/services/name.remal.gradle_plugins.plugins.ide.idea.WriteAnnotation` classpath resources as IDEA write annotations.
* Registers all classes from `META-INF/services/name.remal.gradle_plugins.plugins.ide.idea.NullableAnnotation` classpath resources as IDEA nullable annotations.
* Registers all classes from `META-INF/services/name.remal.gradle_plugins.plugins.ide.idea.NotNullAnnotation` classpath resources as IDEA not-null annotations.
* Registers default annotation-processing profile. If you don't delegate compilation, then annotation-processing profile should be configured to enable annotation-processing.
* Sets Checkstyle version for [Checkstyle-IDEA plugin](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea) from [`checkstyle.toolVersion`](https://docs.gradle.org/current/dsl/org.gradle.api.plugins.quality.CheckstyleExtension.html#org.gradle.api.plugins.quality.CheckstyleExtension:toolVersion).
