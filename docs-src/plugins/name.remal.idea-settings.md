**This plugin works only if [`idea`](https://docs.gradle.org/current/userguide/idea_plugin.html) plugin is applied.**

&nbsp;

* Apply [`idea`](https://docs.gradle.org/current/userguide/idea_plugin.html) and this plugins for all projects
* It turns ON downloading dependency javadoc artifacts.
* It turns ON downloading dependency sources artifacts.
* If [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied:
    * Remove `compileOnly` configuration from IDEA module `TEST` scope

<a id="idea-settings"></a>
Also the plugin creates 'ideaSettings' extension of type [`IdeaSettings`](#nameremalgradle_pluginspluginsideideaideasettings).

&nbsp;

### `name.remal.gradle_plugins.plugins.ide.idea.IdeaSettings`
| Method | Description
| --- | --- |
| <code>void configureIDEAComponent(String componentName, String ideaDirRelativePath, Action&lt;<a href="http://www.jdom.org/docs/apidocs/org/jdom2/Element.html">Element</a>&gt;)</code> | Processes IDEA component (stored as XML element) of name equals to `componentName` and stored into `$rootProjectDir/.idea/$ideaDirRelativePath`. |
| `void entryPointAnnotation(String annotationCanonicalName)` | Registers entry-point annotation into IDEA project configuration. |
| `void entryPointAnnotations(String... annotationCanonicalNames)` | Registers entry-point annotations into IDEA project configuration. |
| `void entryPointAnnotations(Collection<String> annotationCanonicalNames)` | Registers entry-point annotations into IDEA project configuration. |
| `void writeAnnotation(String annotationCanonicalName)` | Registers write annotation into IDEA project configuration. |
| `void writeAnnotations(String... annotationCanonicalNames)` | Registers write annotations into IDEA project configuration. |
| `void writeAnnotations(Collection<String> annotationCanonicalNames)` | Registers write annotations into IDEA project configuration. |
| `void nullableAnnotation(String annotationCanonicalName)` | Registers nullable annotation into IDEA project configuration. |
| `void nullableAnnotations(String... annotationCanonicalNames)` | Registers nullable annotations into IDEA project configuration. |
| `void nullableAnnotations(Collection<String> annotationCanonicalNames)` | Registers nullable annotations into IDEA project configuration. |
| `void notNullAnnotation(String annotationCanonicalName)` | Registers not-null annotation into IDEA project configuration. |
| `void notNullAnnotations(String... annotationCanonicalNames)` | Registers not-null annotations into IDEA project configuration. |
| `void notNullAnnotations(Collection<String> annotationCanonicalNames)` | Registers not-null annotations into IDEA project configuration. |
| `void addDefaultAnnotationProcessingProfile()` | Registers default annotation-processing profile. If you don't delegate compilation, then annotation-processing profile should be configured to enable annotation-processing. |
