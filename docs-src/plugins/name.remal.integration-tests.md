**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies [`name.remal.test-source-sets`](name.remal.test-source-sets.md) plugin.

&nbsp;

This plugin adds `integration` [test-source-set](name.remal.test-source-sets.md#test-source-sets).
