**This plugin works only if [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html) plugin is applied.**

The plugin applies [`name.remal.maven-publish-settings`](name.remal.maven-publish-settings.md) and [`name.remal.environment-variables`](name.remal.environment-variables.md) plugins.

&nbsp;

This plugin adds `publishing.repositories.gradlePluginPortalPublish` method. This method adds [Gradle Plugins Portal](https://plugins.gradle.org/docs/submit) repository to publish Gradle plugins to.

Publishing SNAPSHOT artifacts will be skipped, as Gradle Plugins Portal doesn't accept SNATSHOT versions.

Usage:

```groovy tab="Groovy"
publishing.repositories.gradlePluginPortalPublish {
    plugins {
        "com.org.plugin-name" {
            id = "com.org.plugin-id" // Optional. If not set plugin name will be used (i.e. "com.org.plugin-name")
            displayName = "dysplay name"
            description = "description"
            tags = ["tag1", "tag2"]
            websiteUrl = "http://example.com/" // Optional
            vcsUrl = "http://example.com/repo.git" // Optional
        }
    }

    websiteUrl = "http://example.com/" // Optional. If not set, POM file will be parsed
    vcsUrl = "http://example.com/repo.git" // Optional. If not set, POM file will be parsed

    publishKey = "publish-key" // Optional. By default 'gradle.publish.key' property or 'GRADLE_PUBLISH_KEY' environment variable is used
    publishSecret = "publish-secret" // Optional. By default 'gradle.publish.secret' property or 'GRADLE_PUBLISH_SECRET' environment variable is used
}
```

```kotlin tab="Kotlin"
import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.RepositoryHandlerGradlePluginPortalPublishExtension
import name.remal.gradle_plugins.dsl.extensions.*

publishing.repositories.convention[RepositoryHandlerGradlePluginPortalPublishExtension::class.java].gradlePluginPortalPublish {
    this.plugins {
        "com.org.plugin-id" {
            id = "com.org.plugin-id" // Optional. If not set plugin name will be used (i.e. "com.org.plugin-name")
            displayName = "dysplay name"
            description = "description"
            tags = mutableSetOf("tag1", "tag2")
            websiteUrl = "http://example.com/" // Optional
            vcsUrl = "http://example.com/repo.git" // Optional
        }
    }

    websiteUrl = "http://example.com/" // Optional. If not set, POM file will be parsed
    vcsUrl = "http://example.com/repo.git" // Optional. If not set, POM file will be parsed

    publishKey = "publish-key" // Optional. By default 'gradle.publish.key' property or 'GRADLE_PUBLISH_KEY' environment variable is used
    publishSecret = "publish-secret" // Optional. By default 'gradle.publish.secret' property or 'GRADLE_PUBLISH_SECRET' environment variable is used
}
```

For each published plugin `${pluginId}` is replaced with the plugin's ID in `websiteUrl` and `vcsUrl` properties.
