**This plugin works only if [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html) plugin is applied.**

This plugin applies [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) and [`name.remal.common-settings`](name.remal.common-settings.md) plugins.

&nbsp;

* Add `jar` task to `archives` configuration artifacts`
* Make all publish tasks depend on `build` task
* Make all publish tasks run after `build` task of all projects
* Create `buildInstall` task that depends on `build` and `publishToMavenLocal` tasks
* Configure POM's `dependencyManagement` and `dependencies` sections:
    * All dependencies'versions (including transitive) are set in `dependencyManagement` section
    * Correct exclusions are set
    * Correct scopes are set
* Adds `publishing.publications.mavenDefault` extension method. This method creates default Maven publication with all sources and javadoc artifacts.
* Adds `publishing.publications.mavenBom` extension method. This method creates Maven publication with [BOM](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html) pom file only.
* Default Maven publication is used by default.
* Create publish-to-repository tasks
