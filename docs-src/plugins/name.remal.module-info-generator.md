**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies [`name.remal.common-settings`](name.remal.common-settings.md) plugins.

&nbsp;

The plugin creates `generateModuleInfo` task of type `name.remal.gradle_plugins.plugins.jpms.GenerateModuleInfoTask`.

### Scanning

`GenerateModuleInfoTask` automatically scans `*.class` and `META-INF/services/*` result files to set this values:

* If main class name isn't set explicitly, scanning tries to file main class
* `requires` instructions are added by default by analyzing class files and their dependencies
* `uses` instructions are added by default by analyzing class files invocations of `ServiceLoader.load()` and `ServiceLoader.loadInstalled()` methods
* `provides` instructions are added by default by analyzing `META-INF/services/*` files

### Configuration

```groovy
tasks.generateModuleInfo {
    destinationDir = project.file("...") // Sets destination dir

    moduleName = "..." // Sets module name, if not set, defined automatically.
    open = true // Sets if module should be opened or not. Default value: true.
    mainClassName = "..." // Sets main class name

    requires {
        add("...") // Adds required module name
        addDependency("...") // Adds required module name, that is taken from compile dependencies. Here you should provide dependency notation matcher like '*:jsr305' (includes all dependencies with any group and name equals to 'jsr305').

        addTransitive("...") // Adds required TRANSITIVE module name
        addTransitiveDependency("...") // Adds required TRANSITIVE module name, that is taken from compile dependencies. Here you should provide dependency notation matcher like '*:jsr305' (includes all dependencies with any group and name equals to 'jsr305').

        addStatic("...") // Adds required STATIC module name
        addStaticDependency("...") // Adds required STATIC module name, that is taken from compile dependencies. Here you should provide dependency notation matcher like '*:jsr305' (includes all dependencies with any group and name equals to 'jsr305').

        exclude("...") // Excludes module name from REQUIRE section in generated module-info file
        excludeDependency("...") // Excludes compile dependency from REQUIRE section in generated module-info file
    }

    exports {
        includes.add("com.example.*") // Exports 'com.example' package and all sub-packages
        excludes.add("com.example.*") // Excludes 'com.example' package and all sub-packages from exporting. Default values: '*.internal.*', '*.shaded.*', '*.shadow.*'.
    }

    uses {
        add("...") // Adds service used by ServiceLoader.load() method to uses section
    }

    classesDirs.from("...") // Adds a directory with classes or resources for scanning
    compileClasspathConfiguration = configurations.compileClasspath // Sets compile classpath configuration for scanning
    compileOnlyConfiguration = configurations.compileOnly // Modules from this configuration and it's super configurations will be required STATICally

    sourceSet = sourceSets.main // Uses provided source-set to initialize classesDirs and compileClasspathConfiguration

    classesWithAllowedDynamicServiceLoader += [
        'pkg.*',  // Don't report classes which use dynamic param for ServiceLoader.load() in pkg package
        'pkg.**',  // Don't report classes which use dynamic param for ServiceLoader.load() in pkg package and all subpackages
    ]
}
```

