This plugin randomizes test classes order on every [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) task execution. At the same time it doesn't break those tasks cacheability.

The plugin creates `testClassesRandomization` extension of type [`RandomizeTestClassesExtension`](#nameremalgradle_pluginspluginstestingrandomizetestclassesextension) to control the randomization.

```groovy
testClassesRandomization {
    seed = 123
}
```

The plugin uses the same random seed for all [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) tasks. It allows to easily reproduce tests execution order.

&nbsp;

### `name.remal.gradle_plugins.plugins.testing.RandomizeTestClassesExtension`
| Property | Type | Description
| --- | :---: | --- |
| `enabled` | `boolean` | Is randomization enabled. Default value: `true`. |
| `seed` | `long` | Initial random seed. By default a random value is used. If you want to reproduce an order, set this property to a value used to generate that order. |
