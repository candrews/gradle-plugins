This plugin configures all tasks of type [`Reporting`](https://docs.gradle.org/current/javadoc/org/gradle/api/reporting/Reporting.html). It sets default destination for every report.
