**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies these plugins:

* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.test-settings`](name.remal.test-settings.md)

&nbsp;

<a id="test-source-sets"></a>
The plugin creates `testSourceSets` extension of type `TestSourceSetContainer` (it extends [`SourceSetContainer`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/SourceSetContainer.html)).

* All test-source-sets created by `testSourceSets` container (or just added to it) are added to `sourceSets` container.
* If a test-source-set is added to `testSourceSets` container, it's also added to `sourceSets`.
* If a source-set is removed from `sourceSets` container, it's also removed from `testSourceSets`.
* If a test-source-set is removed from `testSourceSets` container, it's also removed from `sourceSets`.

All configurations related to a test-source-set extend corresponding configuration of `test` source-set.

By default `testSourceSets` container contains `test` source-set.

&nbsp;

The plugin creates a [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) task for every test-source-set. Also it creates `runAllTests` task that depends on `test` task of every test-source-set.
