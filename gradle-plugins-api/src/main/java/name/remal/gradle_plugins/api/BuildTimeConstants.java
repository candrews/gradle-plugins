package name.remal.gradle_plugins.api;

import java.util.Map;

@SuppressWarnings({"unused", "squid:S1192"})
public final class BuildTimeConstants {

    /**
     * Invocation of this method will be replaced with actual class name. <b>Class parameter must be a constant expression</b>.
     */
    public static String getClassName(Class<?> clazz) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual class simple name. <b>Class parameter must be a constant expression</b>.
     */
    public static String getClassSimpleName(Class<?> clazz) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual class simple name. <b>Class parameter must be a constant expression</b>.
     */
    public static String getClassPackageName(Class<?> clazz) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual class internal name. <b>Class parameter must be a constant expression</b>.
     */
    public static String getClassInternalName(Class<?> clazz) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual class descriptor. <b>Class parameter must be a constant expression</b>.
     */
    public static String getClassDescriptor(Class<?> clazz) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }


    /**
     * Invocation of this method will be replaced with actual property value. <b>Property name parameter must be a constant expression</b>.
     */
    public static String getStringProperty(String propertyName) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual property value parsed as int. <b>Property name parameter must be a constant expression</b>.
     */
    public static int getIntegerProperty(String propertyName) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual property value parsed as long. <b>Property name parameter must be a constant expression</b>.
     */
    public static long getLongProperty(String propertyName) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual property value parsed as boolean. <b>Property name parameter must be a constant expression</b>.
     */
    public static boolean getBooleanProperty(String propertyName) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }


    /**
     * Invocation of this method will be replaced with actual properties values. <b>Property name parameter must be a constant expression</b>.
     */
    public static Map<String, String> getStringProperties(String propertyPattern) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual properties values parsed as int. <b>Property name parameter must be a constant expression</b>.
     */
    public static Map<String, Integer> getIntegerProperties(String propertyPattern) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual properties values parsed as long. <b>Property name parameter must be a constant expression</b>.
     */
    public static Map<String, Long> getLongProperties(String propertyPattern) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }

    /**
     * Invocation of this method will be replaced with actual properties values parsed as boolean. <b>Property name parameter must be a constant expression</b>.
     */
    public static Map<String, Boolean> getBooleanProperties(String propertyPattern) {
        throw new UnsupportedOperationException("This method can't be invoked directly. Use 'remal.name.classes-processing' Gradle plugin to process the method invocation.");
    }


    private BuildTimeConstants() {
    }

    static {
        throwIllegalClassUseException();
    }

    private static void throwIllegalClassUseException() {
        throw new UnsupportedOperationException("This class can't be used directly. Use 'remal.name.classes-processing' Gradle plugin to handle the class's methods.");
    }

}
