package name.remal.gradle_plugins.api;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({TYPE, PACKAGE})
@Retention(CLASS)
public @interface ExcludeFromCodeCoverage {
}
