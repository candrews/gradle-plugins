package name.remal.gradle_plugins.api;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({TYPE, CONSTRUCTOR, METHOD})
@Retention(CLASS)
public @interface RelocatePackages {

    String[] value() default {};

    Class<?>[] basePackageClasses() default {};

}
