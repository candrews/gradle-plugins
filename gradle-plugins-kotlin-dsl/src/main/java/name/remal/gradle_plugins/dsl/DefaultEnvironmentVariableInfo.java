package name.remal.gradle_plugins.dsl;

import java.util.Objects;
import javax.annotation.Nullable;
import org.gradle.api.Project;

public class DefaultEnvironmentVariableInfo implements EnvironmentVariableInfo {

    private final String variableName;

    private final String description;

    @Nullable
    private final String pluginId;

    @Nullable
    private final Class<? extends org.gradle.api.Plugin<? extends Project>> pluginClass;

    @Nullable
    private final String scope;

    @Nullable
    private final Class<? extends EnvironmentVariableCondition> conditionClass;

    private DefaultEnvironmentVariableInfo(
        String variableName,
        String description,
        @Nullable String pluginId,
        @Nullable Class<? extends org.gradle.api.Plugin<? extends Project>> pluginClass,
        @Nullable String scope,
        @Nullable Class<? extends EnvironmentVariableCondition> conditionClass
    ) {
        this.variableName = variableName;
        this.description = description;
        this.pluginId = pluginId;
        this.pluginClass = pluginClass;
        this.scope = scope;
        this.conditionClass = conditionClass;
    }

    public DefaultEnvironmentVariableInfo(String variableName, String description) {
        this(variableName, description, null, null, null, null);
    }

    @Override
    public String getVariableName() {
        return variableName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Nullable
    @Override
    public String getPluginId() {
        return pluginId;
    }

    @Nullable
    @Override
    public Class<? extends org.gradle.api.Plugin<? extends Project>> getPluginClass() {
        return pluginClass;
    }

    @Nullable
    @Override
    public String getScope() {
        return scope;
    }

    @Nullable
    @Override
    public Class<? extends EnvironmentVariableCondition> getConditionClass() {
        return conditionClass;
    }

    @Override
    public DefaultEnvironmentVariableInfo withPluginId(@Nullable String pluginId) {
        if (pluginId == null || pluginId.isEmpty()) pluginId = null;
        if (Objects.equals(getPluginId(), pluginId)) return this;
        return new DefaultEnvironmentVariableInfo(getVariableName(), getDescription(), pluginId, getPluginClass(), getScope(), getConditionClass());
    }

    @Override
    public DefaultEnvironmentVariableInfo withPluginClass(@Nullable Class<? extends org.gradle.api.Plugin<? extends Project>> pluginClass) {
        if (NotDefinedProjectPlugin.class == pluginClass) pluginClass = null;
        if (Objects.equals(getPluginClass(), pluginClass)) return this;
        return new DefaultEnvironmentVariableInfo(getVariableName(), getDescription(), getPluginId(), pluginClass, getScope(), getConditionClass());
    }

    @Override
    public DefaultEnvironmentVariableInfo withScope(@Nullable String scope) {
        if (scope == null || scope.isEmpty()) scope = null;
        if (Objects.equals(getScope(), scope)) return this;
        return new DefaultEnvironmentVariableInfo(getVariableName(), getDescription(), getPluginId(), getPluginClass(), scope, getConditionClass());
    }

    @Override
    public DefaultEnvironmentVariableInfo withConditionClass(@Nullable Class<? extends EnvironmentVariableCondition> conditionClass) {
        if (NotDefinedEnvironmentVariableCondition.class == conditionClass) conditionClass = null;
        if (Objects.equals(getConditionClass(), conditionClass)) return this;
        return new DefaultEnvironmentVariableInfo(getVariableName(), getDescription(), getPluginId(), getPluginClass(), getScope(), conditionClass);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName()).append('{');
        sb.append("variableName='").append(getVariableName()).append('\'');
        sb.append(", description='").append(getDescription()).append('\'');
        sb.append(", pluginId='").append(getPluginId()).append('\'');
        sb.append(", pluginClass='").append(getPluginClass()).append('\'');
        sb.append(", scope='").append(getScope()).append('\'');
        sb.append(", conditionClass='").append(getConditionClass()).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
