package name.remal.gradle_plugins.dsl;

import java.util.Objects;
import javax.annotation.Nullable;
import org.gradle.api.Project;

public interface EnvironmentVariableInfo {

    String getVariableName();

    String getDescription();

    @Nullable
    default String getPluginId() {
        return null;
    }

    @Nullable
    default Class<? extends org.gradle.api.Plugin<? extends Project>> getPluginClass() {
        return null;
    }

    @Nullable
    default String getScope() {
        return null;
    }

    @Nullable
    default Class<? extends EnvironmentVariableCondition> getConditionClass() {
        return null;
    }


    default EnvironmentVariableInfo withPluginId(@Nullable String pluginId) {
        String normalizedPluginId = pluginId != null && !pluginId.isEmpty() ? pluginId : null;
        if (Objects.equals(getPluginId(), normalizedPluginId)) return this;
        EnvironmentVariableInfo self = this;
        return new EnvironmentVariableInfo() {
            @Override
            public String getVariableName() {
                return self.getVariableName();
            }

            @Override
            public String getDescription() {
                return self.getDescription();
            }

            @Override
            @Nullable
            public String getPluginId() {
                return normalizedPluginId;
            }

            @Override
            @Nullable
            public Class<? extends org.gradle.api.Plugin<? extends Project>> getPluginClass() {
                return self.getPluginClass();
            }

            @Override
            @Nullable
            public String getScope() {
                return self.getScope();
            }

            @Override
            @Nullable
            public Class<? extends EnvironmentVariableCondition> getConditionClass() {
                return self.getConditionClass();
            }
        };
    }

    default EnvironmentVariableInfo withPluginClass(@Nullable Class<? extends org.gradle.api.Plugin<? extends Project>> pluginClass) {
        Class<? extends org.gradle.api.Plugin<? extends Project>> normalizedPluginClass = NotDefinedProjectPlugin.class != pluginClass ? pluginClass : null;
        if (Objects.equals(getPluginClass(), normalizedPluginClass)) return this;
        EnvironmentVariableInfo self = this;
        return new EnvironmentVariableInfo() {
            @Override
            public String getVariableName() {
                return self.getVariableName();
            }

            @Override
            public String getDescription() {
                return self.getDescription();
            }

            @Override
            @Nullable
            public String getPluginId() {
                return self.getPluginId();
            }

            @Override
            @Nullable
            public Class<? extends org.gradle.api.Plugin<? extends Project>> getPluginClass() {
                return normalizedPluginClass;
            }

            @Override
            @Nullable
            public String getScope() {
                return self.getScope();
            }

            @Override
            @Nullable
            public Class<? extends EnvironmentVariableCondition> getConditionClass() {
                return self.getConditionClass();
            }
        };
    }

    default EnvironmentVariableInfo withScope(@Nullable String scope) {
        String normalizedScope = scope != null && !scope.isEmpty() ? scope : null;
        if (Objects.equals(getScope(), normalizedScope)) return this;
        EnvironmentVariableInfo self = this;
        return new EnvironmentVariableInfo() {
            @Override
            public String getVariableName() {
                return self.getVariableName();
            }

            @Override
            public String getDescription() {
                return self.getDescription();
            }

            @Override
            @Nullable
            public String getPluginId() {
                return self.getPluginId();
            }

            @Override
            @Nullable
            public Class<? extends org.gradle.api.Plugin<? extends Project>> getPluginClass() {
                return self.getPluginClass();
            }

            @Override
            @Nullable
            public String getScope() {
                return normalizedScope;
            }

            @Override
            @Nullable
            public Class<? extends EnvironmentVariableCondition> getConditionClass() {
                return self.getConditionClass();
            }
        };
    }

    default EnvironmentVariableInfo withConditionClass(@Nullable Class<? extends EnvironmentVariableCondition> conditionClass) {
        Class<? extends EnvironmentVariableCondition> normalizedConditionClass = NotDefinedEnvironmentVariableCondition.class != conditionClass ? conditionClass : null;
        if (Objects.equals(getConditionClass(), normalizedConditionClass)) return this;
        EnvironmentVariableInfo self = this;
        return new EnvironmentVariableInfo() {
            @Override
            public String getVariableName() {
                return self.getVariableName();
            }

            @Override
            public String getDescription() {
                return self.getDescription();
            }

            @Override
            @Nullable
            public String getPluginId() {
                return self.getPluginId();
            }

            @Override
            @Nullable
            public Class<? extends org.gradle.api.Plugin<? extends Project>> getPluginClass() {
                return self.getPluginClass();
            }

            @Override
            @Nullable
            public String getScope() {
                return self.getScope();
            }

            @Override
            @Nullable
            public Class<? extends EnvironmentVariableCondition> getConditionClass() {
                return normalizedConditionClass;
            }
        };
    }

}
