package name.remal.gradle_plugins.dsl;

import org.gradle.api.Project;

public interface NotDefinedProjectPlugin extends org.gradle.api.Plugin<Project> {
}
