package name.remal.gradle_plugins.dsl;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public class PluginId {

    private final String id;

    private final Set<String> alternateIds;

    public PluginId(String id, Collection<String> alternateIds) {
        this.id = id;

        Set<String> alternateIdsSet = new LinkedHashSet<>(alternateIds);
        alternateIdsSet.remove(id);
        if (alternateIds.isEmpty()) {
            this.alternateIds = emptySet();
        } else {
            this.alternateIds = unmodifiableSet(alternateIdsSet);
        }
    }

    public PluginId(String id, String... alternateIds) {
        this(id, asList(alternateIds));
    }

    public PluginId(String id) {
        this.id = id;
        this.alternateIds = emptySet();
    }

    public PluginId(PluginId primary, PluginId... others) {
        this(
            primary.getId(),
            Stream.concat(
                primary.getAlternateIds().stream(),
                Stream.of(others).flatMap(it ->
                    Stream.concat(
                        Stream.of(it.getId()),
                        it.getAlternateIds().stream()
                    ))
            )
                .collect(toList())
        );
    }

    public PluginId(PluginId primary) {
        this.id = primary.getId();
        this.alternateIds = primary.getAlternateIds();
    }

    public final String getId() {
        return id;
    }

    public final Set<String> getAlternateIds() {
        return alternateIds;
    }

    public final Set<String> getAllIds() {
        Set<String> result = new LinkedHashSet<>();
        result.add(id);
        result.addAll(alternateIds);
        return result;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PluginId)) return false;
        PluginId pluginId = (PluginId) o;
        return Objects.equals(id, pluginId.id) && Objects.equals(alternateIds, pluginId.alternateIds);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(id, alternateIds);
    }

    @Override
    public final String toString() {
        return this.getClass().getSimpleName() + "{"
            + "id=" + id
            + ", alternateIds=" + alternateIds
            + '}';
    }

}
