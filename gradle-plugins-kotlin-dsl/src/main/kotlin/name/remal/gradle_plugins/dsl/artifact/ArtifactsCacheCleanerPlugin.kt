package name.remal.gradle_plugins.dsl.artifact

import name.remal.KotlinAllOpen
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.archivePathCompatible
import name.remal.gradle_plugins.dsl.extensions.isNotRootProject
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.bundling.AbstractArchiveTask
import org.gradle.api.tasks.compile.AbstractCompile

@KotlinAllOpen
class ArtifactsCacheCleanerPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.rootProject.allprojects { it.applyPlugin(ArtifactsCacheCleanerPlugin::class.java) }
        if (project.isNotRootProject) return

        project.gradle.taskGraph.afterTask { task ->
            task.outputs.files.takeIf { it !is FileTree }?.forEach(ArtifactsCache::invalidate)

            when (task) {
                is AbstractCompile -> ArtifactsCache.invalidate(task.destinationDir)
                is AbstractArchiveTask -> ArtifactsCache.invalidate(task.archivePathCompatible)
                is Copy -> ArtifactsCache.invalidate(task.destinationDir)
            }
        }
    }

}
