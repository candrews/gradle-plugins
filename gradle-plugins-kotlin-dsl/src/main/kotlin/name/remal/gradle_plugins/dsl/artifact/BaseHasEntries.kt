package name.remal.gradle_plugins.dsl.artifact

import name.remal.isClassResourceName
import name.remal.resourceNameToClassName
import name.remal.toSortedSet

abstract class BaseHasEntries : HasEntries {

    override val classEntryNames: Set<String> by lazy {
        entryNames.stream()
            .filter(::isClassResourceName)
            .toSortedSet()
    }

    override val classNames: Set<String> by lazy {
        classEntryNames.stream()
            .map(::resourceNameToClassName)
            .toSortedSet()
    }

    override val manifestMainAttributes: Map<String, String> by lazy {
        mutableMapOf<String, String>().apply {
            readManifest()?.mainAttributes?.forEach { key, value ->
                if (key == null || value == null) return@forEach
                put(key.toString(), value.toString())
            }
        }.toMap()
    }

}
