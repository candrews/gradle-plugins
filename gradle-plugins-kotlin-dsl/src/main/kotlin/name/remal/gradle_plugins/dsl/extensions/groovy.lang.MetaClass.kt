package name.remal.gradle_plugins.dsl.extensions

import groovy.lang.MetaClass
import org.codehaus.groovy.runtime.DefaultGroovyMethods

fun MetaClass.addMixins(mixinClasses: List<Class<*>>) = DefaultGroovyMethods.mixin(this, mixinClasses)
fun MetaClass.addMixin(mixinClass: Class<*>) = addMixins(listOf(mixinClass))
