package name.remal.gradle_plugins.dsl.extensions

import name.remal.isPublic
import java.lang.reflect.Constructor

@Suppress("DEPRECATION")
fun <T> Constructor<T>.makeAccessible() = apply {
    if (isAccessible) return@apply
    if (!isPublic || !declaringClass.isPublic) {
        isAccessible = true
    }
}
