package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Action
import kotlin.reflect.KFunction1

fun <T> KFunction1<T, *>.toConfigureAction() = Action<T> { invoke(it) }
