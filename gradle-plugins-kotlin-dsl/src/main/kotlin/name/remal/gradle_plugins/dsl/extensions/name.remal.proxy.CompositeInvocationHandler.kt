package name.remal.gradle_plugins.dsl.extensions

import name.remal.proxy.CompositeInvocationHandler
import java.lang.reflect.Method

fun CompositeInvocationHandler.appendMethodHandler(name: String, vararg paramTypes: Class<*>, handler: (proxy: Any, method: Method, args: Array<Any?>) -> Any?) = appendMethodHandler(
    condition@{
        if (it.parameterCount != paramTypes.size) return@condition false
        if (it.name != name) return@condition false
        it.parameterTypes.forEachIndexed { index, type ->
            if (!type.isAssignableFrom(paramTypes[index])) return@condition false
        }
        return@condition true

    },
    handler@{ proxy, method, args ->
        return@handler handler(proxy, method, args ?: emptyArray())
    }
)
