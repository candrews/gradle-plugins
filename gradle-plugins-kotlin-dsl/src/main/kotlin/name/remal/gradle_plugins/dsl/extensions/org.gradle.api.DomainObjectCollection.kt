package name.remal.gradle_plugins.dsl.extensions

import name.remal.KotlinAllOpen
import org.gradle.api.DomainObjectCollection
import java.util.concurrent.atomic.AtomicBoolean

operator fun <T, S : T> DomainObjectCollection<T>.get(type: Class<S>): DomainObjectCollection<S> = withType(type)


fun <T, S : T> DomainObjectCollection<T>.all(type: Class<S>, configureAction: (S) -> Unit) = withType(type).all(configureAction)
fun <T, S : T> DomainObjectCollection<T>.forEach(type: Class<S>, configureAction: (S) -> Unit) = withType(type).forEach(configureAction)

fun <T, C : DomainObjectCollection<T>> C.useDefault(configurer: C.() -> Unit) {
    if (isNotEmpty()) return

    configurer(this)
    forEach { it?.addObjectMarker(DefaultObjectMarker::class.java) }

    val wasDefaultRemoved = AtomicBoolean(false)
    whenObjectAdded { obj ->
        if (wasDefaultRemoved.compareAndSet(false, true)) {
            toList().forEach {
                if (obj !== it) {
                    remove(it)
                }
            }
        }
    }
}

fun <T> DomainObjectCollection<T>.useDefault(vararg elements: T) {
    useDefault { elements.forEach { add(it) } }
}

fun <T, C : DomainObjectCollection<T>> C.useDefault(type: Class<out T>, configurer: C.() -> Unit) {
    if (any { type.isInstance(it) }) return

    configurer(this)
    forEach {
        if (it != null && type.isInstance(it)) {
            it.addObjectMarker(DefaultObjectMarker::class.java)
        }
    }

    val wasDefaultRemoved = AtomicBoolean(false)
    whenObjectAdded { obj ->
        if (type.isInstance(obj)) {
            if (wasDefaultRemoved.compareAndSet(false, true)) {
                toList().forEach {
                    if (type.isInstance(it) && obj !== it) {
                        remove(it)
                    }
                }
            }
        }
    }
}

fun <T, S : T> DomainObjectCollection<T>.useDefault(type: Class<T>, vararg elements: S) {
    useDefault(type) { elements.forEach { add(it) } }
}

fun DomainObjectCollection<*>.removeDefaultObjects() = removeIf { it.hasObjectMarker(DefaultObjectMarker::class.java) }

@KotlinAllOpen
private class DefaultObjectMarker : ObjectMarker


fun <T> DomainObjectCollection<T>.whenChanged(action: () -> Unit) {
    whenObjectAdded { action() }
    whenObjectRemoved { action() }
}
