package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_5_0
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_6_0
import name.remal.gradle_plugins.dsl.UpwardCompatibility
import org.gradle.api.JavaVersion

@UpwardCompatibility(GRADLE_VERSION_5_0)
val JavaVersion.isJava12: Boolean
    get() = majorVersion == "12"

@UpwardCompatibility(GRADLE_VERSION_5_0)
val JavaVersion.isJava12Compatible: Boolean
    get() = majorVersion.toInt() >= 12

@UpwardCompatibility(GRADLE_VERSION_6_0)
val JavaVersion.isJava13: Boolean
    get() = majorVersion == "13"

@UpwardCompatibility(GRADLE_VERSION_6_0)
val JavaVersion.isJava13Compatible: Boolean
    get() = majorVersion.toInt() >= 13
