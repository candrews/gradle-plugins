package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.NamedDomainObjectList

operator fun <T, S : T> NamedDomainObjectList<T>.get(type: Class<S>): NamedDomainObjectList<S> = withType(type)
