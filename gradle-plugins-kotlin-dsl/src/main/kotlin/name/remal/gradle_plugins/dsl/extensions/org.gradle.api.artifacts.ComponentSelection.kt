package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.ComponentSelection

val ComponentSelection.notation get() = candidate.notation

fun DependencyNotationMatcher.matches(selection: ComponentSelection) = matches(selection.candidate)
fun DependencyNotationMatcher.notMatches(selection: ComponentSelection) = !matches(selection.candidate)
