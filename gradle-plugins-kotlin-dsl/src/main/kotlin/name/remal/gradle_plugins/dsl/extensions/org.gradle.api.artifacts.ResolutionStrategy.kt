package name.remal.gradle_plugins.dsl.extensions

import name.remal.findCompatibleMethod
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.parseDependencyNotation
import org.gradle.api.Action
import org.gradle.api.artifacts.ResolutionStrategy
import java.util.concurrent.TimeUnit.DAYS
import java.util.concurrent.TimeUnit.SECONDS

fun ResolutionStrategy.selectHighestCapabilityVersion(group: String, module: String) {
    val capabilitiesResolution = javaClass.findCompatibleMethod("getCapabilitiesResolution")
        ?.apply { isAccessible = true }
        ?.invoke(this)
        ?: return
    capabilitiesResolution.invokeCompatibleMethod("withCapability", group, module, Action<Any> {
        it.invokeCompatibleMethod("selectHighestVersion")
    })
}

fun ResolutionStrategy.selectHighestCapabilityVersion(notation: Any) {
    val dependencyNotation: DependencyNotation = if (notation is DependencyNotation) {
        notation
    } else {
        parseDependencyNotation(notation.toString())
    }
    selectHighestCapabilityVersion(dependencyNotation.group, dependencyNotation.module)
}

fun ResolutionStrategy.cacheDynamicForever() {
    cacheChangingModulesFor(30, DAYS)
    cacheDynamicVersionsFor(30, DAYS)
}

fun ResolutionStrategy.disableCacheDynamic() {
    cacheChangingModulesFor(0, SECONDS)
    cacheDynamicVersionsFor(0, SECONDS)
}
