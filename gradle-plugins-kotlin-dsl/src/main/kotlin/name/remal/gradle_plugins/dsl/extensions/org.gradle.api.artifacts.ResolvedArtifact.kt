package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import name.remal.nullIf
import org.gradle.api.artifacts.DependencyArtifact
import org.gradle.api.artifacts.ResolvedArtifact

val ResolvedArtifact.notation
    get() = DependencyNotation(
        group = moduleVersion.id.group,
        module = moduleVersion.id.name,
        version = moduleVersion.id.version,
        classifier = classifier,
        extension = extension.nullIf { this == DependencyArtifact.DEFAULT_TYPE }
    )


fun DependencyNotationMatcher.matches(resolvedArtifact: ResolvedArtifact) = matches(resolvedArtifact.notation)
fun DependencyNotationMatcher.notMatches(resolvedArtifact: ResolvedArtifact) = !matches(resolvedArtifact)
