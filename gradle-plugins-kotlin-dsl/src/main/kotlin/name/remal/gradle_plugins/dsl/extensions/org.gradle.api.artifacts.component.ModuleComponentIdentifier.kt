package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.component.ModuleComponentIdentifier

val ModuleComponentIdentifier.notation
    get() = DependencyNotation(
        group = group,
        module = module,
        version = version
    )


fun DependencyNotationMatcher.matches(id: ModuleComponentIdentifier) = matches(id.notation)
fun DependencyNotationMatcher.notMatches(id: ModuleComponentIdentifier) = !matches(id)
