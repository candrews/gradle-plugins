package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.component.ModuleComponentSelector

val ModuleComponentSelector.notation
    get() = DependencyNotation(
        group = group,
        module = module,
        version = version
    )


fun DependencyNotationMatcher.matches(selector: ModuleComponentSelector) = matches(selector.notation)
fun DependencyNotationMatcher.notMatches(selector: ModuleComponentSelector) = !matches(selector)
