package name.remal.gradle_plugins.dsl.extensions

import groovy.lang.Closure
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.uncheckedCast
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.artifacts.dsl.DependencyHandler

private inline fun <reified T : Dependency> ((T) -> Unit).toClosure(ownerObj: Any): Closure<Unit> = object : Closure<Unit>(ownerObj, ownerObj) {
    override fun call(vararg args: Any?) {
        val firstArg = args.getOrNull(0)
        firstArg?.let { if (it is T) return this@toClosure(it) }

        val delegate = this.delegate
        delegate?.let { if (it is T) return this@toClosure(it) }

        val owner = this.owner
        owner?.let { if (it is T) return this@toClosure(it) }

        val thisObject = this.thisObject
        thisObject?.let { if (it is T) return this@toClosure(it) }

        throw IllegalStateException("All these candidates are not instance of ${T::class.java.name}:\n  args[0] = $firstArg\n  delegate = $delegate\n  owner = $owner\n  thisObject = $thisObject")
    }
}

fun DependencyHandler.create(notation: String, configurer: (dependency: ExternalModuleDependency) -> Unit = {}): ExternalModuleDependency {
    return create(notation, configurer.toClosure(this)).uncheckedCast()
}

fun DependencyHandler.add(configurationName: String, notation: String, configurer: (dependency: ExternalModuleDependency) -> Unit = {}): ExternalModuleDependency {
    return add(configurationName, notation, configurer.toClosure(this)).uncheckedCast()
}

fun DependencyHandler.add(configurationName: String, notation: Project, configurer: (dependency: ProjectDependency) -> Unit = {}): ProjectDependency {
    return add(configurationName, notation, configurer.toClosure(this)).uncheckedCast()
}

fun DependencyHandler.createFromNotation(notation: DependencyNotation, configurer: (dependency: ExternalModuleDependency) -> Unit = {}) = create(notation.toString(), configurer)
