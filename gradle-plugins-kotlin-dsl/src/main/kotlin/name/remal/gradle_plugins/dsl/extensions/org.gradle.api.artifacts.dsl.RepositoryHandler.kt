package name.remal.gradle_plugins.dsl.extensions

import name.remal.escapeRegex
import org.gradle.api.artifacts.ArtifactRepositoryContainer.DEFAULT_MAVEN_CENTRAL_REPO_NAME
import org.gradle.api.artifacts.ArtifactRepositoryContainer.MAVEN_CENTRAL_URL
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.ArtifactRepository
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.internal.artifacts.dsl.DefaultRepositoryHandler.BINTRAY_JCENTER_URL
import org.gradle.api.internal.artifacts.dsl.DefaultRepositoryHandler.DEFAULT_BINTRAY_JCENTER_REPO_NAME

private fun String?.hostEndsWith(host: String) = this != null && (this == host || endsWith(".$host"))

val RepositoryHandler.hasRepositoryWithDynamicVersionsSupport: Boolean
    get() = any { repo ->
        if (repo is MavenArtifactRepository) {
            val url = repo.url ?: return@any false
            val scheme = url.scheme
            val host = url.host
            if (scheme == "http" || scheme == "https") {
                if (host.hostEndsWith("bintray.com")
                    || host.hostEndsWith("jfrog.org")
                    || host.hostEndsWith("maven.org")
                    || host.hostEndsWith("maven.apache.org")
                    || host.hostEndsWith("sonatype.org")
                ) {
                    return@any true
                }
            }
        }
        return@any false
    }


fun RepositoryHandler.maven(repositoryName: String, repositoryUrl: Any? = null, configurer: (repository: MavenArtifactRepository) -> Unit = {}) =
    maven {
        it.name = repositoryName
        if (repositoryUrl != null) {
            it.setUrl(repositoryUrl)
        }
        it.apply(configurer)
    }


private val jcenterRepositoryNameRegex = Regex(escapeRegex(DEFAULT_BINTRAY_JCENTER_REPO_NAME) + "\\d*")
private val jcenterRepositoryUri = BINTRAY_JCENTER_URL
private val ArtifactRepository.isJCenter: Boolean
    get() = jcenterRepositoryNameRegex.matches(name)
        && this is MavenArtifactRepository
        && url.toString() == jcenterRepositoryUri

fun RepositoryHandler.forJCenter(configurer: (repository: MavenArtifactRepository) -> Unit) {
    withType(MavenArtifactRepository::class.java).matching(ArtifactRepository::isJCenter).all(configurer)
}

val RepositoryHandler.isJCenterAdded: Boolean
    get() = any(ArtifactRepository::isJCenter)


private val mavenCentralRepositoryNameRegex = Regex(escapeRegex(DEFAULT_MAVEN_CENTRAL_REPO_NAME) + "\\d*")
private val mavenCentralRepositoryUri = MAVEN_CENTRAL_URL
private val ArtifactRepository.isMavenCentral: Boolean
    get() = mavenCentralRepositoryNameRegex.matches(name)
        && this is MavenArtifactRepository
        && url.toString() == mavenCentralRepositoryUri

fun RepositoryHandler.forMavenCentral(configurer: (repository: MavenArtifactRepository) -> Unit) {
    withType(MavenArtifactRepository::class.java).matching(ArtifactRepository::isMavenCentral).all(configurer)
}

val RepositoryHandler.isMavenCentralAdded: Boolean
    get() = any(ArtifactRepository::isMavenCentral)


private fun RepositoryHandler.mavenCentralOrJCenterIfNotAdded(creator: (RepositoryHandler) -> MavenArtifactRepository) {
    if (isMavenCentralAdded || isJCenterAdded) return
    creator(this)
}

private fun RepositoryHandler.mavenCentralOrJCenterIfNotAdded(
    repositoryUrl: String,
    repositoryName: String,
    configurer: (repository: MavenArtifactRepository) -> Unit = {}
) {
    if (isMavenCentralAdded || isJCenterAdded) return

    if (mavenCentralRepositoryNameRegex.matches(repositoryName)) {
        throw IllegalArgumentException("Repository name can't match '$mavenCentralRepositoryNameRegex'")
    }
    if (jcenterRepositoryNameRegex.matches(repositoryName)) {
        throw IllegalArgumentException("Repository name can't match '$jcenterRepositoryNameRegex'")
    }

    maven(repositoryName, repositoryUrl, configurer)
}

fun RepositoryHandler.jcenterIfNotAdded() = mavenCentralOrJCenterIfNotAdded(RepositoryHandler::jcenter)

fun RepositoryHandler.jcenterIfNotAdded(
    repositoryName: String,
    configurer: (repository: MavenArtifactRepository) -> Unit = {}
) = mavenCentralOrJCenterIfNotAdded(BINTRAY_JCENTER_URL, repositoryName, configurer)

fun RepositoryHandler.mavenCentralIfNotAdded() = mavenCentralOrJCenterIfNotAdded(RepositoryHandler::mavenCentral)

fun RepositoryHandler.mavenCentralIfNotAdded(
    repositoryName: String,
    configurer: (repository: MavenArtifactRepository) -> Unit = {}
) = mavenCentralOrJCenterIfNotAdded(MAVEN_CENTRAL_URL, repositoryName, configurer)

fun RepositoryHandler.mavenCentralOrJCenterIfNotAdded() = mavenCentralIfNotAdded()

fun RepositoryHandler.mavenCentralOrJCenterIfNotAdded(
    repositoryName: String,
    configurer: (repository: MavenArtifactRepository) -> Unit = {}
) = mavenCentralIfNotAdded(repositoryName, configurer)
