package name.remal.gradle_plugins.dsl.extensions

import name.remal.findCompatibleMethod
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.warn
import org.gradle.api.Action
import org.gradle.api.artifacts.repositories.ArtifactRepository
import org.intellij.lang.annotations.Language
import kotlin.DeprecationLevel.ERROR

fun ArtifactRepository.includeGroup(group: String) = forContent { includeGroup(group) }
fun ArtifactRepository.includeGroupByRegex(@Language("RegExp") groupRegex: String) = forContent { includeGroupByRegex(groupRegex) }
fun ArtifactRepository.includeModule(group: String, module: String) = forContent { includeModule(group, module) }
fun ArtifactRepository.includeModule(notation: DependencyNotation) = includeModule(notation.group, notation.module)
fun ArtifactRepository.includeModuleByRegex(@Language("RegExp") groupRegex: String, @Language("RegExp") moduleRegex: String) = forContent { includeModuleByRegex(groupRegex, moduleRegex) }
fun ArtifactRepository.includeVersion(group: String, module: String, version: String) = forContent { includeVersion(group, module, version) }
fun ArtifactRepository.includeVersion(notation: DependencyNotation) = includeVersion(notation.group, notation.module, notation.version)
fun ArtifactRepository.includeVersionByRegex(
    @Language("RegExp") groupRegex: String,
    @Language("RegExp") moduleRegex: String,
    @Language("RegExp") versionRegex: String
) = forContent { includeVersionByRegex(groupRegex, moduleRegex, versionRegex) }

fun ArtifactRepository.excludeGroup(group: String) = forContent { excludeGroup(group) }
fun ArtifactRepository.excludeGroupByRegex(@Language("RegExp") groupRegex: String) = forContent { excludeGroupByRegex(groupRegex) }
fun ArtifactRepository.excludeModule(group: String, module: String) = forContent { excludeModule(group, module) }
fun ArtifactRepository.excludeModule(notation: DependencyNotation) = excludeModule(notation.group, notation.module)
fun ArtifactRepository.excludeModuleByRegex(@Language("RegExp") groupRegex: String, @Language("RegExp") moduleRegex: String) = forContent { excludeModuleByRegex(groupRegex, moduleRegex) }
fun ArtifactRepository.excludeVersion(group: String, module: String, version: String) = forContent { excludeVersion(group, module, version) }
fun ArtifactRepository.excludeVersion(notation: DependencyNotation) = excludeVersion(notation.group, notation.module, notation.version)

@Deprecated(
    message = "This doesn't work as expected, as it applies the logic of excludeModuleByRegex() first and only then filters by the version regex."
        + " Use includeVersionByRegex() with negative lookahead."
        + " Example: includeVersionByRegex(\".*\", \".*\", \"^[^-]+((?!-SNAPSHOT)-[^-]+)*\$\") is equal to includeVersionByRegex(\".*\", \".*\", \"^.+-SNAPSHOT(-.+)*\$\").",
    level = ERROR
)
fun ArtifactRepository.excludeVersionByRegex(
    @Language("RegExp") groupRegex: String,
    @Language("RegExp") moduleRegex: String,
    @Language("RegExp") versionRegex: String
) = forContent { excludeVersionByRegex(groupRegex, moduleRegex, versionRegex) }

private class ArtifactRepositoryContent(private val delegate: Any) {

    companion object {
        private val logger = getGradleLogger(ArtifactRepositoryContent::class.java)
    }

    fun includeGroup(group: String) {
        try {
            delegate.javaClass.getMethod("includeGroup", String::class.java)
                .invokeForInstance<Any?>(delegate, group)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun includeGroupByRegex(groupRegex: String) {
        try {
            delegate.javaClass.getMethod("includeGroupByRegex", String::class.java)
                .invokeForInstance<Any?>(delegate, groupRegex)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun includeModule(group: String, module: String) {
        try {
            delegate.javaClass.getMethod("includeModule", String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, group, module)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun includeModuleByRegex(groupRegex: String, moduleRegex: String) {
        try {
            delegate.javaClass.getMethod("includeModuleByRegex", String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, groupRegex, moduleRegex)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun includeVersion(group: String, module: String, version: String) {
        try {
            delegate.javaClass.getMethod("includeVersion", String::class.java, String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, group, module, version)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun includeVersionByRegex(groupRegex: String, moduleRegex: String, versionRegex: String) {
        try {
            delegate.javaClass.getMethod("includeVersionByRegex", String::class.java, String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, groupRegex, moduleRegex, versionRegex)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }


    fun excludeGroup(group: String) {
        try {
            delegate.javaClass.getMethod("excludeGroup", String::class.java)
                .invokeForInstance<Any?>(delegate, group)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun excludeGroupByRegex(groupRegex: String) {
        try {
            delegate.javaClass.getMethod("excludeGroupByRegex", String::class.java)
                .invokeForInstance<Any?>(delegate, groupRegex)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun excludeModule(group: String, module: String) {
        try {
            delegate.javaClass.getMethod("excludeModule", String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, group, module)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun excludeModuleByRegex(groupRegex: String, moduleRegex: String) {
        try {
            delegate.javaClass.getMethod("excludeModuleByRegex", String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, groupRegex, moduleRegex)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun excludeVersion(group: String, module: String, version: String) {
        try {
            delegate.javaClass.getMethod("excludeVersion", String::class.java, String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, group, module, version)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun excludeVersionByRegex(groupRegex: String, moduleRegex: String, versionRegex: String) {
        try {
            delegate.javaClass.getMethod("excludeVersionByRegex", String::class.java, String::class.java, String::class.java)
                .invokeForInstance<Any?>(delegate, groupRegex, moduleRegex, versionRegex)
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

}

private val contentMethod = ArtifactRepository::class.java.findCompatibleMethod("content", Action::class.java)

private fun ArtifactRepository.forContent(action: ArtifactRepositoryContent.() -> Unit) {
    contentMethod?.invokeForInstance<Any?>(this, Action<Any> { delegate ->
        ArtifactRepositoryContent(delegate).apply(action)
    })
}
