package name.remal.gradle_plugins.dsl.extensions

import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.file.ContentFilterable

fun ContentFilterable.replaceTokens(tokens: Map<String, String>, beginToken: String = "@", endToken: String = "@"): ContentFilterable {
    return filter(
        mapOf(
            "tokens" to tokens,
            "beginToken" to beginToken,
            "endToken" to endToken
        ),
        ReplaceTokens::class.java
    )
}
