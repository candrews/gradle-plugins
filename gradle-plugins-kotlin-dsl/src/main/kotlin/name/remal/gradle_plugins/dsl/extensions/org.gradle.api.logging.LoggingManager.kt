package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.logging.LogLevel
import org.gradle.api.logging.LogLevel.ERROR
import org.gradle.api.logging.LogLevel.LIFECYCLE
import org.gradle.api.logging.LoggingManager

inline fun <R> LoggingManager.withCapturedStandardOutput(outputLevel: LogLevel = LIFECYCLE, errorLevel: LogLevel = ERROR, action: () -> R): R {
    val prevOutputLevel = standardOutputCaptureLevel
    val prevErrorLevel = standardErrorCaptureLevel
    try {
        captureStandardOutput(outputLevel)
        captureStandardError(errorLevel)

        return action()

    } finally {
        captureStandardOutput(prevOutputLevel)
        captureStandardError(prevErrorLevel)
    }
}

