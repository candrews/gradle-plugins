package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.plugins.Convention
import java.lang.reflect.Type
import kotlin.DeprecationLevel.ERROR

fun Convention.addPlugin(name: String, plugin: Any) {
    plugins[name] = plugin
}

fun Convention.addPlugin(plugin: Any) {
    if (plugin is Type) {
        throw IllegalArgumentException("Instance of ${Type::class.java} can't be used as a plugin")
    }

    val baseName = "$$" + plugin.javaClass.name.replace('.', '$') + '$'
    var counter = 0
    while (true) {
        val name = baseName + (++counter)
        if (name !in plugins) {
            plugins[name] = plugin
            return
        }
    }
}

@Deprecated(level = ERROR, message = "Type can't be used as a plugin")
@Suppress("UNUSED_PARAMETER")
fun Convention.addPlugin(pluginType: Type) {
    throw UnsupportedOperationException()
}
