package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Plugin
import org.gradle.api.plugins.PluginCollection

operator fun <T : Plugin<*>, S : T> PluginCollection<T>.get(type: Class<S>): PluginCollection<S> = withType(type)
