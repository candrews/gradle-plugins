package name.remal.gradle_plugins.dsl.extensions

import name.remal.asString
import name.remal.setNoOpEntityResolver
import name.remal.setNoValidatingXMLReaderFactory
import org.gradle.api.publish.maven.MavenPom
import org.jdom2.Document
import org.jdom2.input.SAXBuilder
import java.io.StringReader

fun MavenPom.withJdomDocument(action: Document.() -> Unit) {
    withXml {
        val stringBuilder = it.asString()

        val saxBuilder = SAXBuilder().apply {
            setNoValidatingXMLReaderFactory()
            setNoOpEntityResolver()
        }
        val document = saxBuilder.build(StringReader(stringBuilder.toString()))
        action(document)

        stringBuilder.setLength(0)
        stringBuilder.append(document.asString(true))
    }
}
