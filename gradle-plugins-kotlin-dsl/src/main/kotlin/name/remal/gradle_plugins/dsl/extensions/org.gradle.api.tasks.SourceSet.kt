package name.remal.gradle_plugins.dsl.extensions

import name.remal.Services.loadServicesList
import name.remal.buildSet
import name.remal.fromLowerCamelToLowerHyphen
import name.remal.fromLowerHyphenToLowerCamel
import name.remal.gradle_plugins.api.AutoService
import name.remal.isStatic
import name.remal.nullIfEmpty
import org.gradle.api.Action
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.SourceSet
import java.lang.reflect.Method

fun SourceSet.getNounName(noun: String): String = getTaskName(null, noun)
fun SourceSet.getVerbName(verb: String): String = getTaskName(verb, null)


fun SourceSet.forEachSourceDirectorySet(action: (name: String, directorySet: SourceDirectorySet) -> Unit) {
    val processedNames = hashSetOf<String>()
    fun processObject(obj: Any) {
        obj::class.java.methods.asSequence()
            .filterNot(Method::isStatic)
            .filter { SourceDirectorySet::class.java.isAssignableFrom(it.returnType) }
            .filter { it.name.startsWith("get") }
            .filter { it.parameterCount == 0 }
            .onEach { it.isAccessible = true }
            .forEach {
                val tokenizedName = it.name.fromLowerCamelToLowerHyphen()
                val name = if (tokenizedName.startsWith("get-")) {
                    tokenizedName.substring(4).fromLowerHyphenToLowerCamel().nullIfEmpty() ?: it.name
                } else {
                    it.name
                }

                val directorySet = it.invoke(obj) as? SourceDirectorySet ?: return@forEach

                if (processedNames.add(name)) {
                    action(name, directorySet)
                }
            }
    }

    processObject(this)

    val convention = this.convention
    convention.plugins.values.forEach { processObject(it) }

    convention.extensionsSchema.forEach { schema ->
        convention.configure(schema.name, Action<Any> {
            processObject(it)
        })
    }
}


private val sourceSetConfigurationNameGettersFactories = loadServicesList(SourceSetConfigurationNameGettersFactory::class.java)
val SourceSet.sourceSetConfigurationNames: Set<String>
    get() = buildSet {
        sourceSetConfigurationNameGettersFactories.forEach {
            it.getters.forEach { getter ->
                getter(this@sourceSetConfigurationNames)?.let { add(it) }
            }
        }
    }

fun SourceSet.forEachCorrespondingConfigurationName(other: SourceSet, action: (thisConfigurationName: String, otherConfigurationName: String) -> Unit) {
    val processedNames = hashSetOf<String>()
    sourceSetConfigurationNameGettersFactories.forEach {
        it.getters.forEach forEachGetter@{ getter ->
            val thisConfigurationName = getter(this) ?: return@forEachGetter
            if (processedNames.add(thisConfigurationName)) {
                val otherConfigurationName = getter(other) ?: return@forEachGetter
                action(thisConfigurationName, otherConfigurationName)
            }
        }
    }
}


private val sourceSetTaskNameGettersFactories = loadServicesList(SourceSetTaskNameGettersFactory::class.java)
val SourceSet.sourceSetTaskNames: Set<String>
    get() = buildSet {
        sourceSetTaskNameGettersFactories.forEach {
            it.getters.forEach { getter ->
                getter(this@sourceSetTaskNames)?.let { add(it) }
            }
        }
    }


interface SourceSetConfigurationNameGettersFactory {
    val getters: List<SourceSet.() -> String?>
}

@AutoService
class DefaultSourceSetConfigurationNameGettersFactory : SourceSetConfigurationNameGettersFactory {
    companion object {
        private val staticGetters = SourceSet::class.java.methods.asSequence()
            .filter { !it.isStatic && String::class.java == it.returnType && 0 == it.parameterCount }
            .filter { it.name.startsWith("get") && it.name.endsWith("ConfigurationName") }
            .sortedBy(Method::getName)
            .map { it.apply { isAccessible = true } }
            .map { val getter: SourceSet.() -> String? = { it.invoke(this)?.toString() }; getter }
            .toList()
    }

    override val getters = staticGetters
}


interface SourceSetTaskNameGettersFactory {
    val getters: List<SourceSet.() -> String?>
}

@AutoService
class DefaultSourceSetTaskNameGettersFactory : SourceSetTaskNameGettersFactory {
    companion object {
        private val staticGetters = SourceSet::class.java.methods.asSequence()
            .filter { !it.isStatic && String::class.java == it.returnType && 0 == it.parameterCount }
            .filter { it.name.startsWith("get") && it.name.endsWith("TaskName") }
            .sortedBy(Method::getName)
            .map { it.apply { isAccessible = true } }
            .map { val getter: SourceSet.() -> String? = { it.invoke(this)?.toString() }; getter }
            .toList()
    }

    override val getters = staticGetters
}
