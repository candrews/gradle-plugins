package name.remal.gradle_plugins.dsl.extensions

import name.remal.uncheckedCast
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.tasks.SourceSetOutput

fun SourceSetOutput.addClassesDir(path: Any) = addClassesDirs(path)

fun SourceSetOutput.addClassesDirs(vararg paths: Any) {
    classesDirs
        .uncheckedCast<ConfigurableFileCollection>()
        .from(*paths)
}
