package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Task
import org.gradle.api.tasks.TaskCollection

operator fun <T : Task, S : T> TaskCollection<T>.get(type: Class<S>): TaskCollection<S> = withType(type)
