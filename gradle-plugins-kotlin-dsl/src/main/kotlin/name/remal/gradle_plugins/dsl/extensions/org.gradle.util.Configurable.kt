package name.remal.gradle_plugins.dsl.extensions

import groovy.lang.Closure
import org.gradle.util.Configurable
import org.gradle.util.ConfigureUtil

fun <T : Configurable<*>> T.configure(configureAction: (T) -> Unit): T {
    return ConfigureUtil.configure(object : Closure<Unit>(this, this) {
        override fun call(vararg args: Any?) {
            configureAction(this@configure)
        }
    }, this)
}
