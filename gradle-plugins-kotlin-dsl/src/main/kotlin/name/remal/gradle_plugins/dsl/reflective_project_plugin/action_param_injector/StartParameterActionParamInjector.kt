package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.api.AutoService
import org.gradle.StartParameter
import org.gradle.api.Project

@AutoService
class StartParameterActionParamInjector : ActionParamInjector<StartParameter>() {
    override fun createValue(project: Project): StartParameter = project.gradle.startParameter
}
