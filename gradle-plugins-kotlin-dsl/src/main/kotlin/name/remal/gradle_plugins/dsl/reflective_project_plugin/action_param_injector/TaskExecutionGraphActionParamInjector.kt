package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.api.AutoService
import org.gradle.api.Project
import org.gradle.api.execution.TaskExecutionGraph

@AutoService
class TaskExecutionGraphActionParamInjector : ActionParamInjector<TaskExecutionGraph>() {
    override fun createValue(project: Project): TaskExecutionGraph = project.gradle.taskGraph
}
