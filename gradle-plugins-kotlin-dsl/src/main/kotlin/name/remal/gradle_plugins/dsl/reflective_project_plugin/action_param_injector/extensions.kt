package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.dsl.extensions.invokeForInstance
import name.remal.gradle_plugins.dsl.reflective_project_plugin.info.ActionMethodInfo
import name.remal.gradle_plugins.dsl.reflective_project_plugin.info.ConditionMethodInfo
import org.gradle.api.Project
import java.lang.reflect.Method

private fun Method.invokeForProject(instance: Any, project: Project): Any? {
    val injectors = parameterTypes.map { type ->
        actionParamInjectors.firstOrNull { type.isAssignableFrom(it.paramType) }
            ?: throw IllegalStateException("${ActionParamInjector::class.java.simpleName} can't be found for $type")
    }
    val paramValues = injectors.map { it.createValue(project) }
    return invokeForInstance(instance, *paramValues.toTypedArray())
}

fun ConditionMethodInfo.invoke(pluginInstance: Any, project: Project): Boolean {
    return method.invokeForProject(pluginInstance, project) == true
}

fun ActionMethodInfo.invoke(actionsGroup: Any, project: Project) {
    method.invokeForProject(actionsGroup, project)
}
