package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

data class CreateExtensionInfo(
    val extensionClass: Class<*>,
    val description: String = "",
    val properties: List<ExtensionPropertyInfo> = listOf()
)
