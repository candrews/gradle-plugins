package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

interface WithPluginActions : WithConditionMethods, WithRequirements, WithApplyPlugins {
    val actionMethods: List<ActionMethodInfo>
    val actionsGroups: List<ActionsGroupInfo>

    val actions: List<ActionInfo>
        get() {
            return mutableListOf<ActionInfo>().apply {
                addAll(actionMethods)
                addAll(actionsGroups)
                sortWith(Comparator(ActionInfo::compareTo))
            }
        }

}
