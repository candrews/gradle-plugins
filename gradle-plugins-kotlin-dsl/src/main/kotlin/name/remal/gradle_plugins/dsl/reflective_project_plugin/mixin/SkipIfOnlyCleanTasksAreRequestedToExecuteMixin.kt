package name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin

import name.remal.gradle_plugins.dsl.PluginCondition
import org.gradle.StartParameter
import org.gradle.api.plugins.BasePlugin.CLEAN_TASK_NAME

interface SkipIfOnlyCleanTasksAreRequestedToExecuteMixin {

    @PluginCondition
    fun StartParameter.`Skip if only 'clean' tasks are requested to execute`(): Boolean {
        return taskRequests.isEmpty() || taskRequests.any {
            it.args.isEmpty() || it.args.any {
                CLEAN_TASK_NAME != it
            }
        }
    }

}
