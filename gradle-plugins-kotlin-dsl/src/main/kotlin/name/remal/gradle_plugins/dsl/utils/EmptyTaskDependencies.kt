package name.remal.gradle_plugins.dsl.utils

import org.gradle.api.Task
import org.gradle.api.tasks.TaskDependency

object EmptyTaskDependencies : TaskDependency {
    override fun getDependencies(task: Task?): Set<Task> = emptySet()
}
