package name.remal.gradle_plugins.dsl.utils

import kotlin.reflect.KProperty

fun <T> defaultValue(defaultValueGetter: () -> T) = PropertyWithDefaultValue(defaultValueGetter)
fun <T> defaultValue(defaultValue: T) = PropertyWithDefaultValue({ defaultValue })

class PropertyWithDefaultValue<T>(private val defaultValueGetter: () -> T) {

    private var value: T? = null

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value ?: defaultValueGetter()
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = value
    }

}
