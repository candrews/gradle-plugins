package name.remal.gradle_plugins.dsl.utils

import java.util.concurrent.atomic.AtomicBoolean
import kotlin.reflect.KProperty

fun <T : Any> writeOnce() = WriteOnceProperty<T>()
fun <T : Any> writeOnce(onWrite: (value: T) -> Unit) = WriteOnceProperty<T>(onWrite)

class WriteOnceProperty<T : Any>(private val onWrite: (value: T) -> Unit) {

    constructor() : this({})

    private val isInitialized = AtomicBoolean(false)

    private lateinit var value: T

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        if (isInitialized.compareAndSet(false, true)) {
            this.value = value
            onWrite(value)
        } else {
            throw IllegalStateException("Property ${property.name} has already been initialized")
        }
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value
    }

}
