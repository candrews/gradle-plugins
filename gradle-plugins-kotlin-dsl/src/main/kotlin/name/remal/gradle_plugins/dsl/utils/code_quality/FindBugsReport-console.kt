package name.remal.gradle_plugins.dsl.utils.code_quality

import name.remal.buildList
import name.remal.default
import name.remal.nullIf
import name.remal.nullIfEmpty
import java.io.File

fun FindBugsReport.createConsoleMessages(projectDir: File? = null): List<String> {
    if (bugs.isEmpty()) return emptyList()

    return buildList {
        add(buildString {
            append(bugs.size)

            tool?.let {
                append(' ')
                append(it)
            }

            append(' ')
            append("violations were found")
        })

        val srcDirs = projectDir?.let { projectDir ->
            project?.srcDirs.default(emptySet()).asSequence()
                .mapNotNull { projectDir.resolve(it) }
                .mapNotNull(File::getAbsoluteFile)
                .distinct()
                .toList()
        }

        sortedBugs.forEach { bug ->
            add(buildString {
                bug.rank.nullIf { this <= 0 }?.let { append("[rank ").append(it).append(']') }

                append(' ')
                append('[')
                sequenceOf(
                    bug.category?.let(categories::get)?.textDescription ?: bug.category,
                    bug.type
                ).filterNotNull().joinTo(this, " | ")
                append(']')

                bug.location?.let { location ->
                    val locationSuffix = buildString {
                        val startLine = location.startLine.nullIf { this <= 0 }
                        if (startLine != null) {
                            append(':').append(startLine)
                            location.startLineOffset.nullIf { this <= 0 }?.let { append(':').append(it) }
                        }
                    }

                    val sourceFile = location.sourceFile.nullIfEmpty()
                    if (sourceFile != null && srcDirs != null) {
                        append(' ')
                        val absolutePath = srcDirs.asSequence()
                            .map { it.resolve(sourceFile) }
                            .filter(File::isFile)
                            .mapNotNull(File::getAbsolutePath)
                            .firstOrNull()
                        append(absolutePath ?: sourceFile)
                        append(locationSuffix)
                        return@let
                    }

                    val className = location.className.nullIfEmpty()
                    if (className != null) {
                        append(' ')
                        append(className)
                        append(locationSuffix)
                        return@let
                    }
                }

                append("\n")
                sequenceOf(
                    bug.message.indent(),
                    bug.type?.let(types::get)?.let(FindBugsType::textDescription).indent()
                ).filterNotNull().distinct().joinTo(this, "\n\n")
            })
        }
    }
}


private val severalNewLinesRegex = Regex("\\n{3,}")
private fun String?.indent(): String? {
    if (this == null) return null
    return splitToSequence('\n')
        .map(String::trimEnd)
        .map {
            if (it.isNotBlank()) {
                "  $it"
            } else {
                ""
            }
        }
        .joinToString("\n")
        .replace(severalNewLinesRegex, "\n\n")
}
