package name.remal.gradle_plugins.dsl.utils

import net.htmlparser.jericho.Source
import java.lang.Math.min

fun htmlToText(html: String, maxLength: Int = 98): String = Source(html).renderer
    .setMaxLineLength(maxLength)
    .setHRLineLength(min(maxLength, 40))
    .setConvertNonBreakingSpaces(true)
    .setIncludeFirstElementTopMargin(false)
    .setIncludeHyperlinkURLs(true)
    .setIncludeAlternateText(true)
    .setNewLine("\n")
    .setBlockIndentSize(2)
    .setListIndentSize(2)
    .setTableCellSeparator(" | ")
    .toString()
    .trim('\n')
