package name.remal.gradle_plugins.dsl.utils

import com.google.common.base.FinalizablePhantomReference
import com.google.common.base.FinalizableReferenceQueue
import name.remal.concurrentSetOf
import name.remal.gradle_plugins.dsl.extensions.GRADLE
import org.gradle.api.invocation.Gradle
import java.io.Closeable
import java.lang.ref.Reference
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean

private val closeableReferences = concurrentSetOf<Reference<out Closeable>>()

private val finalizableReferenceQueue = FinalizableReferenceQueue().also { queue ->
    Runtime.getRuntime().addShutdownHook(Thread { queue.close() })
}

private val gradleHolder: Gradle? by lazy { GRADLE }

fun <T : Closeable> registerCloseable(closeable: T): T {
    val isClosed = AtomicBoolean(false)

    val weakReference = WeakReference(closeable)
    gradleHolder?.buildFinished { _ ->
        if (isClosed.compareAndSet(false, true)) {
            weakReference.get()?.apply {
                fileLogger.debug("Closing {}: {}", javaClass.name, this)
                try {
                    close()
                } catch (e: Exception) {
                    fileLogger.error("Error occurred while closing ${javaClass.name}: $this: $e", e)
                }
            }
        }
    }


    closeableReferences.add(object : FinalizablePhantomReference<T>(closeable, finalizableReferenceQueue) {
        override fun finalizeReferent() {
            if (isClosed.compareAndSet(false, true)) {
                closeable.apply {
                    fileLogger.debug("Closing {}: {}", javaClass.name, this)
                    try {
                        close()
                    } catch (e: Exception) {
                        fileLogger.error("Error occurred while closing ${javaClass.name}: $this: $e", e)
                    }
                }
            }
            closeableReferences.remove(this)
        }
    })

    return closeable
}

fun <T> registerIfCloseable(obj: T): T {
    if (obj is Closeable) {
        registerCloseable(obj)
    }
    return obj
}


private val fileLogger = getGradleLoggerForCurrentClass()
