package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.concurrent.atomic.AtomicInteger

class ConfigurationExtensionsTest : BaseProjectTest() {

    @Test
    fun beforeResolve() {
        val executionsCount = AtomicInteger(0)
        val conf = project.configurations.createWithUniqueName()
        conf.beforeResolve { executionsCount.incrementAndGet() }
        assertEquals(0, executionsCount.get())
        conf.copy().resolve()
        assertEquals(0, executionsCount.get())
        conf.resolve()
        assertEquals(1, executionsCount.get())
    }

}
