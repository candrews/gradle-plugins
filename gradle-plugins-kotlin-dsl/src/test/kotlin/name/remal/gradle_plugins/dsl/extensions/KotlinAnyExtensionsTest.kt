package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertNotNull
import org.junit.Test

class KotlinAnyExtensionsTest : BaseProjectTest() {

    @Test
    fun conventionProjectCompatibility() {
        assertNotNull((project as Any).convention)
    }

    @Test
    fun conventionTaskCompatibility() {
        val task = project.tasks.create("testTask")
        assertNotNull((task as Any).convention)
    }

    @Test
    fun conventionOtherCompatibility() {
        assertNotNull((project.repositories as Any).convention)
    }

}
