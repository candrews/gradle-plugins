package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class PluginManagerExtensionsTest : BaseProjectTest() {

    @Test
    fun `test pluginContainer`() {
        assertThat(project.pluginManager.pluginContainer).isNotNull
    }

}
