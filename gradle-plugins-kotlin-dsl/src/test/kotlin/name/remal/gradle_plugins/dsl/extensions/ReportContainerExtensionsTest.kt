package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.Task
import org.gradle.api.reporting.ReportContainer
import org.gradle.api.reporting.SingleFileReport
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import javax.inject.Inject

class ReportContainerExtensionsTest : BaseProjectTest() {

    @Test
    fun taskReportContainerClass() {
        val ctor = TestReports::class.java.taskReportContainerClass.getConstructor(Task::class.java)
        assertTrue(ctor.isAnnotationPresent(Inject::class.java))

        val task = project.tasks.createWithOptionalUniqueSuffix("test")
        val reports = project.extensions.create(TestReports::class.java, "reports", TestReports::class.java.taskReportContainerClass, task)
        assertEquals("html", reports.html.name)
        assertEquals("xml", reports.xml.name)
    }

    interface TestReports : ReportContainer<SingleFileReport> {
        val html: SingleFileReport
        val xml: SingleFileReport
    }

}
