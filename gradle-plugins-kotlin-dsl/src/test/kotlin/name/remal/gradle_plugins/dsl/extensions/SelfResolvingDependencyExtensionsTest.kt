package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.uncheckedCast
import org.gradle.api.artifacts.SelfResolvingDependency
import org.junit.Assert.assertNull
import org.junit.Test

class SelfResolvingDependencyExtensionsTest : BaseProjectTest() {

    @Test
    fun targetComponentId() {
        val dependency = project.dependencies.create(project.files()).uncheckedCast<SelfResolvingDependency>()
        val targetComponentId = dependency.targetComponentId
        assertNull(targetComponentId)
    }

}
