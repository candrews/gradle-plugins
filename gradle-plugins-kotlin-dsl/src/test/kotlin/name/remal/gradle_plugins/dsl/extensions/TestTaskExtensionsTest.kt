package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class TestTaskExtensionsTest : BaseProjectTest() {

    @Test
    fun testIsTestFrameworkSet() {
        val testTask = project.tasks.createWithUniqueName(org.gradle.api.tasks.testing.Test::class.java)
        assertThat(testTask.isTestFrameworkSet).`as`("testTask.isTestFrameworkSet").isFalse()
        testTask.useJUnit()
        assertThat(testTask.isTestFrameworkSet).`as`("testTask.isTestFrameworkSet").isTrue()
    }

}
