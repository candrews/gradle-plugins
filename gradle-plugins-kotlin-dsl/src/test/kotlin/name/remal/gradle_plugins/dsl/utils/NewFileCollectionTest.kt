package name.remal.gradle_plugins.dsl.utils

import name.remal.createParentDirectories
import name.remal.newTempDir
import org.gradle.internal.nativeintegration.services.NativeServices
import org.gradle.wrapper.GradleUserHomeLookup.gradleUserHome
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test
import java.io.File

class NewFileCollectionTest {

    @Test
    fun getSingleFile() {
        assertThrows(IllegalStateException::class.java, newFileCollection()::getSingleFile)
        assertEquals(File("1"), newFileCollection(File("1")).singleFile)
        assertEquals(File("1"), newFileCollection(File("1"), File("1")).singleFile)
        assertThrows(IllegalStateException::class.java, newFileCollection(File("1"), File("2"))::getSingleFile)
    }

    @Test
    fun getFiles() {
        assertEquals(
            setOf(File("1"), File("2")),
            newFileCollection { listOf(File("1"), File("2"), File("1"), File("2")) }.files
        )
    }

    @Test
    fun contains() {
        assertTrue(File("1") in newFileCollection(File("1")))
        assertFalse(File("2") in newFileCollection(File("1")))
    }

    @Test
    fun getAsPath() {
        assertEquals("", newFileCollection().asPath)
        assertEquals("dir\\1", newFileCollection(File("dir\\1")).asPath)
        assertEquals("1${File.pathSeparator}2", newFileCollection(File("1"), File("2")).asPath)
    }

    @Test
    fun plus() {
        assertEquals(
            setOf(File("1"), File("2")),
            (newFileCollection(File("1")) + newFileCollection(File("2"))).files
        )
    }

    @Test
    fun minus() {
        assertEquals(
            setOf(File("1")),
            (newFileCollection(File("1"), File("2")) - newFileCollection(File("2"))).files
        )
    }

    @Test
    fun filter() {
        assertEquals(
            setOf(File("1")),
            newFileCollection(File("1"), File("2")).filter { it.name == "1" }.files
        )
    }

    @Test
    fun isEmpty() {
        assertTrue(newFileCollection().isEmpty)
        assertFalse(newFileCollection(File("1")).isEmpty)
    }

    @Test
    fun getAsFileTree() {
        val tempDir = newTempDir().apply {
            resolve("1/2").createParentDirectories().createNewFile()
        }

        val visitedPaths = mutableListOf<String>()
        newFileCollection(tempDir).asFileTree.visit { visitedPaths.add(it.path) }
        assertEquals(listOf("1", "1/2"), visitedPaths)
    }

    @Test
    fun toStringMethod() {
        assertEquals("file collection", newFileCollection().toString())
    }

    @Test
    fun getBuildDependencies() {
        assertNotNull(newFileCollection().buildDependencies)
    }


    private fun <E : Throwable, R> assertThrows(exceptionType: Class<E>, action: () -> R) {
        try {
            action()
            fail("Should throw ${exceptionType.name}")

        } catch (e: Throwable) {
            if (exceptionType.isInstance(e)) {
                // OK
            } else {
                fail("Should throw ${exceptionType.name}, but another exception has been thrown: ${e.javaClass.name}")
            }
        }
    }


    init {
        NativeServices.initialize(gradleUserHome(), false)
    }

}
