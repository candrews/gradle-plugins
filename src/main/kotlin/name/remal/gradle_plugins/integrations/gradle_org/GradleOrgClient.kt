package name.remal.gradle_plugins.integrations.gradle_org

import name.remal.gradle_plugins.utils.create
import name.remal.gradle_plugins.utils.newJsonRetrofitBuilder

private val gradleOrgServices = newJsonRetrofitBuilder()
    .baseUrl("https://services.gradle.org/")
    .create(GradleOrgServices::class.java)

object GradleOrgClient : GradleOrgServices by gradleOrgServices
