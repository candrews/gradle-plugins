package name.remal.gradle_plugins.plugins.assertj

import name.remal.CLASS_FILE_NAME_SUFFIX
import name.remal.classNameToResourceName
import name.remal.forInstantiatedWithPropagatedPackage
import name.remal.forceDeleteRecursively
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.forClassLoader
import name.remal.gradle_plugins.dsl.extensions.generatedSourcesDir
import name.remal.gradle_plugins.dsl.extensions.isFile
import name.remal.gradle_plugins.dsl.extensions.java
import name.remal.gradle_plugins.dsl.extensions.main
import name.remal.gradle_plugins.dsl.extensions.requirePlugin
import name.remal.gradle_plugins.dsl.extensions.visitFiles
import name.remal.gradle_plugins.plugins.assertj.internal.AssertionGeneratorInvoker
import name.remal.gradle_plugins.plugins.assertj.internal.impl.AssertionGeneratorInvokerImpl
import name.remal.resourceNameToClassName
import org.gradle.api.DefaultTask
import org.gradle.api.file.FileCollection
import org.gradle.api.file.FileTree
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Classpath
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity.ABSOLUTE
import org.gradle.api.tasks.SkipWhenEmpty
import org.gradle.api.tasks.TaskAction
import java.io.File

@BuildTask
@CacheableTask
class AssertJGenerate : DefaultTask() {

    init {
        requirePlugin(AssertJGeneratorPlugin::class.java)

        description = "Generate AssertJ assertions"
    }

    @Input
    @get:Optional
    var classNames: MutableSet<String> = mutableSetOf()

    fun className(vararg classNames: String) {
        this.classNames.addAll(classNames)
    }

    fun className(classNames: Iterable<String>) {
        this.classNames.addAll(classNames)
    }

    @Input
    @get:Optional
    var packageNames: MutableSet<String> = mutableSetOf()

    fun packageName(vararg packageNames: String) {
        this.packageNames.addAll(packageNames)
    }

    fun packageName(packageNames: Iterable<String>) {
        this.packageNames.addAll(packageNames)
    }

    @Input
    @get:Optional
    var includes: MutableSet<String> = mutableSetOf()

    fun include(vararg includes: String) {
        this.includes.addAll(includes)
    }

    fun include(includes: Iterable<String>) {
        this.includes.addAll(includes)
    }

    @Input
    @get:Optional
    var excludes: MutableSet<String> = mutableSetOf()

    fun exclude(vararg excludes: String) {
        this.excludes.addAll(excludes)
    }

    fun exclude(excludes: Iterable<String>) {
        this.excludes.addAll(excludes)
    }

    @get:Optional
    @get:InputFiles
    @get:Classpath
    protected val compileClasspath: FileCollection
        get() = project.java.sourceSets.main.compileClasspath

    private val classesDirs: FileCollection get() = project.java.sourceSets.main.output.classesDirs

    @get:InputFiles
    @get:PathSensitive(ABSOLUTE)
    @get:SkipWhenEmpty
    val classFiles: FileTree
        get() = classesDirs.asFileTree
            .matching { classNames.forEach { className -> it.include(classNameToResourceName(className)) } }
            .matching { packageNames.forEach { packageName -> it.include((packageName.replace('.', '/') + "/**").trim('/')) } }
            .matching { it.include(includes); it.exclude(excludes) }

    @get:InputFiles
    @get:Classpath
    lateinit var assertjGeneratorClasspath: FileCollection

    @get:OutputDirectory
    val outputDir: File
        get() = project.generatedSourcesDir.resolve(name)

    @TaskAction
    protected fun doGenerate() {
        outputDir.forceDeleteRecursively()
        didWork = true

        val classNames = mutableListOf<String>().apply {
            classFiles.visitFiles {
                if (!it.isFile || !it.name.endsWith(CLASS_FILE_NAME_SUFFIX)) return@visitFiles
                val className = resourceNameToClassName(it.relativePath.pathString)
                if (className.substringAfterLast('.') in setOf("package-info", "module-info")) return@visitFiles
                add(className)
            }
            if (isEmpty()) return
        }

        assertjGeneratorClasspath.forClassLoader { assertjClassLoader ->
            assertjClassLoader.forInstantiatedWithPropagatedPackage(AssertionGeneratorInvoker::class.java, AssertionGeneratorInvokerImpl::class.java) { invoker ->
                invoker.invoke(classesDirs, classNames, outputDir, compileClasspath, logger)
            }
        }
    }

}

