package name.remal.gradle_plugins.plugins.assertj.internal.impl

import name.remal.filterNotNull
import name.remal.forThreadContextClassLoader
import name.remal.gradle_plugins.dsl.extensions.forClassLoader
import name.remal.gradle_plugins.plugins.assertj.internal.AssertionGeneratorInvoker
import name.remal.isPublic
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.THIS_ONLY
import name.remal.toList
import org.assertj.assertions.generator.BaseAssertionGenerator
import org.assertj.assertions.generator.description.converter.ClassToClassDescriptionConverter
import org.gradle.api.logging.Logger
import java.io.File

internal class AssertionGeneratorInvokerImpl : AssertionGeneratorInvoker {
    override fun invoke(classesDirs: Iterable<File>, classNames: List<String>, outputDir: File, classpath: Iterable<File>, logger: Logger) {

        val contextClassLoader = Thread.currentThread().contextClassLoader
        (classesDirs + classpath).forClassLoader(THIS_ONLY) { classesClassLoader ->
            forThreadContextClassLoader(contextClassLoader) {

                val classes: List<Class<*>> = classNames.stream()
                    .map { className ->
                        try {
                            return@map Class.forName(className, false, classesClassLoader)
                        } catch (e: Throwable) {
                            logger.warn("Class can't be loaded: $className", e)
                            return@map null
                        }
                    }
                    .filterNotNull()
                    .filter { it.isPublic && !it.isAnonymousClass && !it.isLocalClass }
                    .toList()
                    .apply { if (isEmpty()) return@forClassLoader }

                val generator = BaseAssertionGenerator()
                generator.setDirectoryWhereAssertionFilesAreGenerated(outputDir)

                val classDescriptionConverter = ClassToClassDescriptionConverter()
                classes.forEach { clazz ->
                    logger.info("Generating assertions for class : {}", clazz)
                    val classDesc = classDescriptionConverter.convertToClassDescription(clazz)
                    val generatedFiles = generator.generateHierarchicalCustomAssertionFor(classDesc, mutableSetOf())
                    logger.info("Generated {} hierarchical assertions files: {}", generatedFiles.toList())
                }

            }
        }

    }
}
