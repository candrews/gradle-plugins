package name.remal.gradle_plugins.plugins.check_updates

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.isParentProjectTaskWithSameNameInGraph
import name.remal.gradle_plugins.dsl.extensions.skipIfOffline
import name.remal.gradle_plugins.integrations.gradle_org.GradleOrgClient
import org.gradle.api.DefaultTask
import org.gradle.api.plugins.HelpTasksPlugin.HELP_GROUP
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskContainer
import org.gradle.util.GradleVersion

const val CHECK_GRADLE_UPDATES_TASK_NAME = "checkGradleUpdates"

@Deprecated(message = "Use automatic dependency updates software like Renovate, Dependabot, etc...")
@Plugin(
    id = "name.remal.check-gradle-updates",
    description = "Plugin that provides task for discovering Gradle updates",
    tags = ["versions", "gradle-updates"]
)
class CheckGradleUpdatesPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun TaskContainer.`Create checkGradleUpdates task`() {
        create(CHECK_GRADLE_UPDATES_TASK_NAME, CheckGradleUpdates::class.java)
    }

}


@BuildTask
class CheckGradleUpdates : DefaultTask() {

    init {
        group = HELP_GROUP
        description = "Displays a message if there is newer version of Gradle"

        onlyIf { !it.isParentProjectTaskWithSameNameInGraph }

        skipIfOffline()
    }

    @TaskAction
    protected fun doCheckGradleUpdates() {
        try {
            val currentVersion = GradleVersion.current()
            logger.info("Current Gradle version: {}", currentVersion.version)

            val info = GradleOrgClient.getCurrentVersion()
            logger.info("Latest Gradle version: {}", info.version)

            if (currentVersion < GradleVersion.version(info.version)) {
                logger.lifecycle("New Gradle version is available: {} (downloadUrl: {})", info.version, info.downloadUrl)
            }

            didWork = true

        } catch (e: Exception) {
            logger.error(e.message, e)
        }
    }

}
