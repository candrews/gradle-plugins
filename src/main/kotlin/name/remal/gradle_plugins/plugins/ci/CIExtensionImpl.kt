package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import java.util.Queue
import java.util.concurrent.LinkedBlockingQueue

@Extension
class CIExtensionImpl : CIExtension {

    companion object {
        @JvmStatic
        private val logger = getGradleLogger(CIExtension::class.java)
    }


    @Volatile
    override var isBuildOnCI: Boolean = isBuildOnCiDefault
        set(value) {
            if (field != value) {
                if (!value) {
                    throw IllegalStateException(CIExtension::isBuildOnCI.name + " can't be changed from true to false")
                }

                logger.lifecycle("CI: {}: {}", CIExtension::isBuildOnCI.name, value)

                if (value) {
                    while (true) {
                        val action = forBuildOnCIActions.poll() ?: break
                        action.run()
                    }
                }
            }
            field = value
        }

    private var forBuildOnCIActions: Queue<Runnable> = LinkedBlockingQueue()

    override fun forBuildOnCI(action: Runnable) {
        if (isBuildOnCI) {
            action.run()
        } else {
            forBuildOnCIActions.add(action)
        }
    }


    override fun forBuildOnLocal(action: Runnable) {
        if (isBuildOnLocal) {
            action.run()
        }
    }


    override var pipelineId: String? = null
        set(value) {
            if (value != null && field != value) {
                logger.lifecycle("CI: {}: {}", CIExtension::pipelineId.name, value)
            }
            field = value
        }

    override var buildId: String? = null
        set(value) {
            if (value != null && field != value) {
                logger.lifecycle("CI: {}: {}", CIExtension::buildId.name, value)
            }
            field = value
        }

    override var stageName: String? = null
        set(value) {
            if (value != null && field != value) {
                logger.lifecycle("CI: {}: {}", CIExtension::stageName.name, value)
            }
            field = value
        }

    override var jobName: String? = null
        set(value) {
            if (value != null && field != value) {
                logger.lifecycle("CI: {}: {}", CIExtension::jobName.name, value)
            }
            field = value
        }

}


/**
 * See: [https://codecov.io/bash]
 */
private val isBuildOnCiDefault: Boolean = sequenceOf(
    // General
    "CI",

    // Jenkins CI
    "JENKINS_HOME",
    "JENKINS_URL",

    // AWS Codebuild CI
    "CODEBUILD_CI",

    // Codefresh CI
    "CF_BUILD_URL",
    "CF_BUILD_ID",

    // TeamCity CI
    "TEAMCITY_VERSION",

    // Circle CI
    "CIRCLECI",

    // Bitrise CI
    "BITRISE_IO",

    // Buildkite CI
    "BUILDKITE",

    // Heroku CI
    "HEROKU_TEST_RUN_BRANCH",

    // Wercker CI
    "WERCKER_GIT_BRANCH",

    // GitLab CI
    "GITLAB_CI",

    // GitHub Actions
    "GITHUB_ACTION",

    // Azure Pipelines
    "SYSTEM_TEAMFOUNDATIONSERVERURI",

    // Bitbucket Pipelines
    "BITBUCKET_BUILD_NUMBER",

    // Cirrus CI
    "CIRRUS_CI"
)
    .map(System::getenv)
    .filterNotNull()
    .map(String::trim)
    .filter(String::isNotEmpty)
    .filterNot { it == "0" }
    .filterNot { it.equals("false", ignoreCase = false) }
    .any()
