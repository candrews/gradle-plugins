package name.remal.gradle_plugins.plugins.ci

import groovy.lang.Closure
import name.remal.gradle_plugins.dsl.Extension

@Extension
class CIMethodsExtension(private val ci: CIExtension) {

    fun forBuildOnCI(action: Runnable) = ci.forBuildOnCI(action)
    fun forBuildOnCI(action: Closure<*>) = ci.forBuildOnCI(action)

    fun forBuildOnLocal(action: Runnable) = ci.forBuildOnLocal(action)
    fun forBuildOnLocal(action: Closure<*>) = ci.forBuildOnLocal(action)

}
