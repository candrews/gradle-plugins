package name.remal.gradle_plugins.plugins.classes_processing.processors.build_time_constants

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.dsl.extensions.toHasEntries
import org.gradle.api.tasks.compile.AbstractCompile

@AutoService
class BuildTimeConstantsClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {

    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.classpath.toHasEntries().containsClass(BuildTimeConstants::class.java)) return emptyList()
        return listOf(
            BuildTimeConstantsClassesProcessor(compileTask.project),
            BuildTimeConstantsClassesValidation()
        )
    }

}
