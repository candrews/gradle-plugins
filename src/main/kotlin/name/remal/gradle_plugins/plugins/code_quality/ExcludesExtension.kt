package name.remal.gradle_plugins.plugins.code_quality

import name.remal.gradle_plugins.dsl.Extension

@Extension
class ExcludesExtension {

    var classNames: MutableSet<String> = sortedSetOf()
        set(value) {
            field = value.toSortedSet()
        }

    fun className(value: String) = classNames(value)

    fun classNames(vararg values: String) {
        classNames.addAll(values)
    }

    fun classNames(values: Iterable<String>) {
        classNames.addAll(values)
    }


    var sources: MutableSet<String> = sortedSetOf()
        set(value) {
            field = value.toSortedSet()
        }

    fun source(value: String) = sources(value)

    fun sources(vararg values: String) {
        sources.addAll(values)
    }

    fun sources(values: Iterable<String>) {
        sources.addAll(values)
    }


    var messages: MutableSet<String> = sortedSetOf()
        set(value) {
            field = value.toSortedSet()
        }

    fun message(value: String) = messages(value)

    fun messages(vararg values: String) {
        messages.addAll(values)
    }

    fun messages(values: Iterable<String>) {
        messages.addAll(values)
    }


    fun kotlin() {
        sources.add("*.kt")
        sources.add("*.kts")
    }

    fun groovy() {
        sources.add("*.groovy")
    }

}
