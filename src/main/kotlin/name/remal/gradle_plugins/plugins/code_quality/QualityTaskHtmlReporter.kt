package name.remal.gradle_plugins.plugins.code_quality

import org.gradle.api.Task

interface QualityTaskHtmlReporter<TaskType : Task> : QualityTaskReporter<TaskType> {

    fun writeHtmlReportFileOf(task: TaskType)

}
