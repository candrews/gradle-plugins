package name.remal.gradle_plugins.plugins.code_quality

import com.google.common.reflect.TypeToken
import name.remal.asClass
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.trace
import name.remal.uncheckedCast
import org.gradle.api.Task
import java.lang.reflect.ParameterizedType

interface QualityTaskReporter<TaskType : Task> {

    val isEnabled: Boolean
        get() {
            try {
                taskType
                return true
            } catch (e: Throwable) {
                getGradleLogger(this.javaClass).trace(e)
                return false
            }
        }

    val taskType: Class<TaskType>
        get() {
            val type = TypeToken.of(this.javaClass).getSupertype(QualityTaskReporter::class.java).type
            if (type !is ParameterizedType) throw IllegalStateException("$type is not instance of ParameterizedType")
            return type.actualTypeArguments[0].asClass().uncheckedCast()
        }

    fun configureTask(task: TaskType) {
        // do nothing by default
    }

}
