package name.remal.gradle_plugins.plugins.code_quality.checkstyle

import name.remal.gradle_plugins.dsl.PluginId

object CheckstylePluginId : PluginId("checkstyle")
