package name.remal.gradle_plugins.plugins.code_quality.checkstyle

import name.remal.default
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.dependsOn
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.exclude
import name.remal.gradle_plugins.dsl.extensions.findDependencyConstraintVersion
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.visitFiles
import name.remal.gradle_plugins.plugins.common.ReportsSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.utils.getVersionProperty
import name.remal.version.Version
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.JavaBasePlugin.CHECK_TASK_NAME
import org.gradle.api.plugins.ReportingBasePlugin
import org.gradle.api.plugins.quality.Checkstyle
import org.gradle.api.plugins.quality.CheckstyleExtension
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.compile.JavaCompile
import java.io.File

@Plugin(
    id = "name.remal.checkstyle-settings",
    description = "Plugin that configures 'checkstyle' plugin if it's applied.",
    tags = ["checkstyle"]
)
@WithPlugins(CheckstylePluginId::class)
@ApplyPluginClasses(
    ReportingBasePlugin::class,
    ReportsSettingsPlugin::class
)
class CheckstyleSettingsPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private const val CHECKSTYLE_CONFIGURATION_NAME = "checkstyle"
    }

    @PluginAction
    fun ExtensionContainer.`Update tool version`(project: Project, configurations: ConfigurationContainer) {
        val toolExtension = get(CheckstyleExtension::class.java)
        val constraintVersion = configurations.asSequence()
            .filter { it.name == CHECKSTYLE_CONFIGURATION_NAME }
            .mapNotNull { it.findDependencyConstraintVersion("com.puppycrawl.tools", "checkstyle") }
            .firstOrNull()
        if (constraintVersion != null) {
            toolExtension.toolVersion = constraintVersion
            return
        }

        val propertyVersion = project.getVersionProperty("checkstyle")
        val toolVersionStr = toolExtension.toolVersion
        if (toolVersionStr.isNullOrEmpty()) {
            toolExtension.toolVersion = propertyVersion

        } else {
            val toolVersion = Version.parseOrNull(toolVersionStr)
            val buildVersion = Version.parseOrNull(propertyVersion)
            if (toolVersion != null && buildVersion != null) {
                if (toolVersion < buildVersion) {
                    toolExtension.toolVersion = buildVersion.toString()
                }
            }
        }
    }

    @PluginAction
    @WithPlugins(JavaPluginId::class)
    protected fun setupExclusions(sourceSets: SourceSetContainer, tasks: TaskContainer, project: Project) {
        sourceSets.all { sourceSet ->
            tasks.all(Checkstyle::class.java, sourceSet.toolTaskName) { checkstyle ->
                tasks.all(JavaCompile::class.java, sourceSet.compileJavaTaskName) compile@{ compile ->
                    checkstyle.doSetup {
                        val excludedFilesPaths = mutableSetOf<String>()

                        val aptSourcesDir = compile.options.annotationProcessorGeneratedSourcesDirectory
                        if (aptSourcesDir != null) {
                            project.fileTree(aptSourcesDir).visitFiles { excludedFilesPaths.add(it.path) }
                        }

                        val buildDirPath = project.buildDir.absoluteFile.toPath()
                        sourceSet.allSource.srcDirs.map(File::getAbsoluteFile).forEach { srcDir ->
                            if (srcDir.toPath().startsWith(buildDirPath)) {
                                project.fileTree(srcDir).visitFiles { excludedFilesPaths.add(it.path) }
                            }
                        }

                        if (excludedFilesPaths.isEmpty()) return@doSetup
                        checkstyle.source = checkstyle.source.exclude(excludedFilesPaths)
                    }
                }
            }
        }
    }

    @PluginAction
    @WithPlugins(JavaPluginId::class)
    protected fun setupTasksForSourceSets(sourceSets: SourceSetContainer, tasks: TaskContainer, project: Project) {
        sourceSets.all { sourceSet ->
            tasks.all(Checkstyle::class.java, sourceSet.toolTaskName) { task ->
                task.dependsOn { listOf(sourceSet.classesTaskName) }
            }
        }

        tasks.all(CHECK_TASK_NAME) { checkTask ->
            checkTask.dependsOn {
                project[CheckstyleExtension::class.java].sourceSets.default().asSequence()
                    .mapNotNull { it.toolTaskName }
                    .mapNotNull(tasks::findByName)
                    .toList()
            }
        }
    }


    protected val SourceSet.toolTaskName: String get() = getTaskName("checkstyle", null)

}
