package name.remal.gradle_plugins.plugins.code_quality.detekt

import name.remal.buildList
import name.remal.createParentDirectories
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.getOrNull
import name.remal.newTempFile
import name.remal.uncheckedCast
import org.gradle.api.GradleException
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8

@BuildTask
class UpdateDetektConfig : BaseHandleDefaultDetektConfig() {

    override val targetFile: File by lazy {
        newTempFile("detekt-", ".yml").also { file ->
            project.gradle.buildFinished { file.delete() }
        }
    }

    override fun handleDefaultConfigFile() {
        val configFile = project.getOrNull(DetektExtension::class.java)?.configFile?.absoluteFile
            ?: throw GradleException("Detekt config file is not specified")
        logger.lifecycle("Updating {}", configFile)

        if (!configFile.exists()) {
            targetFile.copyTo(configFile.createParentDirectories())
            return
        }

        val config: Map<String, Any> = configFile.inputStream().use { deserializeConfig(it) }
        val defaultConfig: Map<String, Any> = targetFile.inputStream().use { deserializeConfig(it) }
        val result = mergeMaps(config, defaultConfig)
        val content = serializeConfig(result)
        configFile.writeText(content, UTF_8)
    }

    private fun mergeMaps(map1: Map<String, Any>, map2: Map<String, Any>): Map<String, Any> {
        val mergedMap = mutableMapOf<String, Any>().apply {
            (map1.keys + map2.keys).forEach { key ->
                val value1 = map1[key]
                val value2 = map2[key]
                if (value1 == null && value2 == null) {
                    return@forEach
                } else if (value1 == null && value2 != null) {
                    put(key, value2)
                } else if (value1 != null && value2 == null) {
                    remove(key)
                } else if (value1 is Map<*, *> && value2 is Map<*, *>) {
                    val merged = mergeMaps(value1.uncheckedCast(), value2.uncheckedCast())
                    put(key, merged)
                } else if (value1 is Iterable<*> && value2 is Iterable<*>) {
                    put(key, mergeIterables(value1, value2))
                } else {
                    put(key, value1!!)
                }
            }
        }

        val orderedMap = mergedMap.asSequence()
            .sortedWith(Comparator { entry1, entry2 ->
                if (entry1.value.isComplex && !entry2.value.isComplex) {
                    1
                } else if (!entry1.value.isComplex && entry2.value.isComplex) {
                    -1
                } else {
                    entry1.key.compareTo(entry2.key)
                }
            })
            .map { it.key to it.value }
            .toMap()

        return orderedMap
    }

    private fun mergeIterables(iterable1: Iterable<*>, iterable2: Iterable<*>): Iterable<*> {
        if (canBeMerged(iterable1) && canBeMerged(iterable2)) {
            return buildList<Any> {
                val set1 = iterable1.filterNotNull().toSet()
                val set2 = iterable2.filterNotNull().toSet()
                set1.forEach { add(it) }
                set2.forEach {
                    if (it !in set1) {
                        add(it)
                    }
                }
            }

        } else {
            return iterable1.filterNotNull()
        }
    }

    private fun canBeMerged(iterable: Iterable<*>) = iterable.all {
        it == null || it is String || it is Number || it is Boolean
    }

    private val Any.isComplex: Boolean get() = this is Map<*, *> || this is Iterable<*>

}
