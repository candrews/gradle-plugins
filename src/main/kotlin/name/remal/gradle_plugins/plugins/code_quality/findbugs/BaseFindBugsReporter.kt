package name.remal.gradle_plugins.plugins.code_quality.findbugs

import com.google.common.reflect.TypeToken
import name.remal.asClass
import name.remal.forceDeleteRecursively
import name.remal.gradle_plugins.dsl.extensions.calculateDestination
import name.remal.gradle_plugins.dsl.extensions.getOrNull
import name.remal.gradle_plugins.dsl.extensions.invokeCompatibleMethod
import name.remal.gradle_plugins.dsl.extensions.setDefaultDestinationForTask
import name.remal.gradle_plugins.dsl.utils.code_quality.createConsoleMessages
import name.remal.gradle_plugins.dsl.utils.code_quality.readFindBugsReportXml
import name.remal.gradle_plugins.dsl.utils.code_quality.tool
import name.remal.gradle_plugins.dsl.utils.code_quality.version
import name.remal.gradle_plugins.dsl.utils.code_quality.writeHtmlTo
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.gradle_plugins.plugins.code_quality.QualityTaskConsoleReporter
import name.remal.gradle_plugins.plugins.code_quality.QualityTaskHtmlReporter
import name.remal.nullIf
import name.remal.uncheckedCast
import org.gradle.api.Task
import org.gradle.api.plugins.quality.CodeQualityExtension
import org.gradle.api.reporting.Report.OutputType.DIRECTORY
import org.gradle.api.reporting.ReportContainer
import org.gradle.api.reporting.Reporting
import org.gradle.api.reporting.SingleFileReport
import org.gradle.api.tasks.VerificationTask
import java.io.File
import java.lang.reflect.ParameterizedType

abstract class BaseFindBugsReporter<TaskType, ReportsType : ReportContainer<out SingleFileReport>, ExtensionType : CodeQualityExtension>
    : QualityTaskConsoleReporter<TaskType>, QualityTaskHtmlReporter<TaskType>
    where TaskType : Task, TaskType : VerificationTask, TaskType : Reporting<ReportsType> {

    private val logger = getGradleLogger(javaClass)

    private val taskClass: Class<TaskType>
        get() {
            val type = TypeToken.of(this.javaClass).getSupertype(BaseFindBugsReporter::class.java).type
            if (type !is ParameterizedType) throw IllegalStateException("$type is not instance of ParameterizedType")
            return type.actualTypeArguments[0].asClass().uncheckedCast()
        }

    private val extensionClass: Class<ExtensionType>
        get() {
            val type = TypeToken.of(this.javaClass).getSupertype(BaseFindBugsReporter::class.java).type
            if (type !is ParameterizedType) throw IllegalStateException("$type is not instance of ParameterizedType")
            return type.actualTypeArguments[2].asClass().uncheckedCast()
        }


    override fun configureTask(task: TaskType) {
        task.reports.forEach { report ->
            report.isEnabled = report.name == "xml"
            if (report.isEnabled) {
                report.setDefaultDestinationForTask(task)
            }
        }
    }


    override fun printReportOf(task: TaskType) {
        val xmlReportFile = task.reports.xml.destination.nullIf { !isFile } ?: return
        readReportXml(xmlReportFile, task)
            .createConsoleMessages(task)
            .forEach { logger.error("\n{}", it) }
    }

    override fun writeHtmlReportFileOf(task: TaskType) {
        val htmlReport = task.reports.html
        val reportDestination = htmlReport.destination ?: htmlReport.calculateDestination(task)

        val xmlReportFile = task.reports.xml.destination ?: return
        if (!xmlReportFile.isFile) {
            reportDestination.forceDeleteRecursively()
            return
        }

        val htmlFile = if (DIRECTORY == htmlReport.outputType) reportDestination.resolve("index.html") else reportDestination
        if (xmlReportFile.lastModified() <= htmlFile.lastModified()) return
        reportDestination.forceDeleteRecursively()

        readReportXml(xmlReportFile, task).writeHtmlTo(htmlFile, task)
    }


    private fun readReportXml(file: File, task: TaskType) = readFindBugsReportXml(file)
        .apply { tool(task) }
        .apply { task.project.getOrNull(extensionClass)?.apply(::version) }


    private val ReportsType.xml: SingleFileReport get() = invokeCompatibleMethod(SingleFileReport::class.java, "getXml")
    private val ReportsType.html: SingleFileReport get() = invokeCompatibleMethod(SingleFileReport::class.java, "getHtml")

}
