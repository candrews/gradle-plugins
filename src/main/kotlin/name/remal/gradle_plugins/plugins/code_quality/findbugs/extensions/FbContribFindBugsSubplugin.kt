package name.remal.gradle_plugins.plugins.code_quality.findbugs.extensions

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.utils.ProjectAware
import name.remal.gradle_plugins.utils.getVersionProperty
import org.gradle.api.Project

@AutoService(FindBugsSubplugin::class)
class FbContribFindBugsSubplugin : FindBugsSubplugin, ProjectAware {
    override lateinit var project: Project
    override val extensionName: String = "addFbContribPlugin"
    override val dependencyNotation: String get() = "com.mebigfatguy.fb-contrib:fb-contrib:" + project.getVersionProperty("fb-contrib")
}
