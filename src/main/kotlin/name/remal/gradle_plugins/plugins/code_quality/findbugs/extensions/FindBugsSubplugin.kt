package name.remal.gradle_plugins.plugins.code_quality.findbugs.extensions

interface FindBugsSubplugin : Comparable<FindBugsSubplugin> {

    val extensionName: String

    val dependencyNotation: String

    override fun compareTo(other: FindBugsSubplugin) = extensionName.compareTo(other.extensionName)

}
