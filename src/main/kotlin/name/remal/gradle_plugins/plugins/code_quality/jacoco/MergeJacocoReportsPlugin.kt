package name.remal.gradle_plugins.plugins.code_quality.jacoco

import name.remal.KotlinAllOpen
import name.remal.concurrentSetOf
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.createWithAutoName
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.forEach
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isInTaskGraph
import name.remal.gradle_plugins.dsl.extensions.setupTasksDependenciesAfterEvaluateOrNow
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.uncheckedCast
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.file.FileCollection
import org.gradle.api.plugins.JavaBasePlugin.CHECK_TASK_NAME
import org.gradle.api.plugins.JavaBasePlugin.VERIFICATION_GROUP
import org.gradle.api.tasks.TaskContainer
import org.gradle.testing.jacoco.tasks.JacocoCoverageVerification
import org.gradle.testing.jacoco.tasks.JacocoMerge
import org.gradle.testing.jacoco.tasks.JacocoReport
import java.io.File

const val JACOCO_MERGE_TASK_NAME = "jacocoMerge"
const val JACOCO_MERGE_REPORT_TASK_NAME = "jacocoMergeReport"
const val JACOCO_MERGE_COVERAGE_VERIFICATION_TASK_NAME = "jacocoMergeCoverageVerification"

@Plugin(
    id = "name.remal.merge-jacoco-reports",
    description = "Plugin that merges all jacoco reports.",
    tags = ["java", "jacoco", "coverage", "test"],
    isHidden = true
)
@WithPlugins(JacocoPluginId::class, JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, JacocoSettingsPlugin::class)
class MergeJacocoReportsPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val jacocoReportGetExecutionDataMethod = JacocoReport::class.java.getMethod("getExecutionData").apply { isAccessible = true }
    }

    @PluginAction(order = 1)
    fun TaskContainer.`Create jacocoMerge task`(project: Project) {
        create(JACOCO_MERGE_TASK_NAME, JacocoMerge::class.java) { mergeTask ->
            mergeTask.extensions.createWithAutoName(MergeJacocoReportsTaskFlagExtension::class.java)
            mergeTask.setDestinationFile(project.provider { File(project.buildDir, "jacoco/${mergeTask.name}.exec").absoluteFile })

            this[CHECK_TASK_NAME].dependsOn(mergeTask)

            val reportTasks = concurrentSetOf<JacocoReport>()
            project.setupTasksDependenciesAfterEvaluateOrNow { _ ->
                forEach(JacocoReport::class.java) { reportTask ->
                    if (MergeJacocoReportsTaskFlagExtension::class.java !in reportTask) {
                        reportTasks.add(reportTask)
                        mergeTask.mustRunAfter(reportTask)
                    }
                }
            }
            mergeTask.onlyIf { reportTasks.any(Task::isInTaskGraph) }
            mergeTask.doSetup { _ ->
                reportTasks.forEach { reportTask ->
                    if (reportTask.isInTaskGraph) {
                        // We have to use reflection to be compatible with Gradle 5
                        jacocoReportGetExecutionDataMethod.invoke(reportTask)
                            .uncheckedCast<FileCollection>()
                            .files
                            .forEach {
                                if (it.exists()) {
                                    mergeTask.executionData(it)
                                }
                            }
                    }
                }
            }
        }
    }

    @PluginAction(order = 2)
    fun TaskContainer.`Create jacocoMergeReport task`() {
        val jacocoMerge = this[JacocoMerge::class.java, JACOCO_MERGE_TASK_NAME]
        create(JACOCO_MERGE_REPORT_TASK_NAME, JacocoReport::class.java) { task ->
            task.extensions.createWithAutoName(MergeJacocoReportsTaskFlagExtension::class.java)
            task.group = VERIFICATION_GROUP
            task.description = "Generates code coverage report for the ${jacocoMerge.name} task."
            task.executionData(jacocoMerge.destinationFile)

            task.dependsOn(jacocoMerge)
        }
    }

    @PluginAction(order = 3)
    fun TaskContainer.`Create jacocoMergeCoverageVerification task`() {
        val jacocoMerge = this[JacocoMerge::class.java, JACOCO_MERGE_TASK_NAME]
        val jacocoMergeReport = this[JacocoReport::class.java, JACOCO_MERGE_REPORT_TASK_NAME]
        create(JACOCO_MERGE_COVERAGE_VERIFICATION_TASK_NAME, JacocoCoverageVerification::class.java) { task ->
            task.extensions.createWithAutoName(MergeJacocoReportsTaskFlagExtension::class.java)
            task.group = VERIFICATION_GROUP
            task.description = "Verifies code coverage metrics based on specified rules for the ${jacocoMerge.name} task."
            task.executionData(jacocoMerge.destinationFile)

            task.dependsOn(jacocoMergeReport)
        }
    }

}

@KotlinAllOpen
private class MergeJacocoReportsTaskFlagExtension
