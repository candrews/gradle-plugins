package name.remal.gradle_plugins.plugins.code_quality.sonar

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.extensions.toConfigureAction
import name.remal.gradle_plugins.plugins.code_quality.BaseCodeQualityExtension
import name.remal.gradle_plugins.plugins.code_quality.ExcludesExtension
import org.gradle.api.Action

@Extension
class SonarLintExtension : BaseCodeQualityExtension(), SonarPropertiesMixin<SonarLintExtension> {

    val excludes: ExcludesExtension = ExcludesExtension()

    fun excludes(configurer: Action<ExcludesExtension>) = apply { configurer.execute(excludes) }

    fun excludes(@DelegatesTo(ExcludesExtension::class, strategy = DELEGATE_FIRST) configurer: Closure<*>) = excludes(configurer.toConfigureAction())


    val includes: ExcludesExtension = ExcludesExtension()

    fun includes(configurer: Action<ExcludesExtension>) = apply { configurer.execute(includes) }

    fun includes(@DelegatesTo(ExcludesExtension::class, strategy = DELEGATE_FIRST) configurer: Closure<*>) = includes(configurer.toConfigureAction())


    override var sonarProperties: MutableMap<String, String?> = sortedMapOf()
        set(value) {
            field = value.toSortedMap()
        }


    override var ruleParameters: MutableMap<String, MutableMap<String, String?>> = sortedMapOf()
        set(value) {
            field = value.toSortedMap()
        }

}
