package name.remal.gradle_plugins.plugins.code_quality.sonar

import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.uncheckedCast
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional

interface SonarPropertiesMixin<Chain : SonarPropertiesMixin<Chain>> {

    @get:Input
    @get:Optional
    var sonarProperties: MutableMap<String, String?>

    fun sonarProperty(name: Any?, value: Any?): Chain {
        val stringName = name.unwrapProviders()?.toString()
        if (stringName != null) {
            val stringValue = value.unwrapProviders()?.toString()
            sonarProperties[stringName] = stringValue
        }
        return this.uncheckedCast()
    }

    fun sonarProperties(map: Map<Any?, Any?>): Chain {
        map.forEach { name, value -> sonarProperty(name, value) }
        return this.uncheckedCast()
    }


    @get:Input
    @get:Optional
    var ruleParameters: MutableMap<String, MutableMap<String, String?>>

    fun ruleParameter(ruleId: Any, name: Any?, value: Any?): Chain {
        val stringRuleId = ruleId.toString()
        val stringName = name.unwrapProviders()?.toString()
        if (stringName != null) {
            val stringValue = value.unwrapProviders()?.toString()
            val params = ruleParameters.computeIfAbsent(stringRuleId, { mutableMapOf() }).toMutableMap()
            params[stringName] = stringValue
        }
        return this.uncheckedCast()
    }

    fun ruleParameters(ruleId: Any, map: Map<Any?, Any?>): Chain {
        map.forEach { name, value -> ruleParameter(ruleId, name, value) }
        return this.uncheckedCast()
    }

}
