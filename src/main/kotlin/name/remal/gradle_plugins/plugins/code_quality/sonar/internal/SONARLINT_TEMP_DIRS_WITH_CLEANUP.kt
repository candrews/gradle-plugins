package name.remal.gradle_plugins.plugins.code_quality.sonar.internal

import name.remal.gradle_plugins.dsl.utils.TempDirsWithCleanup

internal val SONARLINT_TEMP_DIRS_WITH_CLEANUP = TempDirsWithCleanup("sonarlint-")
