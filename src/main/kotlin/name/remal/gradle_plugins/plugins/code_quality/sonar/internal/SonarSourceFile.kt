package name.remal.gradle_plugins.plugins.code_quality.sonar.internal

import java.io.File
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8

internal data class SonarSourceFile(
    val file: File,
    val relativePath: String,
    val isTest: Boolean = false,
    val charset: Charset = UTF_8,
    val language: String? = null
) : Comparable<SonarSourceFile> {

    val absolutePath: String = file.absolutePath

    override fun compareTo(other: SonarSourceFile) = absolutePath.compareTo(other.absolutePath)

}
