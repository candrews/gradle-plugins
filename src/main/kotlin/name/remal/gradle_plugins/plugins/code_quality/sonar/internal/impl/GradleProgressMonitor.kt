package name.remal.gradle_plugins.plugins.code_quality.sonar.internal.impl

import org.gradle.api.logging.Logger
import org.gradle.initialization.BuildCancellationToken
import org.sonarsource.sonarlint.core.client.api.common.ProgressMonitor

class GradleProgressMonitor(private val logger: Logger, private val buildCancellationToken: BuildCancellationToken) : ProgressMonitor() {

    override fun isCanceled() = buildCancellationToken.isCancellationRequested

    override fun setMessage(msg: String) = logger.debug(msg)

}
