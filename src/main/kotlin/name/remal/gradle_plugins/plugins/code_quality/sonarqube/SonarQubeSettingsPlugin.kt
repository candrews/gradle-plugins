package name.remal.gradle_plugins.plugins.code_quality.sonarqube

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.dsl.extensions.java
import name.remal.gradle_plugins.dsl.extensions.main
import name.remal.gradle_plugins.dsl.extensions.mustRunAfter
import name.remal.gradle_plugins.dsl.extensions.test
import name.remal.gradle_plugins.plugins.common.TestFixturesPlugin
import name.remal.gradle_plugins.plugins.groovy.GroovyPluginId
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.testing.TestSourceSetContainer
import name.remal.gradle_plugins.plugins.testing.TestSourceSetsPlugin
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.VerificationTask
import org.gradle.api.tasks.testing.Test
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.sonarqube.gradle.SonarQubeExtension
import org.sonarqube.gradle.SonarQubeProperties
import org.sonarqube.gradle.SonarQubeTask
import java.io.File
import kotlin.LazyThreadSafetyMode.NONE

@Plugin(
    id = "name.remal.sonarqube-settings",
    description = "Plugin that configures 'org.sonarqube' plugin if it's applied.",
    tags = ["sonar", "sonarqube"]
)
@WithPlugins(SonarQubePluginId::class)
class SonarQubeSettingsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun Project.`Apply the plugin to all subprojects`() {
        subprojects { applyPlugin(SonarQubeSettingsPlugin::class.java) }
    }

    @PluginAction
    fun TaskContainer.`Setup 'sonarqube' task mustRunAfter`() {
        all(SonarQubeTask::class.java) { task ->
            task.mustRunAfter {
                return@mustRunAfter task.project.allprojects.asSequence()
                    .flatMap { it.tasks.asSequence() }
                    .filter {
                        it.name == "check"
                            || it is VerificationTask
                            || it is JacocoReport
                    }
                    .toList()
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Enable xml reports`() {
        all(Test::class.java) { it.doSetup(Int.MAX_VALUE) { it.reports.junitXml.isEnabled = true } }
        all(JacocoReport::class.java) { it.doSetup(Int.MAX_VALUE) { it.reports.xml.isEnabled = true } }
    }

    @PluginAction
    fun TaskContainer.`Configure 'sonarqube' task properties`() {
        all(SonarQubeTask::class.java) { task ->
            task.doSetup { _ ->
                task.project.allprojects.asSequence()
                    .filter { SonarQubeExtension::class.java in it }
                    .forEach { project ->
                        val sonarqube = project[SonarQubeExtension::class.java]
                        sonarqube.properties { sonarqubeProperties ->
                            sonarqubeProperties.setupSonarQubeExtension(project)
                        }
                    }
            }
        }
    }

}


private fun SonarQubeProperties.setupSonarQubeExtension(project: Project) {
    if (project.isPluginApplied(JavaPluginId)) {
        setupSonarQubeExtensionForJava(project)
    }

    setupSonarQubeExtensionForTests(project)

    properties.entries.removeIf { it.key == null || it.value == null }
}

private fun SonarQubeProperties.setupSonarQubeExtensionForJava(project: Project) {
    val mainSources = project.mainSourceSet.allSource.srcDirs
    val mainBinaries = arrayOf(project.mainSourceSet.output.classesDirs, listOf(project.mainSourceSet.output.resourcesDir))
    val mainLibraries = arrayOf(project.mainSourceSet.compileClasspath, javaLibraries)
    val isNotTestFixturesProject = !project.isPluginApplied(TestFixturesPlugin::class.java)
    if (isNotTestFixturesProject) {
        filesProperty("sonar.java.sources", mainSources)
        filesProperty("sonar.java.binaries", *mainBinaries)
        filesProperty("sonar.java.libraries", *mainLibraries)

        property("sonar.java.tests", "")
        property("sonar.java.test.binaries", "")
        property("sonar.java.test.libraries", "")

    } else {
        property("sonar.java.sources", "")
        property("sonar.java.binaries", "")
        property("sonar.java.libraries", "")

        filesProperty("sonar.java.tests", mainSources)
        filesProperty("sonar.java.test.binaries", *mainBinaries)
        filesProperty("sonar.java.test.libraries", *mainLibraries)

        property("sonar.coverage.exclusions", "**/*")
    }

    appendFilesProperty(
        "sonar.java.tests",
        project.testSourceSets.asSequence()
            .flatMap { it.allSource.srcDirs.asSequence() }
    )
    appendFilesProperty(
        "sonar.java.test.binaries",
        project.testSourceSets.asSequence()
            .flatMap { it.output.classesDirs.asSequence() + sequenceOf(it.output.resourcesDir) }
    )
    appendFilesProperty(
        "sonar.java.test.libraries",
        project.testSourceSets.asSequence()
            .flatMap { it.compileClasspath.asSequence() },
        javaLibraries.asSequence()
    )

    properties["sonar.java.sources"]?.let { property("sonar.sources", it) }
    properties["sonar.java.tests"]?.let { property("sonar.tests", it) }

    // Backward compatibility
    properties["sonar.java.binaries"]?.let { property("sonar.binaries", it) }
    properties["sonar.java.libraries"]?.let { property("sonar.libraries", it) }

    // Groovy
    if (project.isPluginApplied(GroovyPluginId)) {
        val prefix = "sonar.java."
        properties.entries.toList().forEach { (key, value) ->
            if (key.startsWith(prefix)) {
                val newKey = "sonar.groovy." + key.substring(prefix.length)
                property(newKey, value)
            }
        }
    }
}

private fun SonarQubeProperties.setupSonarQubeExtensionForTests(project: Project) {
    filesProperty(
        "sonar.junit.reportPaths",
        project.tasks.asSequence()
            .filterIsInstance(Test::class.java)
            .map { it.reports.junitXml.destination }
    )

    // Backward compatibility
    properties["sonar.junit.reportPaths"]?.let { property("sonar.junit.reportsPath", it) }
    properties["sonar.junit.reportPaths"]?.let { property("sonar.surefire.reportsPath", it) }
}

private fun SonarQubeProperties.filesProperty(key: String, vararg files: Iterable<File?>) {
    filesProperty(key, files.asSequence().flatten())
}

private fun SonarQubeProperties.filesProperty(key: String, vararg files: Sequence<File?>) {
    property(key, files.toPropertyValue())
}

@Suppress("unused")
private fun SonarQubeProperties.appendFilesProperty(key: String, vararg files: Iterable<File?>) {
    appendFilesProperty(key, files.asSequence().flatten())
}

private fun SonarQubeProperties.appendFilesProperty(key: String, vararg files: Sequence<File?>) {
    val currentValue = properties[key]?.toString()
    val valueToAdd = files.toPropertyValue()
    if (currentValue == null || currentValue.isEmpty()) {
        property(key, valueToAdd)

    } else if (valueToAdd.isNotEmpty()) {
        val newValue = sequenceOf(currentValue, valueToAdd)
            .flatMap { it.splitToSequence(',') }
            .distinct()
            .joinToString(",")
        property(key, newValue)
    }
}

private fun Array<out Sequence<File?>>.toPropertyValue(): String = asSequence().flatten().toPropertyValue()
private fun Sequence<File?>.toPropertyValue(): String = filterNotNull().distinct().filter(File::exists).joinToString(",")


private val Project.mainSourceSet: SourceSet get() = java.sourceSets.main

private val Project.testSourceSets: Iterable<SourceSet>
    get() {
        if (isPluginApplied(TestSourceSetsPlugin::class.java)) {
            return get(TestSourceSetContainer::class.java)
        } else {
            return listOf(java.sourceSets.test)
        }
    }

private val javaLibraries: Iterable<File> by lazy(NONE) {
    val javaHome = System.getProperty("java.home")?.let(::File)?.absoluteFile ?: return@lazy emptyList<File>()
    return@lazy sequenceOf(
        sequenceOf(
            "lib/rt.jar",
            "jre/lib/rt.jar"
        ),
        sequenceOf(
            "lib/ext/jfxrt.jar",
            "jre/lib/ext/jfxrt.jar"
        )
    )
        .flatMap { paths ->
            paths
                .map(javaHome::resolve)
                .filter(File::exists)
                .take(1)
        }
        .toList()
}
