package name.remal.gradle_plugins.plugins.common

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.ExtensionProperty
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPluginClasses
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.replaceTokens
import name.remal.gradle_plugins.plugins.generate_sources.GenerateSourcesPlugin
import name.remal.gradle_plugins.plugins.generate_sources.resources.GenerateResources
import org.gradle.api.file.CopySpec
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskContainer
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8

@Plugin(
    id = "name.remal.filtering-settings",
    description = "Plugin that simplifies filtering settings for copy tasks",
    tags = ["filtering"]
)
class FilteringSettingsPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'filteringSettings' extension`() {
        create("filteringSettings", FilteringSettings::class.java)
    }

    @PluginAction
    fun TaskContainer.`Setup all CopySpec tasks using 'filteringSettings' extension`() {
        all { task ->
            if (task !is CopySpec) return@all
            task.doSetup {
                val filteringSettings = task.project[FilteringSettings::class.java]

                task.filteringCharset = filteringSettings.filteringCharset.name()

                if (filteringSettings.fileExtensions.isEmpty() && filteringSettings.filePatterns.isEmpty()) {
                    return@doSetup
                }

                val tokens = mutableMapOf<String, String>()
                task.project.properties.forEach { key, value ->
                    if (key == null) return@forEach
                    var unwrappedValue: Any? = value
                    while (unwrappedValue is Provider<*>) {
                        unwrappedValue = unwrappedValue.orNull
                    }
                    if (unwrappedValue is CharSequence
                        || unwrappedValue is Char
                        || unwrappedValue is Boolean
                        || unwrappedValue is Number
                    ) {
                        tokens[key] = unwrappedValue.toString()
                    }
                }

                filteringSettings.fileExtensions.forEach { fileExtension ->
                    task.filesMatching("**/*.$fileExtension") {
                        it.replaceTokens(tokens, filteringSettings.beginToken, filteringSettings.endToken)
                    }
                }
                filteringSettings.filePatterns.forEach { filePattern ->
                    task.filesMatching(filePattern) {
                        it.replaceTokens(tokens, filteringSettings.beginToken, filteringSettings.endToken)
                    }
                }
            }
        }
    }

    @WithPluginClasses(GenerateSourcesPlugin::class)
    @PluginAction
    fun TaskContainer.`Setup charset for resources generating tasks`() {
        all(GenerateResources::class.java) {
            it.doSetup {
                val filteringSettings = it.project[FilteringSettings::class.java]
                it.charset = filteringSettings.filteringCharset.name()
            }
        }
    }

}


@Extension("Filtering settings for copy tasks")
class FilteringSettings {

    @ExtensionProperty("Specifies extensions of files to do filtering")
    var fileExtensions: MutableSet<String> = sortedSetOf("properties", "xml")
        set(value) {
            field = value.toSortedSet()
        }

    @ExtensionProperty("Specifies file patterns to do filtering")
    var filePatterns: MutableSet<String> = sortedSetOf()
        set(value) {
            field = value.toSortedSet()
        }

    @ExtensionProperty("Specifies the charset used to read and write files when filtering")
    var filteringCharset: Charset = UTF_8

    @ExtensionProperty("Specifies the \"begin token\"")
    var beginToken: String = "@"

    @ExtensionProperty("Specifies the \"end token\"")
    var endToken: String = "@"

}

