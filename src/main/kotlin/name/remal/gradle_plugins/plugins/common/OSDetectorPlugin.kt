package name.remal.gradle_plugins.plugins.common

import name.remal.OS
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.Plugin
import org.gradle.api.plugins.ExtensionContainer

@Plugin(
    id = "name.remal.osdetector",
    description = "Plugin that provides osdetector extension.",
    tags = ["os", "osdetector", "os-detector"]
)
class OSDetectorPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create osdetector extension`(): OSDetectorExtension {
        return create("osdetector", OSDetectorExtension::class.java)
    }

}


@Extension
class OSDetectorExtension {

    val name: String get() = OS.getOsName()

    val arch: String get() = OS.getOsArch()

    val classifier: String get() = OS.getOsClassifier()

    val version: String? get() = OS.getOsVersion()


    val isLinux: Boolean get() = OS.isLinux()

    val isWindows: Boolean get() = OS.isWindows()

}
