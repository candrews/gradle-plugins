package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.extensions.isPluginDisabledByProperty
import org.gradle.api.Project
import org.gradle.api.artifacts.ComponentMetadataContext
import org.gradle.api.artifacts.ComponentMetadataDetails
import org.gradle.api.artifacts.ComponentMetadataRule
import org.gradle.api.artifacts.ModuleVersionIdentifier

abstract class AbstractComponentMetadata(protected val project: Project) : ComponentMetadataRule {

    protected abstract fun ModuleVersionIdentifier.process(details: ComponentMetadataDetails)

    final override fun execute(context: ComponentMetadataContext) {
        if (project.isPluginDisabledByProperty(ComponentMetadataPlugin::class.java)) return

        context.details.let { details ->
            details.id.process(details)
        }
    }

}
