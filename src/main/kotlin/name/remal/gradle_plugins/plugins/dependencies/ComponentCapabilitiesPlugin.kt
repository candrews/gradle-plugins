package name.remal.gradle_plugins.plugins.dependencies

import name.remal.Services.loadImplementationClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_9
import name.remal.gradle_plugins.dsl.MinGradleVersion
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.utils.wrapWithInjectedConstructorParams
import name.remal.uncheckedCast
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler

@Plugin(
    id = "name.remal.component-capabilities",
    description = "Plugin that configures some default component capabilities.",
    tags = ["capabilities", "component-capabilities"]
)
@MinGradleVersion(GRADLE_VERSION_4_9)
class ComponentCapabilitiesPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val capabilitiesClasses: List<Class<AbstractComponentCapabilities>> by lazy {
            sequenceOf(
                AbstractComponentCapabilities::class.java,
                AbstractMavenCentralComponentCapabilities::class.java
            )
                .flatMap { loadImplementationClasses(it).asSequence() }
                .distinct()
                .map { it.uncheckedCast<Class<AbstractComponentCapabilities>>() }
                .toList()
        }
    }

    @PluginAction(isHidden = true)
    protected fun DependencyHandler.registerComponentCapabilities(project: Project) {
        capabilitiesClasses.forEach { capabilitiesClass ->
            val wrappedCapabilitiesClass = wrapWithInjectedConstructorParams(capabilitiesClass, project)
            components.all(wrappedCapabilitiesClass)
        }
    }

}
