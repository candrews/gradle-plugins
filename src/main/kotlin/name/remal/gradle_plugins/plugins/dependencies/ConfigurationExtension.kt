package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.extensions.applyLoggingTransitiveDependenciesExcludes
import name.remal.gradle_plugins.dsl.extensions.newLoggingLoadingOrderFactory
import name.remal.reflection.ExtendedURLClassLoader
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.PARENT_FIRST
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.THIS_FIRST
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.THIS_ONLY
import org.gradle.api.artifacts.Configuration
import java.net.URLClassLoader

@Extension
class ConfigurationExtension(
    private val configuration: Configuration
) {

    fun excludeLogging() {
        configuration.applyLoggingTransitiveDependenciesExcludes()
    }


    private fun createClassLoader(loadingOrder: LoadingOrder): URLClassLoader {
        val loadingOrderFactory = newLoggingLoadingOrderFactory(loadingOrder)
        val urls = configuration.files.map { it.toURI().toURL() }
        return ExtendedURLClassLoader(loadingOrderFactory, urls, this.javaClass.classLoader)
    }

    fun createClassLoader() = createClassLoader(PARENT_FIRST)

    fun createThisOnlyClassLoader() = createClassLoader(THIS_ONLY)

    fun createThisFirstClassLoader() = createClassLoader(THIS_FIRST)

}
