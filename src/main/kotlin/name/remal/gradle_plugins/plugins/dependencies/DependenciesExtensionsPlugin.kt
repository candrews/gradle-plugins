package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.ApplyPluginClassesAtTheEnd
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.convention
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler

@Plugin(
    id = "name.remal.dependencies-extensions",
    description = "Plugin that provides dependencies extensions",
    tags = ["dependencies"]
)
@ApplyPluginClassesAtTheEnd(DependenciesFilterPlugin::class)
class DependenciesExtensionsPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun DependencyHandler.`toolsJar`(project: Project) {
        convention.addPlugin(DependencyHandlerToolsJarExtension(project))
    }

    @CreateExtensionsPluginAction("Add dependencies.embeddedKotlin extension method")
    fun DependencyHandler.embeddedKotlin(dependencies: DependencyHandler) {
        convention.addPlugin(DependencyHandlerEmbeddedKotlinExtension(dependencies))
    }

    @CreateExtensionsPluginAction("Add dependencies.correspondingKotlin extension method")
    fun DependencyHandler.correspondingKotlin(dependencies: DependencyHandler) {
        convention.addPlugin(DependencyHandlerCorrespondingKotlinExtension(dependencies))
    }

    @CreateExtensionsPluginAction("Add dependencies.correspondingKotlinGradlePlugin extension method")
    fun DependencyHandler.correspondingKotlinGradlePlugin(dependencies: DependencyHandler) {
        convention.addPlugin(DependencyHandlerCorrespondingKotlinGradlePluginExtension(dependencies))
    }

}
