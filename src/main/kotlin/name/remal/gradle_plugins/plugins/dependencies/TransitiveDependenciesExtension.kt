package name.remal.gradle_plugins.plugins.dependencies

import name.remal.Ordered
import name.remal.buildMap
import name.remal.concurrentSetOf
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.sourceSetConfigurationNames
import name.remal.loadServicesList
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.ExcludeRule.GROUP_KEY
import org.gradle.api.artifacts.ExcludeRule.MODULE_KEY
import org.gradle.api.artifacts.ExternalDependency
import org.gradle.api.plugins.JavaPluginConvention

@Extension
class TransitiveDependenciesExtension(
    private val project: Project,
    private val configurations: ConfigurationContainer
) {

    companion object {
        private val transitiveDependenciesConfigurationMatchers = loadServicesList(TransitiveDependenciesConfigurationMatcher::class.java)
        private val staticAnalysisTransitiveDependencies = loadServicesList(StaticAnalysisTransitiveDependencies::class.java)
    }

    private val matchers = transitiveDependenciesConfigurationMatchers.toMutableList()

    fun addConfigurationToProcess(vararg configurationNames: String) {
        val configurationNamesSet = configurationNames.toSet()
        matchers.add(object : TransitiveDependenciesConfigurationMatcher {
            override fun matches(project: Project, configuration: Configuration) = configuration.name in configurationNamesSet
        })
    }

    fun addConfigurationToProcess(vararg configurations: Configuration) = addConfigurationToProcess(*configurations.map(Configuration::getName).toTypedArray())

    private val appliedExcludeProperties = concurrentSetOf<Map<String, String?>>()

    fun exclude(excludeProperties: Map<String, String?>) {
        val excludePropertiesNormalized = HashMap(excludeProperties.filterValues { !it.isNullOrEmpty() && it != "*" })
        if (appliedExcludeProperties.add(excludePropertiesNormalized)) {
            configurations.all { conf ->
                if (matchers.none { it.matches(project, conf) }) return@all
                conf.dependencies.all { dep ->
                    if (dep is ExternalDependency) {
                        dep.exclude(excludePropertiesNormalized)
                    }
                }
            }
        }
    }

    @JvmOverloads
    fun exclude(group: String? = null, module: String? = null) = exclude(buildMap {
        val parsedNotation = group?.split(':')
        put(GROUP_KEY, parsedNotation?.getOrNull(0))
        put(MODULE_KEY, parsedNotation?.getOrNull(1) ?: module)
    })

    fun excludeStaticAnalysisTools() {
        staticAnalysisTransitiveDependencies.forEach {
            it.dependencyNotations.forEach { exclude(it) }
        }
    }

}


interface TransitiveDependenciesConfigurationMatcher : Ordered<TransitiveDependenciesConfigurationMatcher> {
    fun matches(project: Project, configuration: Configuration): Boolean
}

@AutoService
class JavaTransitiveDependenciesConfigurationMatcher : TransitiveDependenciesConfigurationMatcher {
    override fun matches(project: Project, configuration: Configuration): Boolean {
        if (JavaPluginConvention::class.java !in project) return false
        return project[JavaPluginConvention::class.java].sourceSets.any { configuration.name in it.sourceSetConfigurationNames }
    }
}
