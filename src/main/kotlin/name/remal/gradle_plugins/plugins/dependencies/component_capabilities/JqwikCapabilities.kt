package name.remal.gradle_plugins.plugins.dependencies.component_capabilities

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.dependencies.AbstractComponentCapabilities
import org.gradle.api.Project
import org.gradle.api.artifacts.ComponentMetadataDetails
import javax.inject.Inject

@AutoService
class JqwikCapabilities @Inject constructor(project: Project) : AbstractComponentCapabilities(project) {

    companion object {
        private val modulesToAlign = setOf(
            "jqwik",
            "jqwik-api",
            "jqwik-engine"
        )
    }

    override fun ComponentMetadataDetails.process() {
        if (id.group == "net.jqwik" && id.name in modulesToAlign) {
            belongsTo("jqwik", id.version)
        }
    }

}
