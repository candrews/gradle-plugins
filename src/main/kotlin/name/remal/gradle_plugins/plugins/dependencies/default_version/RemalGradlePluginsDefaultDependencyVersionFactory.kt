package name.remal.gradle_plugins.plugins.dependencies.default_version

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants.getStringProperty
import name.remal.gradle_plugins.dsl.utils.parseDependencyNotation
import name.remal.gradle_plugins.plugins.dependencies.DefaultDependencyVersion
import name.remal.gradle_plugins.plugins.dependencies.DefaultDependencyVersionFactory
import name.remal.gradle_plugins.plugins.dependencies.defaultDependencyVersionFromDependencyNotation
import org.gradle.api.Project

@AutoService
class RemalGradlePluginsDefaultDependencyVersionFactory : DefaultDependencyVersionFactory {
    override fun create(project: Project): List<DefaultDependencyVersion> {
        return getStringProperty("allproject-notations").splitToSequence(';')
            .map(String::trim)
            .filter(String::isNotEmpty)
            .map(::parseDependencyNotation)
            .map(::defaultDependencyVersionFromDependencyNotation)
            .toList()
    }
}
