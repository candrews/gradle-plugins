package name.remal.gradle_plugins.plugins.dependencies.dependencies_modifiers

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.exclude
import name.remal.gradle_plugins.plugins.dependencies.DependencyModifier
import name.remal.gradle_plugins.plugins.dependencies.StaticAnalysisTransitiveDependencies
import name.remal.loadServicesList
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalModuleDependency

@AutoService
class GuavaDependencyModifier : DependencyModifier {

    companion object {
        private val staticAnalysisTransitiveDependencies = loadServicesList(StaticAnalysisTransitiveDependencies::class.java)
    }

    override fun modify(dependency: Dependency) {
        (dependency as? ExternalModuleDependency)?.apply {
            if (group == "com.google.guava" && name == "guava") {
                staticAnalysisTransitiveDependencies.forEach {
                    it.dependencyNotations.forEach {
                        exclude(it)
                    }
                }
            }
        }
    }

}
