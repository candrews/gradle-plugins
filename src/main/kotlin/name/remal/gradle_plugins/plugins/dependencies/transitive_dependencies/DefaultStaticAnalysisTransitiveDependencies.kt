package name.remal.gradle_plugins.plugins.dependencies.transitive_dependencies

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.dependencies.StaticAnalysisTransitiveDependencies

@AutoService
class DefaultStaticAnalysisTransitiveDependencies : StaticAnalysisTransitiveDependencies {
    override val dependencyNotations: List<String> = listOf(
        "javax.annotation:javax.annotation-api",
        "jakarta.annotation:jakarta.annotation-api",
        "org.jetbrains:annotations",
        "com.google.code.findbugs:findbugs",
        "com.google.code.findbugs:annotations",
        "com.google.code.findbugs:jsr305",
        "com.github.spotbugs:spotbugs",
        "com.github.spotbugs:spotbugs-annotations",
        "com.github.spotbugs:annotations",
        "net.jcip:jcip-annotations",
        "org.checkerframework",
        "com.google.errorprone",
        "com.google.j2objc:j2objc-annotations",
        "org.codehaus.mojo:animal-sniffer-annotations"
    ).flatMap { listOf(it, "org.jboss.spec.$it") }
}
