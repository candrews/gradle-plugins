package name.remal.gradle_plugins.plugins.generate_sources

import name.remal.gradle_plugins.dsl.BuildTask
import org.gradle.api.tasks.CacheableTask

@BuildTask
@CacheableTask
abstract class BaseGenerateClassTask<WriterType : BaseGeneratingClassWriter<WriterType>> : BaseGenerateTask(), GenerateClassTask<WriterType>
