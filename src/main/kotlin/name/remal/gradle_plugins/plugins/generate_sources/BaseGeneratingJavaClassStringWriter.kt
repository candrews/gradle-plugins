package name.remal.gradle_plugins.plugins.generate_sources

import java.io.File

abstract class BaseGeneratingJavaClassStringWriter<Self : BaseGeneratingJavaClassStringWriter<Self>>(
    packageName: String = "",
    simpleName: String = "",
    classpath: Iterable<File> = emptyList(),
    wrapDepth: Int = 0
) : BaseGeneratingClassStringWriter<Self>(packageName, simpleName, classpath, wrapDepth), BaseGeneratingJavaClassWriterInterface<Self>
