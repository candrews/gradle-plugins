package name.remal.gradle_plugins.plugins.generate_sources

interface BaseGeneratingJavaClassWriterInterface<Self : BaseGeneratingJavaClassWriterInterface<Self>> : GeneratingClassWriterInterface<Self> {

    companion object {
        private val IMMUTABLE_ANNOTATIONS = arrayOf("javax.annotation.concurrent.Immutable", "net.jcip.annotations.Immutable")
        private val THREAD_SAFE_ANNOTATIONS = arrayOf("javax.annotation.concurrent.ThreadSafe", "net.jcip.annotations.ThreadSafe")
        private val NOT_THREAD_SAFE_ANNOTATIONS = arrayOf("javax.annotation.concurrent.NotThreadSafe", "net.jcip.annotations.NotThreadSafe")
        private val NOT_NULL_ANNOTATIONS = arrayOf("javax.annotation.Nonnull", "org.jetbrains.annotations.NotNull")
        private val NULLABLE_ANNOTATIONS = arrayOf("javax.annotation.Nullable", "org.jetbrains.annotations.Nullable")
    }

    override fun writeSuppressWarnings(vararg warnings: String) {
        append("@SuppressWarnings")
        if (warnings.isNotEmpty()) {
            append('(')
            if (1 == warnings.size) {
                append('"').append(escapeJava(warnings[0])).append('"')
            } else {
                warnings.joinTo(this, ", ", "{", "}", transform = { "\"" + escapeJava(it) + "\"" })
            }
            append(')')
        }
        append('\n')

        "edu.umd.cs.findbugs.annotations.SuppressFBWarnings".let {
            if (isClassInClasspath(it)) {
                append("@").append(it)
                if (warnings.isNotEmpty() && warnings.none { it.equals("all", true) }) {
                    append('(')
                    if (1 == warnings.size) {
                        append('"').append(escapeJava(warnings[0])).append('"')
                    } else {
                        warnings.joinTo(this, ", ", "{", "}", transform = { "\"" + escapeJava(it) + "\"" })
                    }
                    append(')')
                }
                append('\n')
            }
        }
    }

    fun writeImmutableAnnotation() {
        IMMUTABLE_ANNOTATIONS.forEach {
            if (isClassInClasspath(it)) {
                append("@").append(it).append('\n')
                return
            }
        }
    }

    fun writeThreadSafeAnnotationAnnotation() {
        THREAD_SAFE_ANNOTATIONS.forEach {
            if (isClassInClasspath(it)) {
                append("@").append(it).append('\n')
                return
            }
        }
    }

    fun writeNotThreadSafeAnnotationAnnotation() {
        NOT_THREAD_SAFE_ANNOTATIONS.forEach {
            if (isClassInClasspath(it)) {
                append("@").append(it).append('\n')
                return
            }
        }
    }

    fun writeOverrideAnnotation() {
        append("@Override\n")
    }

    fun writeNotNullAnnotation() {
        NOT_NULL_ANNOTATIONS.forEach {
            if (isClassInClasspath(it)) {
                append("@").append(it).append('\n')
                return
            }
        }
    }

    val notNullAnnotation: String
        get() {
            NOT_NULL_ANNOTATIONS.forEach {
                if (isClassInClasspath(it)) {
                    return "@$it "
                }
            }
            return ""
        }

    fun writeNullableAnnotation() {
        NULLABLE_ANNOTATIONS.forEach {
            if (isClassInClasspath(it)) {
                append("@").append(it).append('\n')
                return
            }
        }
    }

    val nullableAnnotation: String
        get() {
            NULLABLE_ANNOTATIONS.forEach {
                if (isClassInClasspath(it)) {
                    return "@$it "
                }
            }
            return ""
        }

    @Suppress("UNUSED_VALUE")
    fun writeContractAnnotation(value: String, pure: Boolean, mutates: String) {
        if (value.isEmpty() && !pure && mutates.isEmpty()) {
            return
        }

        "org.jetbrains.annotations.Contract".let {
            if (isClassInClasspath(it)) {
                append("@").append(it).append('(')
                var isFirstArg = true
                if (value.isNotEmpty()) {
                    if (isFirstArg) {
                        isFirstArg = false
                    } else {
                        append(", ")
                    }
                    append("value = \"").append(escapeJava(value)).append("\"")
                }
                if (pure) {
                    if (isFirstArg) {
                        isFirstArg = false
                    } else {
                        append(", ")
                    }
                    append("pure = true")
                }
                if (mutates.isNotEmpty()) {
                    if (isFirstArg) {
                        isFirstArg = false
                    } else {
                        append(", ")
                    }
                    append("mutates = \"").append(escapeJava(mutates)).append("\"")
                }
                append(')').append('\n')
            }
        }
    }

    fun writeContractAnnotation(value: String, pure: Boolean) = writeContractAnnotation(value, pure, "")

    fun writeContractAnnotation(value: String) = writeContractAnnotation(value, false)

    fun writeContractAnnotation(pure: Boolean) = writeContractAnnotation("", pure)

    override fun writePackage() {
        if (packageName.isNotEmpty()) {
            append("package ").append(packageName).append(";\n")
        }
    }

    override fun writeImport(canonicalClassName: String) {
        append("import ").append(canonicalClassName).append(";\n")
    }

    override fun writeStaticImport(canonicalClassName: String, member: String) {
        append("import static ").append(canonicalClassName).append('.').append(member).append(";\n")
    }

}
