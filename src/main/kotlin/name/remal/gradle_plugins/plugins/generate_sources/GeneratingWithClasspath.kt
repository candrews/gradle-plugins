package name.remal.gradle_plugins.plugins.generate_sources

interface GeneratingWithClasspath {
    fun isClassInClasspath(className: String): Boolean
    fun isClassInClasspath(clazz: Class<*>) = isClassInClasspath(clazz.name)
}
