package name.remal.gradle_plugins.plugins.generate_sources

import name.remal.default
import java.io.BufferedWriter
import java.io.File
import java.io.Writer

open class GeneratingWriter(
    override val targetFile: File,
    override val relativePath: String,
    override val generateTask: BaseGenerateTask,
    delegate: Writer,
    private val bufferedWriter: BufferedWriter = BufferedWriter(delegate)
) : Writer(), GeneratingOutput, GeneratingWriterInterface {

    override fun write(cbuf: CharArray, from: Int, len: Int) = bufferedWriter.write(cbuf, from, len)
    override fun write(obj: Any?) = bufferedWriter.write(obj?.toString().default("null"))

    override fun append(csq: CharSequence?) = bufferedWriter.append(csq)
    override fun append(csq: CharSequence?, start: Int, end: Int) = bufferedWriter.append(csq, start, end)
    override fun append(c: Char) = bufferedWriter.append(c)

    override fun flush() = bufferedWriter.flush()
    override fun close() = bufferedWriter.close()

}
