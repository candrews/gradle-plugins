package name.remal.gradle_plugins.plugins.generate_sources

interface GeneratingWriterInterface : Appendable, GeneratingWithClasspath {

    fun write(obj: Any?)
    fun writeln() = write("\n")
    fun writeln(obj: Any?) = run { write(obj); writeln() }

    fun append(obj: Any?): Appendable = apply { write(obj) }
    fun appendln(obj: Any?): Appendable = apply { writeln(obj) }
    override fun append(csq: CharSequence?): Appendable = apply { write(csq) }
    override fun append(csq: CharSequence?, start: Int, end: Int): Appendable = apply { write((csq ?: "null").subSequence(start, end)) }
    override fun append(c: Char): Appendable = append(String(charArrayOf(c)))

}
