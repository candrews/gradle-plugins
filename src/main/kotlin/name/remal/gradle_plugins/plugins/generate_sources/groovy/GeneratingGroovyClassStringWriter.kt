package name.remal.gradle_plugins.plugins.generate_sources.groovy

import name.remal.gradle_plugins.plugins.generate_sources.BaseGeneratingJavaClassStringWriter
import java.io.File

class GeneratingGroovyClassStringWriter(
    packageName: String = "",
    simpleName: String = "",
    classpath: Iterable<File> = emptyList(),
    wrapDepth: Int = 0
) : BaseGeneratingJavaClassStringWriter<GeneratingGroovyClassStringWriter>(packageName, simpleName, classpath, wrapDepth), GeneratingGroovyClassWriterInterface<GeneratingGroovyClassStringWriter> {

    override val classFileExtension: String get() = "groovy"

    override fun newSubWriter() = GeneratingGroovyClassStringWriter(packageName, simpleName, classpath, wrapDepth + 1)

}
