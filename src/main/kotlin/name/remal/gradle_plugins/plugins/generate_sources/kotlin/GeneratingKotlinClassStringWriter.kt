package name.remal.gradle_plugins.plugins.generate_sources.kotlin

import name.remal.gradle_plugins.plugins.generate_sources.BaseGeneratingClassStringWriter
import java.io.File

class GeneratingKotlinClassStringWriter(
    packageName: String = "",
    simpleName: String = "",
    classpath: Iterable<File> = emptyList(),
    wrapDepth: Int = 0
) : BaseGeneratingClassStringWriter<GeneratingKotlinClassStringWriter>(packageName, simpleName, classpath, wrapDepth), GeneratingKotlinClassWriterInterface<GeneratingKotlinClassStringWriter> {

    override val classFileExtension: String get() = "kt"

    override fun newSubWriter() = GeneratingKotlinClassStringWriter(packageName, simpleName, classpath, wrapDepth + 1)

}
