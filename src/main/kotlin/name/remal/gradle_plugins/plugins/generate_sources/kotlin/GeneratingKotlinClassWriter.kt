package name.remal.gradle_plugins.plugins.generate_sources.kotlin

import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateTask
import name.remal.gradle_plugins.plugins.generate_sources.BaseGeneratingClassWriter
import java.io.File
import java.io.StringWriter
import java.io.Writer

class GeneratingKotlinClassWriter(
    packageName: String,
    simpleName: String,
    targetFile: File,
    relativePath: String,
    generateTask: BaseGenerateTask,
    delegate: Writer,
    wrapDepth: Int = 0
) : BaseGeneratingClassWriter<GeneratingKotlinClassWriter>(packageName, simpleName, targetFile, relativePath, generateTask, delegate, wrapDepth),
    GeneratingKotlinClassWriterInterface<GeneratingKotlinClassWriter> {

    override fun wrapStringWriter(stringWriter: StringWriter) = GeneratingKotlinClassWriter(packageName, simpleName, targetFile, relativePath, generateTask, stringWriter, wrapDepth + 1)

}
