package name.remal.gradle_plugins.plugins.gradle_plugins

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import java.io.File

sealed class ClasspathItem

data class FileClasspathItem(val file: File) : ClasspathItem()

data class NotationClasspathItem(val notation: DependencyNotation) : ClasspathItem()
