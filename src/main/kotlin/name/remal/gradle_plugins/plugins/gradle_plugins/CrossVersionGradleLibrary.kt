package name.remal.gradle_plugins.plugins.gradle_plugins

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.newFileCollectionDependency
import name.remal.gradle_plugins.dsl.utils.parseDependencyNotation
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.internal.installation.CurrentGradleInstallation
import org.gradle.util.GradleVersion
import java.io.File

enum class CrossVersionGradleLibrary(
    notation: String,
    val dependencyFactory: DependencyHandler.() -> Dependency?
) {

    LOCAL_GROOVY("name.remal.gradle-api:local-groovy", DependencyHandler::localGroovy),

    GRADLE_API("name.remal.gradle-api:gradle-api", DependencyHandler::gradleApi),

    GRADLE_TEST_KIT("name.remal.gradle-api:gradle-test-kit", DependencyHandler::gradleTestKit),

    EMBEDDED_KOTLIN("name.remal.gradle-api:embedded-kotlin", {
        newFileCollectionDependency {
            libDirs.asSequence()
                .flatMap { it.listFiles()?.asSequence() ?: emptySequence() }
                .filter { it.name.startsWith("kotlin-") && it.name.endsWith(".jar") }
                .filter { !it.name.contains("compiler") }
                .filter(File::isFile)
                .map(File::getAbsoluteFile)
                .toList()
        }
    }),

    CORRESPONDING_KOTLIN("name.remal.gradle-api:embedded-kotlin", { create("name.remal.gradle-api:embedded-kotlin:${GradleVersion.current().version}") }),

    CORRESPONDING_KOTLIN_PLUGIN("name.remal.gradle-api:corresponding-kotlin-plugin", { create("name.remal.gradle-api:corresponding-kotlin-plugin:${GradleVersion.current().version}") }),

    ;

    val notation: DependencyNotation = parseDependencyNotation(notation)

}

private val libDirs: List<File> get() = CurrentGradleInstallation.get()?.libDirs ?: emptyList()
