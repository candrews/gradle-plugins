package name.remal.gradle_plugins.plugins.groovy

import name.remal.gradle_plugins.dsl.PluginId

object GroovyPluginId : PluginId("groovy")
