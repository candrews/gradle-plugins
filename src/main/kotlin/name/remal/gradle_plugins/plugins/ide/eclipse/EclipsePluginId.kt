package name.remal.gradle_plugins.plugins.ide.eclipse

import name.remal.gradle_plugins.dsl.PluginId

object EclipsePluginId : PluginId("eclipse")
