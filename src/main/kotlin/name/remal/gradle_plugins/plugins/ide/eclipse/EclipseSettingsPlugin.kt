package name.remal.gradle_plugins.plugins.ide.eclipse

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.HighestPriorityPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.get
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.plugins.ide.eclipse.model.EclipseModel

@Plugin(
    id = "name.remal.eclipse-settings",
    description = "Plugin that configures 'eclipse' plugin if it's applied.",
    tags = ["eclipse"]
)
@WithPlugins(EclipsePluginId::class)
class EclipseSettingsPlugin : BaseReflectiveProjectPlugin() {

    @HighestPriorityPluginAction
    fun Project.`Apply 'eclipse' and this plugins for all projects`() {
        rootProject.allprojects {
            it.applyPlugin(EclipsePluginId)
            it.applyPlugin(EclipseSettingsPlugin::class.java)
        }
    }

    @PluginAction
    fun ExtensionContainer.`Turn ON downloading dependency javadoc`() {
        this[EclipseModel::class.java].classpath?.apply {
            isDownloadJavadoc = true
        }
    }

    @PluginAction
    fun ExtensionContainer.`Turn ON downloading dependency sources`() {
        this[EclipseModel::class.java].classpath?.apply {
            isDownloadSources = true
        }
    }

}
