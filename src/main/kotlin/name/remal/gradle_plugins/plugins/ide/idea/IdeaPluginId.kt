package name.remal.gradle_plugins.plugins.ide.idea

import name.remal.gradle_plugins.dsl.PluginId

object IdeaPluginId : PluginId("idea")
