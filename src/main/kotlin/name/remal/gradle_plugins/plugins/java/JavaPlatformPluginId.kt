package name.remal.gradle_plugins.plugins.java

import name.remal.gradle_plugins.dsl.PluginId

object JavaPlatformPluginId : PluginId("java-platform")
