package name.remal.gradle_plugins.plugins.java

import name.remal.concurrentMapOf
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_5_0
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.beforeResolve
import name.remal.gradle_plugins.dsl.extensions.beforeResolveIncoming
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.java
import name.remal.gradle_plugins.dsl.extensions.jcenterIfNotAdded
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.artifacts.component.ModuleComponentIdentifier
import org.gradle.api.artifacts.component.ModuleComponentSelector
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.util.GradleVersion

@Plugin(
    id = "name.remal.experimental-jsr205-jsr305-split-package-fixer",
    description = "Plugin that fixes split package situation of JSR 205 and JSR 305 artifacts.",
    tags = ["dependencies", "java9", "jigsaw", "split-package", "jsr205", "jsr305"]
)
@WithPlugins(JavaPluginId::class)
class Jsr205Jsr305SplitPackageFixerPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        internal const val MERGED_ARTIFACT_GROUP = "name.remal.merged"
        internal const val MERGED_ARTIFACT_ID = "jsr205-jsr305"
        internal const val MERGED_ARTIFACT_ID_REVERSE = "jsr305-jsr205"
    }

    private val versionsInfoCache = concurrentMapOf<Configuration, VersionsInfo>()

    @PluginAction(isHidden = true)
    fun processConfigurations(configurations: ConfigurationContainer, project: Project, repositories: RepositoryHandler) {
        configurations.all {
            if (GradleVersion.current() >= GRADLE_VERSION_5_0) {
                it.beforeResolveIncoming { beforeResolveConfiguration(it, configurations, project, repositories) }
            } else {
                it.beforeResolve { beforeResolveConfiguration(it, configurations, project, repositories) }
            }
        }
    }

    private fun beforeResolveConfiguration(configuration: Configuration, configurations: ConfigurationContainer, project: Project, repositories: RepositoryHandler) {
        if (!configuration.isCanBeResolved) {
            return
        }

        if (!project.java.sourceCompatibility.isJava9Compatible) {
            return
        }

        val versionsInfo = VersionsInfo()
        configurations.fillVersionsInfo(configuration, versionsInfo)
        versionsInfoCache[configuration] = versionsInfo

        val substitutionNotation: String? = with(versionsInfo) {
            if (jsr205Version != null && jsr305Version != null) {
                "$MERGED_ARTIFACT_GROUP:$MERGED_ARTIFACT_ID:$jsr205VersionPrefix$jsr205Version-$jsr305VersionPrefix$jsr305Version"
            } else if (jsr205Version != null) {
                "$MERGED_ARTIFACT_GROUP:$MERGED_ARTIFACT_ID:$jsr205VersionPrefix$jsr205Version-+"
            } else if (jsr305Version != null) {
                "$MERGED_ARTIFACT_GROUP:$MERGED_ARTIFACT_ID_REVERSE:$jsr305VersionPrefix$jsr305Version-+"
            } else {
                null
            }
        }

        if (substitutionNotation != null) {
            repositories.jcenterIfNotAdded()

            val notationsToSubstitute = versionsInfo.notationsToSubstitute
            configuration.resolutionStrategy {
                it.dependencySubstitution {
                    it.all substitution@{ subst ->
                        val requested = subst.requested as? ModuleComponentSelector ?: return@substitution
                        if ("${requested.group}:${requested.module}" in notationsToSubstitute) {
                            subst.useTarget(
                                substitutionNotation,
                                "Using ${notationsToSubstitute.joinToString(", ")} at the same time causes split package compile error, thus merged dependency is used."
                            )
                        }
                    }
                }
            }
        }
    }

    @Suppress("ComplexMethod", "LongMethod")
    private fun ConfigurationContainer.fillVersionsInfo(configuration: Configuration, versionsInfo: VersionsInfo) {
        forEach {
            if (it.isCanBeResolved && configuration in it.extendsFrom) {
                fillVersionsInfo(it, versionsInfo)
                if (versionsInfo.isCompleted) {
                    return
                }
            }
        }

        versionsInfoCache[configuration]?.let { cachedVersionInfo ->
            if (cachedVersionInfo.jsr205Version != null) {
                versionsInfo.jsr205VersionPrefix = cachedVersionInfo.jsr205VersionPrefix
                versionsInfo.jsr205Version = cachedVersionInfo.jsr205Version
            }
            if (cachedVersionInfo.jsr305Version != null) {
                versionsInfo.jsr305VersionPrefix = cachedVersionInfo.jsr305VersionPrefix
                versionsInfo.jsr305Version = cachedVersionInfo.jsr305Version
            }
            versionsInfo.notationsToSubstitute.addAll(cachedVersionInfo.notationsToSubstitute)
            return
        }

        with(versionsInfo) {
            val resolvedArtifacts: Iterable<ResolvedArtifact> = configuration.copyRecursive().resolvedConfiguration.resolvedArtifacts
            resolvedArtifacts.forEach { resolvedArtifact ->
                val id = resolvedArtifact.id.componentIdentifier as? ModuleComponentIdentifier ?: return@forEach
                with(id) {

                    if (jsr205Version == null) {
                        if (group == "javax.annotation" && module == "javax.annotation-api") {
                            notationsToSubstitute.add("$group:$module")
                            jsr205VersionPrefix = "javax_"
                            jsr205Version = version

                        } else if (group == "jakarta.annotation" && module == "jakarta.annotation-api") {
                            notationsToSubstitute.add("$group:$module")
                            jsr205VersionPrefix = "jakarta_"
                            jsr205Version = version
                        }
                    }

                    if (jsr305Version == null) {
                        if (group == "com.google.code.findbugs" && module == "jsr305") {
                            notationsToSubstitute.add("$group:$module")
                            jsr305VersionPrefix = "findbugs_"
                            jsr305Version = version
                        }
                    }

                }
            }
        }
    }

    private data class VersionsInfo(
        val notationsToSubstitute: MutableSet<String> = hashSetOf(),
        var jsr205Version: String? = null,
        var jsr205VersionPrefix: String = "",
        var jsr305Version: String? = null,
        var jsr305VersionPrefix: String = ""
    ) {

        val isCompleted: Boolean get() = jsr205Version != null && jsr305Version != null

    }

}
