package name.remal.gradle_plugins.plugins.jdk_cross_compilation

import java.io.File

class JdkInfo(
    jdkHome: File,
    bootstrapClasspath: Collection<File>? = null
) {

    val jdkHome: File = jdkHome.absoluteFile

    val bootstrapClasspath: Collection<File> = (bootstrapClasspath ?: listOf(
        jdkHome.resolve("jre/lib/rt.jar")
    )).filter(File::exists)

}
