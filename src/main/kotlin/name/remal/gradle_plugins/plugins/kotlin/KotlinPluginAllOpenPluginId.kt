package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinPluginAllOpenPluginId : PluginId("kotlin-allopen", "org.jetbrains.kotlin.plugin.allopen")
