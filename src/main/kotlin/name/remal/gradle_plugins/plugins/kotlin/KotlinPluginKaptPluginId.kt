package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinPluginKaptPluginId : PluginId("kotlin-kapt", "org.jetbrains.kotlin.kapt")
