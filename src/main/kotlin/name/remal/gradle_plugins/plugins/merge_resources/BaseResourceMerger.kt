package name.remal.gradle_plugins.plugins.merge_resources

import name.remal.gradle_plugins.dsl.utils.PathMatcher

abstract class BaseResourceMerger(final override val pattern: String) : ResourceMerger {

    private val caseSensitivePatternMatcher = PathMatcher(pattern, true)
    private val caseInsensitivePatternMatcher = PathMatcher(pattern, false)

    final override fun getPatternMatcher(isCaseSensitive: Boolean) = if (isCaseSensitive) {
        caseSensitivePatternMatcher
    } else {
        caseInsensitivePatternMatcher
    }

}
