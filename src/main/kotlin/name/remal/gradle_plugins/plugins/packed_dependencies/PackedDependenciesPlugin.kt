package name.remal.gradle_plugins.plugins.packed_dependencies

import name.remal.fromLowerCamelToLowerHyphen
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.AfterProjectEvaluation
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.HighestPriorityPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.compileOnly
import name.remal.gradle_plugins.dsl.extensions.createDependencyTransformConfiguration
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.gradle_plugins.dsl.extensions.main
import name.remal.gradle_plugins.dsl.extensions.makeNotTransitive
import name.remal.gradle_plugins.plugins.dependencies.TransitiveDependenciesConfigurationMatcher
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.Dependency
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.plugins.JavaPlugin.CLASSES_TASK_NAME
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import java.io.File

const val PACKED_CONFIGURATION_NAME = "packed"
const val PACKED_NOT_TRANSITIVE_CONFIGURATION_NAME = "packed-not-transitive"

@Plugin(
    id = "name.remal.packed-dependencies",
    description = "Plugin that provides 'packed' configuration.",
    tags = ["packed-dependencies"],
    isHidden = true
)
@WithPlugins(JavaPluginId::class)
class PackedDependenciesPlugin : BaseReflectiveProjectPlugin() {

    @HighestPriorityPluginAction
    fun ConfigurationContainer.`Create 'packed' configuration`() {
        val packedConf = create(PACKED_CONFIGURATION_NAME) { conf ->
            conf.description = "Packed dependencies"
        }

        val notTransitiveConf = createDependencyTransformConfiguration(packedConf, PACKED_NOT_TRANSITIVE_CONFIGURATION_NAME, transformer = Dependency::copy)
        notTransitiveConf.makeNotTransitive()
        notTransitiveConf.isVisible = false
        notTransitiveConf.isCanBeConsumed = false
        compileOnly.extendsFrom(notTransitiveConf)
    }

    @PluginAction
    @AfterProjectEvaluation
    fun TaskContainer.`Create 'processPackedDependencies' task`(configurations: ConfigurationContainer, project: Project, sourceSets: SourceSetContainer) {
        create("processPackedDependencies", ProcessPackedDependencies::class.java) { task ->
            task.packedDependencies = configurations.packed
            task.outputDir = File(project.buildDir, "classes/${task.name.fromLowerCamelToLowerHyphen()}")

            val classesDirs = sourceSets.main.output.classesDirs
            classesDirs as ConfigurableFileCollection
            classesDirs.from(task.outputDir)

            all(CLASSES_TASK_NAME) { it.dependsOn(task) }
        }
    }

}


val ConfigurationContainer.packed: Configuration get() = this[PACKED_CONFIGURATION_NAME]


@AutoService
class PackedDependenciesPluginTransitiveDependenciesConfigurationMatcher : TransitiveDependenciesConfigurationMatcher {
    override fun matches(project: Project, configuration: Configuration): Boolean {
        if (!project.isPluginAppliedAndNotDisabled(PackedDependenciesPlugin::class.java)) return false
        if (PACKED_CONFIGURATION_NAME == configuration.name) return true
        if (PACKED_NOT_TRANSITIVE_CONFIGURATION_NAME == configuration.name) return true
        return false
    }
}
