package name.remal.gradle_plugins.plugins.prohibited_classes

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.extensions.toConfigureAction
import org.gradle.api.Action
import org.gradle.api.artifacts.Dependency
import java.util.SortedMap
import java.util.SortedSet

@Extension
class ProhibitedClassesExtension {

    val classNames: SortedSet<String> = sortedSetOf()

    fun add(className: String) = add(listOf(className))
    fun add(vararg classNames: String) = add(classNames.toList())
    fun add(classNames: Iterable<String>) {
        this.classNames.addAll(classNames)
    }


    val modules: SortedMap<String, ProhibitedModule> = sortedMapOf()

    @JvmOverloads
    fun module(notation: Any, configurer: Action<ProhibitedModule> = Action {}): ProhibitedModule {
        val dependencyNotation: String
        if (notation is Dependency) {
            dependencyNotation = notation.notation.toString()
        } else {
            dependencyNotation = notation.toString()
        }

        val prohibitedModule = modules.computeIfAbsent(dependencyNotation, { ProhibitedModule() })
        configurer.execute(prohibitedModule)
        return prohibitedModule
    }

    fun module(notation: Any, @DelegatesTo(ProhibitedModule::class, strategy = DELEGATE_FIRST) configurer: Closure<*>) = module(notation, configurer.toConfigureAction())

}

class ProhibitedModule {

    val permittedClassNames: SortedSet<String> = sortedSetOf()

    fun permit(className: String) = permit(listOf(className))
    fun permit(vararg classNames: String) = permit(classNames.toList())
    fun permit(classNames: Iterable<String>) {
        this.permittedClassNames.addAll(classNames)
    }

}
