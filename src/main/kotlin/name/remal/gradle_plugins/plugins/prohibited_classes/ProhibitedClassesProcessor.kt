package name.remal.gradle_plugins.plugins.prohibited_classes

import name.remal.accept
import name.remal.buildList
import name.remal.escapeRegex
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.VALIDATION_STAGE
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.getOrNull
import name.remal.gradle_plugins.dsl.extensions.isCompilingSourceSet
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.compile.AbstractCompile
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper
import kotlin.LazyThreadSafetyMode.NONE

class ProhibitedClassesProcessor(
    private val settings: ProhibitedClassesExtension,
    private val classpathConfiguration: Configuration?
) : ClassesProcessor {

    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        val classNameRegexpsList = this.classNameRegexpsList
        if (classNameRegexpsList.isEmpty()) return

        val prohibitedClassNames = sortedSetOf<String>()
        val remapper = object : Remapper() {
            override fun map(internalClassName: String): String? {
                val currentClassName = internalClassName.replace('/', '.')
                classNameRegexpsList.forEach { classNameRegexps ->
                    if (classNameRegexps.permitted.any { it.matches(currentClassName) }) return@forEach
                    if (classNameRegexps.prohibited.any { it.matches(currentClassName) }) {
                        prohibitedClassNames.add(currentClassName)
                    }
                }
                return super.map(internalClassName)
            }
        }
        val classReader = ClassReader(bytecode)
        val classRemapper = ClassRemapper(ClassWriter(0), remapper)
        classReader.accept(classRemapper)

        if (prohibitedClassNames.isNotEmpty()) {
            throw CompiledClassHasProhibitedDependenciesException(buildString {
                append("Class ").append(className).append(" has prohibited ")
                if (prohibitedClassNames.size == 1) {
                    append("dependency:")
                } else {
                    append("dependencies:")
                }
                prohibitedClassNames.forEach {
                    append("\n    ").append(it)
                }
            })
        }
    }


    override fun getStage() = VALIDATION_STAGE


    private data class ClassNames(
        val prohibited: MutableSet<String> = mutableSetOf(),
        val permitted: MutableSet<String> = mutableSetOf()
    )

    private val classNamesList: List<ClassNames> by lazy(NONE) {
        buildList<ClassNames> {
            val prohibitedClassNames = settings.classNames
            if (prohibitedClassNames.isNotEmpty()) {
                add(ClassNames().apply {
                    prohibited.addAll(prohibitedClassNames)
                })
            }

            if (classpathConfiguration == null) return@buildList
            val prohibitedModules = settings.modules
            if (prohibitedModules.isEmpty()) return@buildList
            val resolvedDependencies = classpathConfiguration.resolvedConfiguration.lenientConfiguration.allModuleDependencies
            prohibitedModules.forEach { (matcherNotation, prohibitedModule) ->
                val matcher = DependencyNotationMatcher(matcherNotation)
                val permittedClassNames = prohibitedModule.permittedClassNames
                resolvedDependencies.asSequence()
                    .filter { matcher.matches(it.notation) }
                    .forEach { resolvedDependency ->
                        val classNames = CachedArtifactsCollection(resolvedDependency.moduleArtifacts.map(ResolvedArtifact::getFile)).classNames
                        if (classNames.isNotEmpty() || permittedClassNames.isNotEmpty()) {
                            add(ClassNames().apply {
                                prohibited.addAll(classNames)
                                permitted.addAll(permittedClassNames)
                            })
                        }
                    }
            }
        }
    }


    private data class ClassNameRegexps(
        val prohibited: MutableList<Regex> = mutableListOf(),
        val permitted: MutableList<Regex> = mutableListOf()
    )

    private val classNameRegexpsList: List<ClassNameRegexps> by lazy(NONE) {
        classNamesList.map { classNames ->
            ClassNameRegexps().apply {
                classNames.prohibited.forEach { prohibited.add(classNameToRegex(it)) }
                classNames.permitted.forEach { permitted.add(classNameToRegex(it)) }
            }
        }
    }

    private fun classNameToRegex(className: String) = Regex(
        className.splitToSequence('.')
            .map(::escapeRegex)
            .map { it.replace("\\*", ".*") }
            .joinToString("\\.")
    )

}

class CompiledClassHasProhibitedDependenciesException : RuntimeException {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
}


@AutoService
class ProhibitedClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {
    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        val project = compileTask.project
        if (!project.isPluginAppliedAndNotDisabled(ProhibitedClassesPlugin::class.java)) return emptyList()

        val settings = project[ProhibitedClassesExtension::class.java]
        if (settings.classNames.isEmpty() && settings.modules.isEmpty()) return emptyList()

        val classpathConfiguration = project.getOrNull(JavaPluginConvention::class.java)
            ?.sourceSets
            ?.firstOrNull(compileTask::isCompilingSourceSet)
            ?.compileClasspathConfigurationName
            ?.let { project.configurations.findByName(it) }

        return listOf(ProhibitedClassesProcessor(settings, classpathConfiguration))
    }
}
