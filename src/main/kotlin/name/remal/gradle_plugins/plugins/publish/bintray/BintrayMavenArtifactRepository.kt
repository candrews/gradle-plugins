package name.remal.gradle_plugins.plugins.publish.bintray

import org.gradle.api.artifacts.repositories.MavenArtifactRepository

interface BintrayMavenArtifactRepository : MavenArtifactRepository {

    var apiUrl: String

    var owner: String?

    var repositoryName: String?

    var packageName: String?

}
