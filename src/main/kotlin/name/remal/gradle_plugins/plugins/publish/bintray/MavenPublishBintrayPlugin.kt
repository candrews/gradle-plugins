package name.remal.gradle_plugins.plugins.publish.bintray

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.SNAPSHOT_REGEX
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.atTheEndOfAfterEvaluationOrNow
import name.remal.gradle_plugins.dsl.extensions.convention
import name.remal.gradle_plugins.dsl.extensions.invoke
import name.remal.gradle_plugins.dsl.extensions.logWarn
import name.remal.gradle_plugins.plugins.environment_variables.EnvironmentVariablesPlugin
import name.remal.gradle_plugins.plugins.publish.MavenPublishPluginId
import name.remal.gradle_plugins.plugins.publish.MavenPublishSettingsPlugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.tasks.PublishToMavenRepository
import org.gradle.api.tasks.TaskContainer

@Plugin(
    id = "name.remal.maven-publish-bintray",
    description = "Plugin that allows 'maven-publish' plugin publicate to Bintray.",
    tags = ["bintray", "java", "publish", "publication", "maven", "maven-publish"]
)
@WithPlugins(MavenPublishPluginId::class)
@ApplyPluginClasses(MavenPublishSettingsPlugin::class, EnvironmentVariablesPlugin::class)
class MavenPublishBintrayPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction("Add publishing.repositories.bintray extension method", order = 1001)
    fun ExtensionContainer.bintray() {
        invoke(PublishingExtension::class.java) {
            it.repositories.also {
                it.convention.addPlugin("name.remal.maven-publish-settings.bintray", RepositoryHandlerBintrayExtension(it))
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Skip publishing SNAPSHOT version to Bintray`(project: Project) {
        project.atTheEndOfAfterEvaluationOrNow(Int.MAX_VALUE) {
            all(PublishToMavenRepository::class.java) { task ->
                task.onlyIf { _ ->
                    val repositoryHost = task.repository.url.host
                    if (repositoryHost != null && (repositoryHost == "bintray.com" || repositoryHost.endsWith(".bintray.com"))) {
                        if (SNAPSHOT_REGEX.containsMatchIn(task.publication.version)) {
                            task.logWarn("SNAPSHOT version can't be published to Bintray")
                            return@onlyIf false
                        }
                    }
                    return@onlyIf true
                }
            }
        }
    }

}
