package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal

import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.artifacts.repositories.ArtifactRepository

interface GradlePluginPortalRepository : ArtifactRepository {

    var publishKey: String?

    var publishSecret: String?

    var websiteUrl: String?

    var vcsUrl: String?

    val plugins: NamedDomainObjectContainer<PluginInfo>

}
