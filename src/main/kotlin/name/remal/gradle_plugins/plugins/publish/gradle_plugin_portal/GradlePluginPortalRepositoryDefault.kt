package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal

import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.internal.artifacts.repositories.AbstractArtifactRepository

class GradlePluginPortalRepositoryDefault(project: Project) : AbstractArtifactRepository(project.objects), GradlePluginPortalRepository {

    override var publishKey: String? = null

    override var publishSecret: String? = null

    override var websiteUrl: String? = null

    override var vcsUrl: String? = null

    override val plugins: NamedDomainObjectContainer<PluginInfo> = project.container(PluginInfo::class.java)


    private lateinit var _name: String

    internal var isPartOfContainer = false

    override fun setName(name: String) {
        if (isPartOfContainer) {
            throw IllegalStateException("The name of an ArtifactRepository cannot be changed after it has been added to a repository container. You should set the name when creating the repository.")
        }
        _name = name
    }

    override fun getName(): String {
        return _name
    }

}
