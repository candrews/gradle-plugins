package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.internal

import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.PublishToGradlePluginPortalRepository
import org.jdom2.Document
import java.io.File

internal abstract class PublishToGradlePluginPortalInvoker {

    lateinit var task: PublishToGradlePluginPortalRepository

    lateinit var pomFile: File

    lateinit var pomDocument: Document

    abstract fun publish()

}
