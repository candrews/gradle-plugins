package name.remal.gradle_plugins.plugins.publish.nexus_staging

data class NexusRepositoryContentInfo(
    val repositoryId: String,
    val repositoryPath: String,
    val sha1Hash: String,
    val md5Hash: String
)
