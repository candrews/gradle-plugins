package name.remal.gradle_plugins.plugins.publish.nexus_staging

import java.time.ZonedDateTime

data class NexusStagingRepositoryEvent(
    val timestamp: ZonedDateTime,
    val name: String,
    val severity: Int,
    val properties: List<NexusStagingRepositoryEventProperty> = emptyList()
)


data class NexusStagingRepositoryEventProperty(
    val name: String,
    val value: String
)


@Suppress("ComplexMethod", "LongMethod", "ReturnCount")
fun NexusStagingRepositoryEvent.format(): List<String> {
    if (name == "rulesEvaluate") {
        return emptyList()
    }

    if (name == "ruleEvaluate" || name == "rulePassed" || name == "ruleFailed") {
        val messages = mutableListOf(buildString {
            append(
                when (name) {
                    "ruleEvaluate" -> "Evaluating rule"
                    "rulePassed" -> "Passed"
                    "ruleFailed" -> "Failed"
                    else -> name
                }
            )
            append(": ")

            val typeId = properties.firstOrNull { it.name == "typeId" }?.value
            append(
                when (typeId) {
                    "checksum-staging" -> "Checksum validation"
                    "no-traversal-paths-in-archive-file" -> "Archives must not contain insecure paths"
                    "javadoc-staging" -> "Javadoc validation"
                    "pom-staging" -> "POM validation"
                    "sources-staging" -> "Sources validation"
                    "signature-staging" -> "Signature validation"
                    "RepositoryWritePolicy" -> "Repository Writable"
                    else -> typeId
                }
            )
        })

        properties.forEach {
            if (it.name == "failureMessage") {
                messages.add("    " + it.value)
            }
        }
        return messages
    }

    if (name == "rulesFailed") {
        return listOf("Rules failed: " + properties.firstOrNull { it.name == "failureCount" }?.value)
    }

    if (name == "repositoryCloseFailed") {
        return listOf("Close failed")
    }

    if (name == "repositoryReleaseFailed") {
        return listOf("Release failed")
    }

    if (name == "rulesPassed") {
        return listOf("All rules passed")
    }

    if (name == "email") {
        val messages = mutableListOf("Sending e-mail notifications")
        properties.forEach {
            if (it.name == "to") {
                //messages.add("    " + it.value)
            }
        }
        return messages
    }

    if (name == "repositoryClosed") {
        return listOf("Repository closed")
    }

    if (name == "copyItems") {
        return listOf(buildString {
            append("Copying items from ")
            append(properties.firstOrNull { it.name == "source" }?.value)
            append(" to ")
            append(properties.firstOrNull { it.name == "target" }?.value)
        })
    }

    if (name == "repositoryReleased") {
        return listOf(buildString {
            append("Repository released to: ")
            append(properties.firstOrNull { it.name == "target" }?.value)
        })
    }

    return listOf(name)
}
