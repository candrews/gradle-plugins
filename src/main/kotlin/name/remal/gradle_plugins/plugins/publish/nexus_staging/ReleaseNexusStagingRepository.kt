package name.remal.gradle_plugins.plugins.publish.nexus_staging

import name.remal.buildSet
import name.remal.default
import name.remal.error
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.logDebug
import name.remal.gradle_plugins.dsl.extensions.logError
import name.remal.gradle_plugins.dsl.extensions.logLifecycle
import name.remal.gradle_plugins.dsl.extensions.requirePlugin
import name.remal.gradle_plugins.dsl.extensions.shouldRunSequentially
import name.remal.gradle_plugins.dsl.utils.retryIO
import name.remal.gradle_plugins.utils.RetrofitCallException
import name.remal.gradle_plugins.utils.addAuthorization
import name.remal.gradle_plugins.utils.baseUrl
import name.remal.gradle_plugins.utils.create
import name.remal.gradle_plugins.utils.newJsonRetrofitBuilder
import name.remal.gradle_plugins.utils.send
import name.remal.queueOf
import name.remal.warn
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.artifacts.repositories.PasswordCredentials
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import java.net.URI
import java.time.ZonedDateTime
import java.util.SortedSet

@BuildTask
class ReleaseNexusStagingRepository : DefaultTask() {

    companion object {
        private val SLEEP_MILLIS_DURATION: Long = 2500
    }


    init {
        requirePlugin(MavenPublishNexusStagingPlugin::class.java)
        shouldRunSequentially()
    }


    @get:Input
    lateinit var publication: MavenPublication

    @get:Input
    lateinit var repository: MavenArtifactRepository

    @get:Input
    lateinit var stagingNexusURI: URI

    @get:Input
    @get:Optional
    var stagingProfileId: String? = null


    private val nexusStagingApi: NexusStagingApi by lazy {
        val passwordCredentials = repository.getCredentials(PasswordCredentials::class.java)
        return@lazy newJsonRetrofitBuilder {
            it.addInterceptor {
                it.proceed(
                    it.request().newBuilder()
                        .addAuthorization(passwordCredentials.username.default(), passwordCredentials.password.default())
                        .build()
                )
            }
        }
            .baseUrl(stagingNexusURI)
            .create(NexusStagingApi::class.java)
    }


    @TaskAction
    protected fun release() {
        synchronized(ReleaseNexusStagingRepository::class.java) {
            doRelease()
        }
    }


    private fun doRelease() {
        val stagingNexusURI: URI = this.stagingNexusURI

        val stagingProfiles = retryIO { nexusStagingApi.getProfiles() }.data
            .filter { it.repositoryType == "maven2" }
            .sortedByDescending(NexusStagingProfile::name)

        val stagingProfileId: String = this.stagingProfileId ?: run {
            val groupId = publication.groupId
            return@run stagingProfiles.firstOrNull { (groupId + '.').startsWith(it.name + '.') }?.id
                ?.also { logLifecycle("Staging profile: {}", it) }
                ?: throw IllegalStateException(
                    "$stagingNexusURI: There is no matched staging profile for groupId '$groupId': ${
                        stagingProfiles.map(
                            NexusStagingProfile::name
                        )
                    }"
                )
        }

        val stagingProfile = stagingProfiles.firstOrNull { it.id == stagingProfileId }
            ?: throw IllegalStateException("$stagingNexusURI: There is no staging profile with id '$stagingProfileId'")


        val repositoriesToRelease = retryIO { nexusStagingApi.getProfileRepositories(stagingProfile) }.data.asSequence()
            .filter { it.type != "dropped" }
            .toList()

        if (repositoriesToRelease.isNotEmpty()) {
            var isSuccess = true
            repositoriesToRelease.forEach {
                try {
                    doRelease(it, stagingProfile.promotionTargetRepository)
                } catch (e: NexusRepositoryTransitionException) {
                    logger.error(e)
                    isSuccess = false
                }
            }

            if (!isSuccess) {
                throw GradleException("There were errors while performing operations on Nexus staging repositories, see logs")
            }

        } else {
            logLifecycle("There are no staging repositories to release with this staging profile.")
        }

        didWork = true
    }

    @Suppress("ComplexMethod", "LongMethod", "ReturnCount")
    private final tailrec fun doRelease(repository: NexusStagingRepository, targetRepositoryId: String) {
        if (repository.transitioning) {
            logDebug("{} - in transitioning state, waiting...", repository.repositoryURI)
            Thread.sleep(SLEEP_MILLIS_DURATION)
            doRelease(fetchRepository(repository.repositoryId) ?: return, targetRepositoryId)
            return
        }

        val startTimestamp = ZonedDateTime.now()
        val loggedEvents = mutableSetOf<NexusStagingActionEvent>()
        fun waitForActionToComplete() {
            var isSuccess = true
            while (true) {
                Thread.sleep(SLEEP_MILLIS_DURATION)
                val actions = fetchRepositoryActivity(repository.repositoryId)
                    .asSequence()
                    .filter { it.started >= startTimestamp }
                    .toList()
                if (actions.isEmpty()) {
                    break
                }

                actions.asSequence()
                    .flatMap { action ->
                        action.events.asSequence()
                            .filter { it.timestamp >= startTimestamp }
                            .map { NexusStagingActionEvent(action.name, action.started, it) }
                    }
                    .filter(loggedEvents::add)
                    .map(NexusStagingActionEvent::event)
                    .forEach { event ->
                        if (event.severity == 0) {
                            event.format().forEach { logLifecycle("    {}", it) }
                        } else {
                            event.format().forEach { logError("    {}", it) }
                            isSuccess = false
                        }
                    }

                if (actions.all { it.stopped != null }) {
                    break
                }
            }

            if (!isSuccess) {
                throw NexusRepositoryTransitionException(repository, loggedEvents)
            }
        }

        fun dropRepository() {
            logLifecycle("{} - dropping", repository.repositoryURI)
            retryIO { nexusStagingApi.bulkAction("drop", repository.repositoryId).send() }
            try {
                waitForActionToComplete()
            } catch (exception: NexusRepositoryTransitionException) {
                logger.warn(exception)
            }
        }

        fun waitForActionToCompleteOrDrop() {
            try {
                waitForActionToComplete()

            } catch (exception: NexusRepositoryTransitionException) {
                try {
                    dropRepository()
                } catch (e: Exception) {
                    exception.addSuppressed(e)
                }
                throw exception
            }
        }


        if (repository.type == "open") {
            logLifecycle("{} - closing", repository.repositoryURI)
            retryIO { nexusStagingApi.bulkAction("close", repository.repositoryId).send() }
            waitForActionToCompleteOrDrop()
            doRelease(fetchRepository(repository.repositoryId) ?: return, targetRepositoryId)
            return

        } else if (repository.type == "closed") {
            val content: Set<String> = fetchRepositoryContent(repository.repositoryId).filterTo(mutableSetOf<String>()) filter@{ relativePath ->
                if (relativePath.endsWith(".md5")) return@filter false
                if (relativePath.endsWith(".sha1")) return@filter false
                if (relativePath.endsWith(".asc")) return@filter false
                if ("/$relativePath".endsWith("/archetype-catalog.xml")) return@filter false
                if ("/$relativePath".endsWith("/maven-metadata.xml")) return@filter false
                return@filter true
            }


            if (content.isEmpty()) {
                logLifecycle("{} - is empty", repository.repositoryURI)
                dropRepository()
                return

            } else {
                logLifecycle("{} - fetching content", repository.repositoryURI)
                val alreadyPublishedPaths = buildSet<String> {
                    content.forEach { relativePath ->
                        fetchRepositoryContentInfo(targetRepositoryId, relativePath)?.let { targetInfo ->
                            fetchRepositoryContentInfo(repository.repositoryId, relativePath)?.let { stagingInfo ->
                                if (stagingInfo.sha1Hash == targetInfo.sha1Hash && stagingInfo.md5Hash == targetInfo.md5Hash) {
                                    add(relativePath)
                                }
                            }
                        }
                    }
                }

                logLifecycle("{} - content:{}", repository.repositoryURI, buildString {
                    content.forEach { relativePath ->
                        append("\n    ").append(relativePath)
                        if (relativePath in alreadyPublishedPaths) {
                            append(" - ALREADY PUBLISHED")
                        }
                    }
                })

                if (alreadyPublishedPaths == content) {
                    logLifecycle("{} - everything has already been published", repository.repositoryURI)
                    dropRepository()
                    return
                }
            }

            logLifecycle("{} - releasing", repository.repositoryURI)
            retryIO { nexusStagingApi.bulkAction("promote", repository.repositoryId).send() }
            waitForActionToCompleteOrDrop()
            return
        }
    }


    private fun fetchRepository(repositoryId: String): NexusStagingRepository? = retryIO {
        try {
            nexusStagingApi.getRepository(repositoryId)
        } catch (e: RetrofitCallException) {
            if (e.status == 404) {
                null
            } else {
                throw e
            }
        }
    }

    private fun fetchRepositoryActivity(repositoryId: String): List<NexusStagingRepositoryAction> = retryIO {
        try {
            nexusStagingApi.getRepositoryActivity(repositoryId)
        } catch (e: RetrofitCallException) {
            if (e.status == 404) {
                emptyList()
            } else {
                throw e
            }
        }
    }

    private fun fetchRepositoryContent(repositoryId: String): SortedSet<String> {
        val relativePathsQueue = queueOf<String>()
        relativePathsQueue.add("")

        val processedDirectories = hashSetOf<String>()
        processedDirectories.addAll(relativePathsQueue)

        val relativePaths = sortedSetOf<String>()
        while (true) {
            val currentRelativePath = relativePathsQueue.poll() ?: break
            val contentItems = retryIO {
                nexusStagingApi.getRepositoryContent(repositoryId, currentRelativePath).data
            }

            contentItems.forEach { contentItem ->
                val relativePath = contentItem.relativePath.trimStart('/')
                if (contentItem.leaf) {
                    relativePaths.add(relativePath)
                } else if (processedDirectories.add(relativePath)) {
                    relativePathsQueue.add(relativePath)
                }
            }
        }
        return relativePaths
    }

    private fun fetchRepositoryContentInfo(repositoryId: String, relativePath: String): NexusRepositoryContentInfo? = retryIO {
        try {
            nexusStagingApi.getRepositoryContentInfo(repositoryId, relativePath).data
        } catch (e: RetrofitCallException) {
            if (e.status == 404) {
                null
            } else {
                throw e
            }
        }
    }

}
