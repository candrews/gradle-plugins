package name.remal.gradle_plugins.plugins.publish.ossrh

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.SimpleTestAdditionalGradleScript
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.convention
import name.remal.gradle_plugins.dsl.extensions.invoke
import name.remal.gradle_plugins.plugins.environment_variables.EnvironmentVariablesPlugin
import name.remal.gradle_plugins.plugins.publish.MavenPublishPluginId
import name.remal.gradle_plugins.plugins.publish.MavenPublishSettingsPlugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.publish.PublishingExtension

@Plugin(
    id = "name.remal.maven-publish-ossrh",
    description = "Plugin that allows 'maven-publish' plugin publicate to OSS Repository Hosting.",
    tags = ["ossrh", "oss", "java", "publish", "publication", "maven", "maven-publish"]
)
@WithPlugins(MavenPublishPluginId::class)
@ApplyPluginClasses(MavenPublishSettingsPlugin::class, EnvironmentVariablesPlugin::class)
@SimpleTestAdditionalGradleScript(
    """
    publishing.repositories {
        ossrh()
    }
"""
)
class MavenPublishOssrhPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction("Add publishing.repositories.ossrh extension method", order = 1001)
    fun ExtensionContainer.ossrh(project: Project) {
        invoke(PublishingExtension::class.java) {
            it.repositories.let { repos ->
                repos.convention.addPlugin("name.remal.maven-publish-settings.ossrh", RepositoryHandlerOssrhExtension(project, repos))
            }
        }
    }

}
