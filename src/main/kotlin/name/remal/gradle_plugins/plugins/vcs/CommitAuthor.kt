package name.remal.gradle_plugins.plugins.vcs

data class CommitAuthor(
    val name: String,
    val email: String
)
