package name.remal.gradle_plugins.plugins.vcs

import name.remal.gradle_plugins.dsl.BuildTask

@BuildTask
class CreateProjectVersionTag : BaseCreateTagTask() {

    private var _tagNamePrefix: String? = null

    var tagNamePrefix: String
        get() = _tagNamePrefix ?: project.extensions.findByType(AutoVcsVersionExtension::class.java)?.versionTagPrefixes?.firstOrNull() ?: "version-"
        set(value) {
            _tagNamePrefix = value
        }

    override val tagName: String? get() = tagNamePrefix + project.version

}
