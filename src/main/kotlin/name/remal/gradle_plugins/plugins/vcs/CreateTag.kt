package name.remal.gradle_plugins.plugins.vcs

import name.remal.gradle_plugins.dsl.BuildTask

@BuildTask
class CreateTag : BaseCreateTagTask() {

    public override var tagName: String? = null

}
