package name.remal.gradle_plugins.plugins.vcs

data class VcsCacheKey(
    val commit: Commit?,
    val currentBranch: String?,
    val masterBranch: String?,
    val allTags: Set<String>
)
