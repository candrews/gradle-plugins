package name.remal.gradle_plugins.plugins.vcs

import name.remal.gradle_plugins.dsl.Extension
import org.gradle.api.Project
import java.io.File

@Extension
class VcsOperationsExtension(private val project: Project) : VcsOperations() {

    private val vcsOperations: VcsOperations by lazy { createVcsOperations(project) }

    override val vcsRootDir get() = vcsOperations.vcsRootDir

    override var overwriteMasterBranch: String?
        get() = vcsOperations.overwriteMasterBranch
        set(value) {
            vcsOperations.overwriteMasterBranch = value
            project.calculateAutoVcsVersion()
        }

    override val trueMasterBranch get() = vcsOperations.masterBranch

    override var overwriteCurrentBranch: String?
        get() = vcsOperations.overwriteCurrentBranch
        set(value) {
            vcsOperations.overwriteCurrentBranch = value
            project.calculateAutoVcsVersion()
        }
    override val trueCurrentBranch get() = vcsOperations.currentBranch

    override val isCommitted get() = vcsOperations.isCommitted

    override var commitAuthor: CommitAuthor?
        get() = vcsOperations.commitAuthor
        set(value) {
            vcsOperations.commitAuthor = value
        }

    override fun walkCommits() = vcsOperations.walkCommits()
    override fun getCurrentCommit() = vcsOperations.getCurrentCommit()
    override fun commitFiles(message: String, files: Collection<File>) = vcsOperations.commitFiles(message, files)
    override fun pathsToFiles(filePaths: Collection<String>): Collection<File> = project.files(filePaths).files
    override fun getAllTagNames() = vcsOperations.getAllTagNames()
    override fun createTag(commitId: String, tagName: String, message: String) = vcsOperations.createTag(commitId, tagName, message)
    override fun findTagWithDepth(predicate: (tagName: String) -> Boolean) = vcsOperations.findTagWithDepth(predicate)
    override fun setUnauthorizedRemoteURI(uri: String) = vcsOperations.setUnauthorizedRemoteURI(uri)
    override fun setUsernamePasswordAuth(username: String, password: CharArray) = vcsOperations.setUsernamePasswordAuth(username, password)
    override fun setSSHAuth(privateKeyFile: File, password: CharArray?) = vcsOperations.setSSHAuth(privateKeyFile, password)

}
