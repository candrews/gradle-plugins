package name.remal.gradle_plugins.utils

import okhttp3.Credentials
import okhttp3.Request

fun Request.Builder.addAuthorization(user: String, password: String) = addHeader("Authorization", Credentials.basic(user, password))
