package name.remal.gradle_plugins.plugins.code_quality.sonar

import name.remal.gradle_plugins.dsl.extensions.createWithUniqueName
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertNotNull
import org.junit.Test

class SonarLintTest : BaseProjectTest() {

    @Test
    fun compatibilityTest() {
        val task = project.tasks.createWithUniqueName(SonarLint::class.java)
        val reports = task.reports
        assertNotNull(reports.findBugsXml)
        assertNotNull(reports.html)
    }

}
