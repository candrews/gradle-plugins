package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertNotNull
import org.junit.Test

class DependencyHandlerToolsJarExtensionTest : BaseProjectTest() {

    @Test
    fun toolsJar() {
        val extension = DependencyHandlerToolsJarExtension(project)
        assertNotNull(extension.toolsJar())
    }

}
