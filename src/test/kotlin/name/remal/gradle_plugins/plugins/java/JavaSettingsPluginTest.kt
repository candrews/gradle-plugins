package name.remal.gradle_plugins.plugins.java

import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_5_6
import name.remal.gradle_plugins.dsl.extensions.api
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.compileClasspath
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.assertj.core.api.Assertions.assertThat
import org.gradle.api.artifacts.ArtifactRepositoryContainer.DEFAULT_MAVEN_CENTRAL_REPO_NAME
import org.gradle.api.artifacts.ArtifactRepositoryContainer.DEFAULT_MAVEN_LOCAL_REPO_NAME
import org.gradle.jvm.tasks.Jar
import org.gradle.util.GradleVersion
import org.junit.Test

class JavaSettingsPluginTest : BaseProjectTest() {

    @Test
    fun `Use maven-central and mavenLocal repositories by default`() {
        assertThat(project.repositories).`as`("project.repositories").isEmpty()

        project.applyPlugin(JavaSettingsPlugin::class.java)
        assertThat(project.repositories).`as`("project.repositories").isEmpty()

        project.applyPlugin(JavaPluginId)
        assertThat(project.repositories).`as`("project.repositories").hasSize(2)
        assertThat(project.repositories[0].name).`as`("project.repositories[0].name").startsWith(DEFAULT_MAVEN_CENTRAL_REPO_NAME)
        assertThat(project.repositories[1].name).`as`("project.repositories[1].name").startsWith(DEFAULT_MAVEN_LOCAL_REPO_NAME)

        project.repositories.mavenCentral()
        assertThat(project.repositories).`as`("project.repositories").hasSize(1)
        assertThat(project.repositories[0].name).`as`("project.repositories[0].name").startsWith(DEFAULT_MAVEN_CENTRAL_REPO_NAME)
    }

    @Test
    fun `Always consume JAR artifact and not classes`() {
        if (GradleVersion.current() < GRADLE_VERSION_5_6) return // not supported

        val dependencyProject = project.newChildProject("dependency") {
            applyPlugin(JavaSettingsPlugin::class.java)
            applyPlugin(JavaLibraryPluginId)
        }

        val dependentProject = project.newChildProject("dependent") {
            applyPlugin(JavaSettingsPlugin::class.java)
            applyPlugin(JavaLibraryPluginId)

            configurations.api.dependencies.add(dependencies.create(dependencyProject))
        }

        val dependentProjectCompileClasspath = dependentProject.configurations.compileClasspath.files
        assertThat(dependentProjectCompileClasspath)
            .contains(dependencyProject.tasks[Jar::class.java, "jar"].archiveFile.get().asFile)
    }

}
