package name.remal.gradle_plugins.plugins.java

import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.java
import name.remal.gradle_plugins.plugins.java.Jsr205Jsr305SplitPackageFixerPlugin.Companion.MERGED_ARTIFACT_GROUP
import name.remal.gradle_plugins.plugins.java.Jsr205Jsr305SplitPackageFixerPlugin.Companion.MERGED_ARTIFACT_ID
import name.remal.gradle_plugins.plugins.java.Jsr205Jsr305SplitPackageFixerPlugin.Companion.MERGED_ARTIFACT_ID_REVERSE
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.gradle_plugins.testing.dsl.testMavenRepository
import org.gradle.api.JavaVersion.VERSION_1_8
import org.gradle.api.JavaVersion.VERSION_1_9
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.artifacts.component.ModuleComponentIdentifier
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class Jsr205Jsr305SplitPackageFixerPluginTest : BaseProjectTest() {

    private lateinit var separateConf: Configuration
    private lateinit var superConf: Configuration
    private lateinit var parentConf: Configuration
    private lateinit var childConf: Configuration

    private lateinit var jsr250Version: String
    private lateinit var jsr350Version: String

    private lateinit var latestJsr250Version: String
    private lateinit var latestJsr350Version: String

    @Before
    fun before() {
        project.applyPlugin(Jsr205Jsr305SplitPackageFixerPlugin::class.java)
        project.applyPlugin(JavaPluginId)


        val jsr250Versions = arrayOf("1.2", "1.3", "1.3.1", "1.3.2")
        val jsr350Versions = arrayOf("2.0.0", "2.0.1", "2.0.2", "2.0.3", "3.0.0", "3.0.1", "3.0.2")
        project.testMavenRepository {
            jsr250Versions.forEach { jsr250Version ->
                val jsr250Component = component("javax.annotation", "javax.annotation-api", jsr250Version) { jar() }

                component(artifactId = "jsr205-dependent", version = jsr250Version) {
                    pom {
                        dependency(jsr250Component)
                    }
                }
            }

            jsr350Versions.forEach { jsr350Version ->
                val jsr350Component = component("com.google.code.findbugs", "jsr305", jsr350Version) { jar() }

                component(artifactId = "jsr305-dependent", version = jsr350Version) {
                    pom {
                        dependency(jsr350Component)
                    }
                }
            }


            jsr250Versions.forEach { jsr250Version ->
                jsr350Versions.forEach { jsr350Version ->
                    val mergedComponent = component(MERGED_ARTIFACT_GROUP, MERGED_ARTIFACT_ID, "javax_$jsr250Version-findbugs_$jsr350Version") { jar() }

                    component(MERGED_ARTIFACT_GROUP, MERGED_ARTIFACT_ID_REVERSE, "findbugs_$jsr350Version-javax_$jsr250Version") {
                        pom {
                            relocation(mergedComponent)
                        }
                    }
                }
            }
        }


        jsr250Version = jsr250Versions[1]
        jsr350Version = jsr350Versions[1]

        latestJsr250Version = jsr250Versions.last()
        latestJsr350Version = jsr350Versions.last()

        separateConf = project.configurations.create("separate") {
            it.dependencies.add(project.dependencies.create("test:jsr305-dependent:$jsr350Version"))
        }

        superConf = project.configurations.create("super") {
            it.dependencies.add(project.dependencies.create("test:jsr305-dependent:$jsr350Version"))
        }

        parentConf = project.configurations.create("parent") {
            superConf.extendsFrom(it)
        }

        childConf = project.configurations.create("child") {
            parentConf.extendsFrom(it)
            it.dependencies.add(project.dependencies.create("test:jsr205-dependent:$jsr250Version"))
        }
    }

    @Test
    fun java8() {
        project.java.sourceCompatibility = VERSION_1_8

        assertTrue(separateConf.resolvedConfiguration.resolvedArtifacts.none { it.group == MERGED_ARTIFACT_GROUP })
        assertTrue(superConf.resolvedConfiguration.resolvedArtifacts.none { it.group == MERGED_ARTIFACT_GROUP })
        assertTrue(parentConf.resolvedConfiguration.resolvedArtifacts.none { it.group == MERGED_ARTIFACT_GROUP })
        assertTrue(childConf.resolvedConfiguration.resolvedArtifacts.none { it.group == MERGED_ARTIFACT_GROUP })
    }

    @Test
    fun java9() {
        project.java.sourceCompatibility = VERSION_1_9

        separateConf.resolvedConfiguration.resolvedArtifacts.single { it.group == MERGED_ARTIFACT_GROUP }.run {
            assertEquals(MERGED_ARTIFACT_ID, module)
            assertEquals("javax_$latestJsr250Version-findbugs_$jsr350Version", version)
        }

        childConf.resolvedConfiguration.resolvedArtifacts.single { it.group == MERGED_ARTIFACT_GROUP }.run {
            assertEquals(MERGED_ARTIFACT_ID, module)
            assertEquals("javax_$jsr250Version-findbugs_$jsr350Version", version)
        }
        superConf.resolvedConfiguration.resolvedArtifacts.single { it.group == MERGED_ARTIFACT_GROUP }.run {
            assertEquals(MERGED_ARTIFACT_ID, module)
            assertEquals("javax_$jsr250Version-findbugs_$jsr350Version", version)
        }
        parentConf.resolvedConfiguration.resolvedArtifacts.single { it.group == MERGED_ARTIFACT_GROUP }.run {
            assertEquals(MERGED_ARTIFACT_ID, module)
            assertEquals("javax_$jsr250Version-findbugs_$jsr350Version", version)
        }
    }


    private val ResolvedArtifact.group get() = (id.componentIdentifier as? ModuleComponentIdentifier)?.group
    private val ResolvedArtifact.module get() = (id.componentIdentifier as? ModuleComponentIdentifier)?.module
    private val ResolvedArtifact.version get() = (id.componentIdentifier as? ModuleComponentIdentifier)?.version

}
