package name.remal.gradle_plugins.plugins.testing

import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.java
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.internal.GeneratedSubclass
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class TestSourceSetsPluginTest : BaseProjectTest() {

    lateinit var testSourceSets: TestSourceSetContainer
    lateinit var sourceSets: SourceSetContainer

    @Before
    fun before() {
        val project = newTempProject()
        project.applyPlugin(TestSourceSetsPlugin::class.java)
        project.applyPlugin(JavaPluginId)

        testSourceSets = project[TestSourceSetContainer::class.java]
        sourceSets = project.java.sourceSets
    }

    @Test
    @Suppress("JoinDeclarationAndAssignment")
    fun createAddRemove() {
        var counter = 0
        var sourceSet: SourceSet

        sourceSet = testSourceSets.create("temp${++counter}")
        assertTrue(sourceSet in testSourceSets)
        assertTrue(sourceSet in sourceSets)
        assertFalse(sourceSets.add(sourceSet))
        assertTrue(testSourceSets.remove(sourceSet))
        assertFalse(sourceSet in testSourceSets)
        assertFalse(sourceSet in sourceSets)

        sourceSet = testSourceSets.create("temp${++counter}")
        assertTrue(sourceSet in testSourceSets)
        assertTrue(sourceSet in sourceSets)
        assertTrue(sourceSets.remove(sourceSet))
        assertFalse(sourceSet in testSourceSets)
        assertFalse(sourceSet in sourceSets)

        sourceSet = sourceSets.create("temp${++counter}")
        assertFalse(sourceSet in testSourceSets)
        assertTrue(sourceSet in sourceSets)
        assertTrue(testSourceSets.add(sourceSet))
        assertTrue(sourceSet in testSourceSets)
        assertTrue(sourceSet in sourceSets)
        assertTrue(testSourceSets.remove(sourceSet))
        assertFalse(sourceSet in testSourceSets)
        assertFalse(sourceSet in sourceSets)

        sourceSet = sourceSets.create("temp${++counter}")
        assertFalse(sourceSet in testSourceSets)
        assertTrue(sourceSet in sourceSets)
        assertTrue(testSourceSets.add(sourceSet))
        assertTrue(sourceSet in testSourceSets)
        assertTrue(sourceSet in sourceSets)
        assertTrue(sourceSets.remove(sourceSet))
        assertFalse(sourceSet in testSourceSets)
        assertFalse(sourceSet in sourceSets)
    }

    @Test
    fun testSourceSetsDelegation() {
        assertTrue(testSourceSets is GeneratedSubclass)
    }

}
